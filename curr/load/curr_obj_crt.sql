CREATE TABLE cwt_wpt_stor_ctl(
  stor_id varchar(36),
  stor_type_ls varchar(15),
  opco_id varchar(4),
  vrsn_cd smallint,
  name_tx varchar(100),
  stor_no varchar(10),
  shop_cntr_name_tx varchar(100),
  adr1_tx varchar(100),
  adr2_tx varchar(100),
  city_tx varchar(100),
  st_cd char(2),
  zip_cd varchar(5),
  zip4_cd varchar(4),
  lat_qy float,
  long_qy float,
  tm_zone_qy smallint,
  desc_tx varchar(255),
  open_dt datetime year to second,
  clos_dt datetime year to second,
  audt_cr_dt datetime year to second,
  audt_upd_dt datetime year to second,
  audt_upd_user_id varchar(50),
  actv_fl char(1) 
)in curdbs2 extent size 256 next size 256 lock mode row;
revoke all on cwt_wpt_stor_ctl from public;

create unique index cwt_wpt_stor_ctl_pk on cwt_wpt_stor_ctl
    (stor_id) using btree in curidxdbs2 ;
alter table cwt_wpt_stor_ctl add constraint primary key
    (stor_id) constraint pk_cwt_wpt_stor_ctl;

grant select on cwt_wpt_stor_ctl to public;
grant delete,update,insert on cwt_wpt_stor_ctl to pentaho;
