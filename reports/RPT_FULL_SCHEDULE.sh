#!/bin/bash

#####Variables######
#KDIR=/Applications/pentaho61/design-tools/data-integration
KDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/reports
LDIR=/logs/pentaho/reports
DDIR=/data/pentaho/reports
FDATE=`date "+%Y%m%d"`
rm -rf $DDIR/.my_run_file

if [ ! -e "$DDIR/.my_date_${FDATE}" ]
then
 rm -f $DDIR/.my_date_[0-9]*
 echo "getting dates..."
 cd $KDIR
 ./pan.sh -file="$CDIR/set_time_var.ktr" -Level=ERROR > $LDIR/log.set_time_var 2>&1
fi

for i in `cat $DDIR/.my_date_${FDATE}`
do
  THE_DATE=`echo $i | cut -d"|" -f1`
  YEST=`echo $i | cut -d"|" -f2`
  TWO_AGO=`echo $i | cut -d"|" -f3`
  WEEK=`echo $i | cut -d"|" -f4`
  MONT=`echo $i | cut -d"|" -f5`
  PER=`echo $i | cut -d"|" -f6`
  QTR=`echo $i | cut -d"|" -f7`
  W_END_DT=`echo $i | cut -d"|" -f8`
  LAST_WEEK_BEG_DT=`echo $i | cut -d"|" -f9`
  LAST_WEEK_END_DT=`echo $i | cut -d"|" -f10`
  WEEK_BEG_DT=`echo $i | cut -d"|" -f11`
  WEEK_END_DT=`echo $i | cut -d"|" -f12`
  PER_BEG_DT=`echo $i | cut -d"|" -f13`
  PER_END_DT=`echo $i | cut -d"|" -f14`
  MONT_BEG_DT=`echo $i | cut -d"|" -f15`
  MONT_END_DT=`echo $i | cut -d"|" -f16`
  QTR_BEG_DT=`echo $i | cut -d"|" -f17`
  QTR_END_DT=`echo $i | cut -d"|" -f18`
  AHDZ_WEEK_NUM=`echo $i | cut -d"|" -f19`
  MON_DAY_NUM=`echo $i | cut -d"|" -f20`
  WEEK_DAY_NUM=`echo $i | cut -d"|" -f21`
done

TEN_AGO=`date -d '10 minutes ago' '+%Y-%m-%d %H:%M'`
#TEN_AGO=`date -v -10M "+%Y-%m-%d %H:%M"`
CURR_TM=`date "+%Y-%m-%d %H:%M"`

for j in `cat $DDIR/.my_job_list`
do
  REPORT_NAME=`echo $j | cut -d"|" -f1`
  RDIR=`echo $j | cut -d"|" -f2`
  RTYPE=`echo $j | cut -d"|" -f3`
  RMONT=`echo $j | cut -d"|" -f4`
  RPER_NUM=`echo $j | cut -d"|" -f5`
  RWEEK_NUM=`echo $j | cut -d"|" -f6`
  TEST=$(echo $j | cut -d"|" -f7)
  RJOB=$(echo $j | cut -d"|" -f8)
  TIM=$(date "+%Y-%m-%d $TEST")
#  if [[ "$TIM" < "$CURR_TM" && "$TIM" > "$TEN_AGO" ]] 
#  then
#     if [ "$RTYPE" = "P" -a "$AHDZ_WEEK_NUM" == "$RPER_NUM" -a "$WEEK_DAY_NUM" == "$RWEEK_NUM" ]
#     then
#        echo $RJOB"|"$RDIR 
#     elif [ "$RTYPE" = "W" -a "$WEEK_DAY_NUM" == "$RWEEK_NUM" ]
#     then
#       echo $RJOB"|"$RDIR 
#     elif [ "$RTYPE" = "M" -a "$MON_DAY_NUM" == "$RMONT" ]
#     then
#       echo $RJOB"|"$RDIR
#     fi
#  fi
  echo $RJOB"|"$RDIR
done >> $DDIR/.my_run_file 

if [ -s "$DDIR/.my_run_file" ]
then
 for x in `cat $DDIR/.my_run_file`
 do 
    job=`echo $x | cut -d"|" -f1`
    RDIR=`echo $x | cut -d"|" -f2`

    date;
    echo "Running $job ..."
    cd $KDIR
    echo "$KDIR/kitchen.sh -file=\"$CDIR/$RDIR/$job\" -Level=Basic > $LDIR/log.$job 2 >&1"
 done
else
 echo "NOTHING TO RUN"
fi
