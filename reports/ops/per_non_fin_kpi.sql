set isolation to dirty read;

select distinct ahdz_per_no
from rdt_tm_look
where ahdz_per_no = (select distinct ahdz_per_no - 1 as ahdz_per_no from rdt_tm_look where the_date = today)
into temp dates with no log;

--Route Totals
select distinct ahdz_per
,dlv_dt, dsc_rte_id
,case when depo_id = 'DK2' then 'GKC' else stor_id end as stor_id
,pip_intvl_conv(extend(est_rtrn_dt_tm,year to second)-extend(est_dprt_dt_tm,year to second)) as est_roadtime
,est_mile_qy
from archive:art_rte_hdr x1, datamart:rdt_tm_look x2
where x1.dlv_dt = x2.the_date
and x2.ahdz_per_no in (select distinct ahdz_per_no from dates)
into temp skm00 with no log;

--Route Detail
select distinct x2.ahdz_per, x1.dlv_dt, x1.pup_id,x1.stop_id,x1.est_mile_qy
,x1.est_ariv_tm_scnd_qy
,x1.dsc_rte_id
,count(distinct ord_id) as stops
,max(x1.shft_num_cd) as shft_num_cd
from archive:art_rte_dtl x1, datamart:rdt_tm_look x2
where x1.dlv_dt = x2.the_date
and x2.ahdz_per_no in (select distinct ahdz_per_no from dates)
group by 1,2,3,4,5,6,7
into temp skm01 with no log;

--grab the last stop on the route bc need to make 
--an adjustment at the end when joining with route header
select distinct
x1.ahdz_per, x1.dsc_rte_id, x1.dlv_dt, x1.pup_id
from skm01 x1
,(
select distinct
dsc_rte_id, dlv_dt, max(stop_id) as last_stop
from skm01
group by 1,2
) as x2
where x1.dsc_rte_id = x2.dsc_rte_id
and x1.dlv_dt = x2.dlv_dt
and x1.stop_id = x2.last_stop
into temp skm02 with no log;

--Calculates the Return Stem & Return Miles Per Route
select distinct x1.dlv_dt, x1.dsc_rte_id
,x1.est_roadtime - x2.est_roadtime_dtl as roadtime_stem
,x1.est_mile_qy - x2.est_mile_dtl as miles_stem
from skm00 as x1,
(
select distinct dsc_rte_id, dlv_dt
,sum(est_mile_qy) as est_mile_dtl
,(sum(est_ariv_tm_scnd_qy)/60) as est_roadtime_dtl
from skm01
group by 1,2
) as x2
where x1.dlv_dt = x2.dlv_dt
and x1.dsc_rte_id = x2.dsc_rte_id
into temp skm03 with no log;

--Clean Up Values
update skm03 set roadtime_stem = 0 
where roadtime_stem <= 0;
update skm03 set miles_stem = 0 
where miles_stem <= 0;

select distinct ahdz_per, dsc_rte_id, dlv_dt, pup_id, est_ariv_tm_scnd_qy, est_mile_qy, stops
from skm01
UNION
select distinct x1.*, x2.roadtime_stem, x2.miles_stem, 0 as stops
from skm02 x1, skm03 x2
where x1.dlv_dt = x2.dlv_dt
and x1.dsc_rte_id = x2.dsc_rte_id
into temp skm04 with no log;

--Append STORE ID
select distinct x1.*, x2.stor_id
from skm04 x1, skm00 x2
where x1.dlv_dt = x2.dlv_dt
and x1.dsc_rte_id = x2.dsc_rte_id
into temp skm05 with no log;

select distinct ahdz_per
,stor_id
,pup_id
,round(sum(est_mile_qy),4) as est_miles
,round(sum(est_ariv_tm_scnd_qy),4) as est_roadtime_mins
,sum(stops) as stops
from skm05
group by 1,2,3
into temp temp01 with no log;

select distinct ahdz_per, stor_id
,sum(est_miles) as tot_est_miles
,sum(est_roadtime_mins) as tot_est_roadtime_mins
,sum(stops) as tot_stops
from temp01
group by 1,2
into temp temp02 with no log;

select distinct x1.*, x2.tot_est_miles, x2.tot_est_roadtime_mins
,x2.tot_stops
from temp01 x1, temp02 x2
where x1.ahdz_per = x2.ahdz_per
and x1.stor_id = x2.stor_id
into temp allocation with no log;

--Different types of orders
create temp table ords_grp
(stor_id char(3),
ahdz_per	char(6),
pup_id	smallint,
stat	varchar(20),
value	decimal);

insert into ords_grp
select distinct x1.stor_id,x2.ahdz_per,x1.pup_id,'stops'
,count(distinct x1.ord_id) as stops
from datamart:rdt_rte_rpt_dtl x1, rdt_tm_look x2
where x1.leg_id = 1
and x1.dlv_dt = x2.the_date
and x2.ahdz_per_no in (select distinct ahdz_per_no from dates)
group by 1,2,3;

insert into ords_grp
select distinct x1.stor_id,x2.ahdz_per,x1.pup_id,'orders'
,count(distinct x1.ord_id) as orders
from rdt_ord_sum x1, rdt_tm_look x2
where x1.dlv_dt = x2.the_date
and x2.ahdz_per_no in (select distinct ahdz_per_no from dates)
and x1.ord_seq_num > 0  --Removes any re-deliveries
and x1.it_dmnd_qy > 0   --Removes any tote pick ups
and x1.whse_it_dlv_qy>0 --Removes anything cancelled before the shop/ after order download
and x1.ship_type_cd <> 'AUTHORIZE'
group by 1,2,3;

insert into ords_grp
select distinct x1.stor_id,x2.ahdz_per,x1.pup_id,'orders_shop'
,count(distinct x1.ord_id) as shopped_ords
from rdt_ord_sum x1, rdt_tm_look x2
where x1.dlv_dt = x2.the_date
and x2.ahdz_per_no in (select distinct ahdz_per_no from dates)
and x1.tot_pod_qy>0
group by 1,2,3;

insert into ords_grp
select distinct x1.stor_id,x2.ahdz_per,x1.pup_id,'orders_billed'
,count(distinct x1.ord_id) as shopped_ords
from rdt_ord_sum x1, rdt_tm_look x2
where x1.dlv_dt = x2.the_date
and x2.ahdz_per_no in (select distinct ahdz_per_no from dates)
and x1.ord_seq_num > 0  --Removes any re-deliveries
and x1.it_dmnd_qy > 0   --Removes any tote pick ups
and x1.whse_it_dlv_qy>0 --Removes anything cancelled before the shop/ after order download
and x1.ship_type_cd <> 'AUTHORIZE'
and x1.it_dlv_qy>0      --Billing Completed**
group by 1,2,3;

create temp table ords_grp_1
(stor_id char(3),
pup_id	smallint,
ords_net	decimal);

insert into ords_grp_1
select trim(a.stor_id) stor_id
        ,a.pup_id
        ,count(distinct case when (c.ord_id is null or c2.ord_id is not null) and a.whse_it_dlv_qy>0 and (a.ord_seq_num>0 or h.rdlv_type='F') then a.ord_id else null end) as ords_net
from rdt_ord_sum a
join rdt_tm_look b on a.dlv_dt=b.the_date
left join (select distinct ord_id from art_svc_err where ord_id is not null and cmpl_cd='Y' and rsn_cd in ('S-120','S-121','S-123','S-568') and svc_dt in (select the_date from rdt_tm_look where ahdz_per in (select ahdz_per from rdt_tm_look where the_date=today-6))) c on a.ord_id=c.ord_id
left join (select distinct ord_id from art_svc_err where ord_id is not null and cmpl_cd='Y' and rsn_cd in ('S-122') and svc_dt in (select the_date from rdt_tm_look where ahdz_per in (select ahdz_per from rdt_tm_look where the_date=today-6))) c2 on a.ord_id=c2.ord_id
join rdt_ecom_xref d on a.stor_id=d.stor_id
join cdt_pup_ctl e on a.pup_id=e.pup_id
join datamart:rdv_stor_zip f on a.zip_cd=f.zip_cd and a.type_cd=f.mbr_type_cd
join (select distinct ord_id
from cyborgdw:gxt_it_act
where arc_ld_dt in (select the_date from rdt_tm_look where ahdz_per in (select ahdz_per from rdt_tm_look where the_date=today-6))
) g on a.ord_id=g.ord_id
left join rdt_rdlv_type h on a.ord_id=h.ord_id
where b.ahdz_per in (select ahdz_per from rdt_tm_look where the_date=today-6)
group by 1,2;

--Combines all the diff orders/ stops type
--HERE
select distinct a.ahdz_per, a.stor_id, a.pup_id
,b.ords_net
,sum(case when a.stat = 'orders' then value else 0 end) as orders
,sum(case when a.stat = 'orders_billed' then value else 0 end) as orders_billed
,sum(case when a.stat = 'orders_shop' then value else 0 end) as orders_shop
,sum(case when a.stat = 'stops' then value else 0 end) as stops
from ords_grp a
left join ords_grp_1 b
on a.stor_id=b.stor_id
and a.pup_id=b.pup_id
group by 1,2,3,4
into temp orders with no log;

select distinct ahdz_per, stor_id, count(*) as routes
,sum(est_mile_qy) as orig_plan_mi
--,sum(case when act_mile_qy is null then 0 else est_mile_qy end) as adjst_plan_mi
--,sum(case when act_mile_qy is null then 0 else act_mile_qy end) as act_mi
--sum(case when act_mile_qy is null then 1 else 0 end) as rtes_missing_act_mi
,round(sum(plan_roadtime)/60,3) as plan_roadtime
,round(sum(act_roadtime)/60,3) as act_roadtime
FROM
(
select distinct
ahdz_per, stor_id, dlv_dt, dsc_rte_id
,est_mile_qy
,act_mile_qy
,pip_intvl_conv(extend(est_rtrn_dt_tm, year to second) - extend(est_dprt_dt_tm, year to second))
as plan_roadtime
,pip_intvl_conv
(
extend(
(case when gps_rtrn_dt_tm is null then act_rtrn_dt_tm else gps_rtrn_dt_tm end), year to second)
-
extend(
(case when gps_dprt_dt_tm is null then act_dprt_dt_tm else gps_dprt_dt_tm end), year to second)
)
as act_roadtime
FROM
(
select distinct 
x2.ahdz_per
,case when depo_id = 'DK2' then 'GKC' else stor_id
end as stor_id
,dlv_dt, dsc_rte_id
,est_dprt_dt_tm
,est_rtrn_dt_tm
,est_mile_qy
,act_dprt_dt_tm
,gps_dprt_dt_tm
,act_rtrn_dt_tm
,gps_rtrn_dt_tm
,act_mile_qy
, max(shft_num_cd) as shft_num_cd
from archive:art_rte_hdr x1, datamart:rdt_tm_look x2
where x1.dlv_dt = x2.the_date
and x2.ahdz_per_no in (select distinct ahdz_per_no from dates)
group by 1,2,3,4,5,6,7,8,9,10,11,12
)
)
group by 1,2
into temp routes with no log;

select distinct ahdz_per, stor_id, pup_id
,routes*(case when tot_stops <>0 then stops/tot_stops else 0 end) as calc_rtes
,orig_plan_mi*(case when tot_est_miles <>0 then est_miles/tot_est_miles else 0 end) as calc_plan_miles
,plan_roadtime*(case when tot_est_roadtime_mins <>0 then est_roadtime_mins/tot_est_roadtime_mins else 0 end) as calc_plan_roadtime_hrs
,act_roadtime*(case when tot_est_roadtime_mins <>0 then est_roadtime_mins/tot_est_roadtime_mins else 0 end) as calc_act_roadtime_hrs
FROM
(
select distinct x1.*, x2.routes, x2.orig_plan_mi, x2.plan_roadtime, x2.act_roadtime
from allocation x1, routes x2
where x1.ahdz_per = x2.ahdz_per
and x1.stor_id = x2.stor_id
)
into temp temp04 with no log;

select distinct x1.*
,x2.orders
,x2.orders_billed
,x2.orders_shop
,x2.ords_net
,x2.stops
from temp04 x1, orders x2
where x1.ahdz_per = x2.ahdz_per
and x1.stor_id = x2.stor_id
and x1.pup_id = x2.pup_id
into temp temp05 with no log;

--Used to calculate Shift Productivity
--(Based on Daily Ops Dash which collects more history than datamart:rdt_prod_pick_rpt
--select distinct x1.stor_id, x3.ahdz_per
--,sum(x1.value) as prod_eaches
--,sum(x2.value) as prod_shft_mins
--from rdt_ops_dly_dshbd x1, rdt_ops_dly_dshbd x2
--,rdt_tm_look x3
--where x1.dlv_dt = x2.dlv_dt
--and x1.stor_id = x2.stor_id
--and x1.stat = 'prod_eaches'
--and x2.stat = 'prod_shft_mins'
--and x1.dlv_dt = x3.the_date
--and x3.ahdz_per_no in (select distinct ahdz_per_no from dates)
--group by 1,2
--into temp prod1 with no log;

select ahdz_per,stor_id,sum(unit_cnt) prod_eaches,sum(tot_shft_min) prod_shft_mins
from datamart:rdt_prod_pick_rpt x1, rdt_tm_look x3
where x1.dlv_dt = x3.the_date
and x3.ahdz_per_no in (select distinct ahdz_per_no from dates)
group by 1,2
into temp prod1 with no log;

select distinct x1.ahdz_per, x1.stor_id, x1.pup_id
,case when x2.tot_ords = 0 then 0 else x1.orders_shop/x2.tot_ords
end as ords_alloc
from (select distinct ahdz_per, stor_id
,pup_id, orders_shop from temp05) as x1
,(select distinct ahdz_per, stor_id
,sum(orders_shop) as tot_ords
from temp05 group by 1,2) as x2
where x1.ahdz_per = x2.ahdz_per
and x1.stor_id = x2.stor_id
into temp prod2 with no log;

select distinct x1.ahdz_per, x1.stor_id, x1.pup_id
,x1.ords_alloc*prod_eaches as calc_prod_eaches
,(x1.ords_alloc*prod_shft_mins)/60 as calc_prod_shft_hrs
from prod2 x1,  prod1 x2
where x1.stor_id = x2.stor_id
and x1.ahdz_per = x2.ahdz_per
into temp prod3 with no log;

--Append Productivity #'s
select distinct x1.*
,x2.calc_prod_eaches
,x2.calc_prod_shft_hrs
from temp05 x1, prod3 x2
where x1.ahdz_per=x2.ahdz_per
and x1.stor_id = x2.stor_id
and x1.pup_id = x2.pup_id
into temp temp05a with no log;

--OOS dollars and Demand Dollars
select distinct ahdz_per, stor_id, pup_id
,sum(demand) as demand_doll
,sum(oos) as oos_doll
from auto_oos_zone x1, 
(select distinct week_end_dt,ahdz_per
from rdt_tm_look x1, dates x2
where x1.ahdz_per_no = x2.ahdz_per_no) as x2
where x1.week_end_dt = x2.week_end_dt
group by 1,2,3
into temp oos with no log;

--Append OOS #'s
select distinct x1.*
,x2.demand_doll
,x2.oos_doll
from temp05a x1, oos x2
where x1.ahdz_per=x2.ahdz_per
and x1.stor_id = x2.stor_id
and x1.pup_id = x2.pup_id
into temp temp05b with no log;

--Adds BPI metrics
SELECT DISTINCT
x11.ahdz_per ,
x9.stor_id ,
x9.pup_id ,
SUM(CASE
WHEN (x10.rsn_cd = 'S-100' )THEN 1 ELSE 0 END )s100 ,
SUM(CASE
WHEN (x10.rsn_cd = 'S-101' )THEN 1 ELSE 0 END )s101 ,
SUM(CASE
WHEN (x10.rsn_cd = 'S-102' )THEN 1 ELSE 0 END )s102,
SUM(CASE
WHEN (x10.rsn_cd = 'S-003' )THEN 1 ELSE 0 END )s003, -- previously s103
COUNT(*) as tot_bpi
FROM
datamart:"c00dba".rdt_ord_sum x9 ,
archive:"c00dba".art_svc_err x10 ,
datamart:"c00dba".rdt_tm_look x11
WHERE x11.ahdz_per_no in (select distinct ahdz_per_no from dates)
AND x9.ord_id = x10.ord_id
AND x9.dlv_dt = x11.the_date
AND x10.rsn_cd IN ('S-100' ,'S-101' ,'S-102' ,'S-003')
group by 1,2,3
into temp bpi with no log;

--Appends BPI
select distinct x1.*
,case when x2.s100 is null then 0 else x2.s100 end as s100
,case when x2.s101 is null then 0 else x2.s101 end as s101
,case when x2.s102 is null then 0 else x2.s102 end as s102
,case when x2.s003 is null then 0 else x2.s003 end as s003
,case when x2.tot_bpi is null then 0 else x2.tot_bpi end as tot_bpi
from temp05b x1, outer bpi x2
where x1.ahdz_per = x2.ahdz_per
and x1.stor_id = x2.stor_id
and x1.pup_id = x2.pup_id
into temp temp05c with no log;

select distinct
 ahdz_per
,stor_id
,cost_cntr_id
,pup_id
,round(calc_rtes,0) as calc_rtes
,round(calc_plan_miles,0) as calc_plan_miles
,round(calc_plan_roadtime_hrs,0) as calc_plan_roadtime_hrs
,round(calc_act_roadtime_hrs,0) as calc_act_roadtime_hrs
,round(orders,0) as orders
,round(orders_billed,0) as orders_billed
,round(orders_shop,0) as orders_shop
,round(ords_net,0) as orders_net
,round(stops,0) as stops
,round(calc_prod_eaches,0) as calc_prod_eaches
,round(calc_prod_shft_hrs,0) as calc_prod_shft_hrs
,round(demand_doll,0) as demand_doll
,round(oos_doll,0) as oos_doll
,round(s100,0) as s100
,round(s101,0) as s101
,round(s102,0) as s102
,round(s003,0) as s003
,round(tot_bpi,0) as tot_bpi
FROM
(
select distinct
x1.*
,case 
when x1.pup_id = 0 then cast(x2.fin_stor_id as smallint)
when x1.pup_id>0 then x3.hypr_loc_cd 
end as cost_cntr_id
from temp05c x1
,datamart:rdt_ecom_xref x2
,outer daily_curr:cdt_pup_ctl x3
where x1.stor_id = x2.stor_id
and x1.pup_id = x3.pup_id
)
into temp final with no log;

delete from ea_per_nonfin_kpi where 1=1;
insert into ea_per_nonfin_kpi
select distinct x1.*
,case when x1.stor_id = 'GIJ' then 'Jersey City' 
else x2.sns_mkt_area_tx end as sns_mkt_area_tx
,case when pup_id = 0 then 'D' else 'P' end as dlv_meth_cd
from final x1, rdt_ecom_xref x2
where x1.stor_id = x2.stor_id;
