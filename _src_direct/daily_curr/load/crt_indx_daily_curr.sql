set isolation dirty read;
set pdqpriority 100;

create index cdt_mgrp_it_mstr2 on cdt_mgrp_it_mstr
    (vend_sku,dc_cd) using btree  in curdlyidxdbs2;

create index cdt_omt_prim_cat_tree_id1 on cdt_omt_prim_cat_tree
    (chld_cat_id) using btree  in curdlyidxdbs2;

create index cdt_omt_prim_cat_tree_id2 on cdt_omt_prim_cat_tree
    (parn_cat_tree_id) using btree  in curdlyidxdbs2;

create index cdt_caps_cnsm_idx2 on cdt_caps_cnsm_info
    (cnsm_id,pmt_meth_cd) using btree in curdlyidxdbs2;

create index cdt_cnsm_dlv_idx1 on cdt_cnsm_dlv (last_name_tx,
    frst_name_tx) fillfactor 100 in curdlyidxdbs2;

create index cdt_csi_offr_dtl_i2 on cdt_csi_offr_dtl
    (list_id) using btree  in curdlyidxdbs2;

create index cdt_csi_offr_dtl_i3 on cdt_csi_offr_dtl
    (csi_offr_id) using btree  in curdlyidxdbs2;

create index cdt_csi_offr_upc_i2 on cdt_csi_offr_upc
    (list_id) using btree  in curdlyidxdbs2;

create index cdt_csi_offr_upc_i3 on cdt_csi_offr_upc
    (csi_offr_dtl_id) using btree  in curdlyidxdbs2;

create index cdt_nwtn_it_sub_i12 on cdt_nwtn_it_sub 
    (pod_id, stor_id) in curdlyidxdbs2;

create index cur_cnsm_cpn_hdr2 on cur_cnsm_cpn_hdr
    (cnsm_id) in curdlyidxdbs2;

create index cur_cnsm_cpn_hdr3 on cur_cnsm_cpn_hdr
    (list_id) in curdlyidxdbs2;

create index cdt_oct_atr_val_id1 on cdt_oct_atr_val
    (atr_id) using btree  in curdlyidxdbs2;

create index cdt_olt_list_atr_val_id1 on cdt_olt_list_atr_val
    (list_id) using btree  in curdlyidxdbs2;

create index cdt_olt_list_dtl_id1 on cdt_olt_list_dtl
    (list_id) using btree  in curdlyidxdbs2;

create index cdt_olt_list_sub_dtl_id1 on cdt_olt_list_sub_dtl
    (list_dtl_id) using btree  in curdlyidxdbs2;

create index cdt_it_dict_idx1 on cdt_it_dict
    (it_name_tx) using btree  in curdlyidxdbs2;

create index cdt_it_dict_idx2 on cdt_it_dict
    (it_dict_cat_id) using btree  in curdlyidxdbs2;

create index cdt_rfr_mail_trk_2 on cdt_rfr_mail_trk
    (send_mail_tx) fillfactor 100 in curdlyidxdbs2;

create index cdt_rfr_mail_trk_3 on cdt_rfr_mail_trk
    (rcv_mail_tx) fillfactor 100 in curdlyidxdbs2;

create index cdt_user_mail_subs_i2 on cdt_user_mail_subs
    (user_id) using btree  in curdlyidxdbs2;

create index cur_dly_user_idx2 on cur_dly_user
    (cnsm_id) using btree  in curdlyidxdbs2;

create index cur_dly_user_prof2 on cur_dly_user_prof
    (vip_cd) using btree  in curdlyidxdbs2;

create index cur_dly_user_prof3 on cur_dly_user_prof
    (cnsm_id) using btree  in curdlyidxdbs2;

create index cur_dly_user_prof4 on cur_dly_user_prof
    (user_levl_cd) using btree  in curdlyidxdbs2;

create index cdt_mail_job_ctl_i3 on cdt_mail_job_ctl
    (job_cat_cd) using btree  in curdlyidxdbs2;

create index cdt_mail_job_trk_i2 on cdt_mail_job_trk
    (job_id) using btree  in curdlyidxdbs2;

create index cdt_mail_user_trk_i2 on cdt_mail_user_trk
    (job_id) using btree  in curdlyidxdbs2;

create index cdt_mail_user_trk_i3 on cdt_mail_user_trk (user_id)
    using btree  in curdlyidxdbs2;

create index cdt_mrch_po2 ON cdt_mrch_po(ord_id)  
    using btree in curdlyidxdbs2;

create index cdt_mrch_po3 ON cdt_mrch_po (list_id)
    using btree in curdlyidxdbs2;

create index cdt_pmtt_trk_rt_2 on cdt_pmtt_trk_rtrn
    (user_id) fillfactor 100 in curdlyidxdbs2;

create index cdt_x_it2 on cdt_x_it (upc_cd,stor_id) 
    fillfactor 100 in curdlyidxdbs2;

create index cdt_x_it3 on cdt_x_it (stor_id) 
    fillfactor 100 in curdlyidxdbs2;

create index cdt_x_it4 on cdt_x_it (sub_pod_id) 
    fillfactor 100 in curdlyidxdbs2;
