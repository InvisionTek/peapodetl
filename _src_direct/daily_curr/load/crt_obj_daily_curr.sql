create table cur_dly_slot_dtl
  (
    slot_id integer,
    slot_hdr_id integer,
    dlv_area_id smallint,
    dlv_tm datetime hour to minute,
    ship_type_cd char(10),
    dlv_wnd_tm interval hour to minute,
    slot_avl_qy smallint,
    disc_qy decimal(7,2),
    slot_stat_cd char(1) ,
    dlv_msg_id char(1),
    stor_id char(7),
    shft_num_cd smallint,
    dlv_dt date,
    cut_off_dt_tm datetime year to minute,
    max_ord_qy smallint,
    slot_used_qy smallint,
    slot_hdr_used_qy smallint
  ) in curdlydbs1 extent size 44000 next size 4000 lock mode row;
revoke all on cur_dly_slot_dtl from "public";
create unique index cur_dly_slot_dtl1 on cur_dly_slot_dtl
    (slot_id, dlv_dt)  fillfactor 100 in curdlyidxdbs1 ;
alter table cur_dly_slot_dtl add constraint primary key
    (slot_id) constraint cur_dly_slot_dtl1  ;
--####################################
create table cur_dly_it_invt
  (
    it_cd char(20),
    it_name_tx char(35),
    hypr_cat_cd char(4)
  ) in curdlydbs1  extent size 1000 next size 1000 lock mode row;
revoke all on cur_dly_it_invt from "public";
create unique index cur_dly_it_invt1 on cur_dly_it_invt
    (it_cd) using btree  in curdlyidxdbs1;
alter table cur_dly_it_invt add constraint primary key
    (it_cd) constraint cur_dly_it_invt1;
--####################################
create table cdt_it_dict
  (
    pod_id serial not null ,
    it_name_tx char(35),
    it_size_cd char(11),
    it_uom_cd char(6),
    unit_scal_qy decimal(7,2),
    pr_scal_qy decimal(7,4),
    actv_cd char(1),
    proc_cd char(1),
    majr_cat_id integer,
    it_dict_cat_id integer,
    evnt_prod_id integer,
    it_long_name_tx char(100),
    addl_srch_tx char(256),
    brnd_id integer,
    adv_id integer,
    audt_cr_id char(8),
    audt_cr_dt date,
    audt_upd_id char(8),
    audt_upd_dt date,
    ingr_flag_cd char(1),
    pkg_flag_cd char(1),
    rule_id integer,
    ntrn_fact_cd char(1)
  ) in curdlydbs1  extent size 70000 next size 10000 lock mode row;
revoke all on cdt_it_dict from "public" ;
create unique index cdt_it_dict_pk on cdt_it_dict
    (pod_id) using btree  in curdlyidxdbs1;
alter table cdt_it_dict add constraint primary key (pod_id)
    constraint cdt_it_dict_pk  ;
--#################################################
create table cur_dly_user
  (
    user_id integer,
    cnsm_id char(8),
    log_id char(8),
    hndl_id varchar(20),
    ckie_id varchar(100),
    rtlr_card_id varchar(20),
    rtlr_card_trk_id varchar(20),
    mail_tx varchar(60),
    othr_mail_tx varchar(60),
    msg_mail_tx varchar(60),
    mail_type_cd smallint,
    mail_stat_fl smallint,
    othr_mail_stat_fl smallint,
    mail_opt_news_cd char(1),
    mail_opt_spec_cd char(1),
    gst_cd char(1),
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second
  ) in curdlydbs1  extent size 80000 next size 5000 lock mode row;
revoke all on cur_dly_user from "public" ;
create unique index cur_dly_user_pk on cur_dly_user
    (user_id) using btree  in curdlyidxdbs1;
alter table cur_dly_user add constraint primary key
    (user_id) constraint cur_dly_user_pk  ;
--########################################################
create table cur_dly_cnsm
  (
    cnsm_id char(8),
    last_name_tx varchar(30),
    frst_name_tx varchar(20),
    mi_tx char(1),
    ttl_tx char(6),
    adr_1_tx varchar(30),
    adr_2_tx varchar(30),
    city_tx varchar(20),
    st_cd char(2),
    zip_cd char(10),
    home_phon_cd char(13),
    work_phon_cd char(13),
    work_phon_ext_cd char(4),
    stor_id char(7),
    cnsm_dt date,
    mbr_fee_freq_cd char(1),
    cnsm_exp_dt date,
    pmt_meth_cd char(6),
    crdt_card_id varchar(32),
    crdt_card_exp_dt char(5),
    crdt_card_cnsm_tx char(20),
    chk_acct_id varchar(64),
    chk_acct_rtng_id varchar(9),
    bank_tx varchar(25),
    tax_cd char(1),
    ord_lim_qy decimal(10),
    ar_acct_dflt_cd integer,
    ar_dept_dflt_cd char(3),
    acct_bal_qy decimal(10),
    on_acct_qy decimal(10),
    stat_cd char(1),
    type_cd char(1),
    cptr_cd char(1),
    phon_pswd_tx varchar(15),
    ds_rate_cd char(1),
    allw_smpl_tx char(1),
    time_ord_qy smallint,
    pup_time_ord_qy smallint,
    last_mnth_fee_dt date,
    last_ord_dt date,
    cnsm_alrt_qy smallint,
    shop_cd char(1)
  ) in curdlydbs1  extent size 210000 next size 20000 lock mode row;
revoke all on cur_dly_cnsm from "public" ;
create unique index cur_dly_cnsm_pk on cur_dly_cnsm
    (cnsm_id) using btree  in curdlyidxdbs1;
alter table cur_dly_cnsm add constraint primary key
    (cnsm_id) constraint cur_dly_cnsm_pk  ;
--###############################################
create table cur_dly_stor_zip
  (
    zip_city_id serial not null ,
    zip_cd char(10) not null ,
    city_tx char(20) not null ,
    st_cd char(3) not null ,
    stor_id char(7) not null ,
    mbr_type_cd char(1),
    dlv_area_id smallint not null ,
    stop_time_cd smallint not null ,
    tax_grp_cd char(3) not null ,
    rpt_stor_id char(4) not null ,
    prom_pr_zone_cd smallint not null ,
    base_pr_zone_cd smallint not null ,
    time_diff_qy smallint
  ) in curdlydbs1  extent size 1024 next size 1024 lock mode row;
revoke all on cur_dly_stor_zip from "public" ;
create unique index cur_dly_stor_zip_pk on cur_dly_stor_zip
    (zip_city_id) using btree  in curdlyidxdbs1;
alter table cur_dly_stor_zip add constraint primary
    key (zip_city_id) constraint cur_dly_stor_zip_pk
     ;
--#################################################
create table cdt_user_pref
  (
    user_id integer not null ,
    pref_id integer not null ,
    pref_val_cd varchar(64)
  ) in curdlydbs1  extent size 20000 next size 5000 lock mode row;
revoke all on cdt_user_pref from public;
create unique index cdt_user_pref_i1 on cdt_user_pref
    (user_id,pref_id) using btree  in curdlyidxdbs1;
alter table cdt_user_pref add constraint primary key
    (user_id,pref_id) constraint pk_cdt_user_pref  ;
--############################################
create table cur_dly_user_prof
  (
    user_id integer,
    cnsm_id char(8),
    last_name_tx char(30),
    frst_name_tx char(30),
    mi_tx char(1),
    ttl_tx char(6),
    stor_id char(7),
    stat_cd char(1),
    type_cd char(1),
    shop_cd char(1),
    ecom_stor_id smallint,
    opco_id char(4),
    pref_pmt_meth_cd char(6),
    crdt_card_type_cd char(4),
    vip_cd varchar(10),
    schl_cd varchar(10),
    schl_cd_updt_dt date,
    bons_card_fl char(1),
    spec_ctl_grp_cd char(1),
    pma_id char(10),
    dlv_st_cd char(2),
    dlv_zip_cd char(5),
    dlv_area_id smallint,
    user_levl_cd smallint,
    user_sgmt_cd smallint,
    avg_bskt_lftm_qy decimal(7,2),
    avg_bskt_rcnt_qy decimal(7,2),
    ord_freq_lftm_qy decimal(5,3),
    ord_freq_rcnt_qy decimal(5,3),
    time_ord_qy smallint,
    pup_time_ord_qy smallint,
    last_sess_dt datetime year to second,
    tot_sess_qy integer,
    cnsm_exp_dt date,
    frst_ord_dt date,
    frst_dlv_dt date,
    frst_pup_dt date,
    frst_svc_dt date,
    last_dlv_dt date,
    last_pup_dt date,
    last_svc_dt date,
    bpi_svc_rec_qy smallint,
    tote_bal_qy smallint,
    acct_bal_qy decimal(7,2),
    cp_conv_dt date,
    cp_send_ord_fl char(1),
    kids_opt_in_cd char(1),
    mktg_1_targ_cd char(1),
    mktg_2_targ_cd char(1),
    mktg_3_targ_cd char(1),
    mktg_4_targ_cd char(1),
    mktg_5_targ_cd char(1),
    mrch_1_targ_cd char(1),
    mrch_2_targ_cd char(1),
    mrch_3_targ_cd char(1),
    mrch_4_targ_cd char(1),
    mrch_5_targ_cd char(1),
    mrch_6_targ_cd char(1),
    prgm_1_ord_qy smallint,
    prgm_2_ord_qy smallint,
    prgm_3_ord_qy smallint,
    prgm_4_ord_qy smallint,
    prgm_5_ord_qy smallint,
    prgm_pup1_ord_qy smallint,
    prgm_pup2_ord_qy smallint,
    prgm_1_sale_qy decimal(9,2),
    prgm_2_sale_qy decimal(9,2),
    prgm_3_sale_qy decimal(9,2),
    prgm_4_sale_qy decimal(9,2),
    prgm_5_sale_qy decimal(9,2),
    prgm_pup1_sale_qy decimal(9,2),
    prgm_pup2_sale_qy decimal(9,2),
    mktg_prgm_dt date,
    audt_wkly_upd_dt datetime year to second,
    audt_dly_upd_dt datetime year to second
  ) in curdlydbs1  extent size 200000 next size 40000 lock mode row;
revoke all on cur_dly_user_prof from "public";
create unique index cur_dly_user_prof1 on cur_dly_user_prof
    (user_id) using btree  in curdlyidxdbs1;
alter table cur_dly_user_prof add constraint primary
    key (user_id) constraint cur_dly_user_prof1;
--############################################
create table cdt_cnsm_dlv
  (
    cnsm_id char(8),
    last_name_tx char(30),
    frst_name_tx char(20),
    mi_tx char(1),
    adr_1_tx char(30),
    adr_2_tx char(30),
    city_tx char(20),
    st_cd char(2),
    zip_cd char(10),
    day_phon_cd char(13),
    day_phon_ext_cd char(4),
    map_cd char(5),
    map_sect_cd char(1),
    istr_1_tx char(60),
    istr_2_tx char(60),
    istr_3_tx char(60),
    istr_oth_tx char(60),
    isct_tx char(25),
    ew_str_tx char(24),
    ns_str_tx char(24),
    nrth_1_cd char(2),
    east_1_cd char(2),
    east_2_cd char(2),
    sth_1_cd char(2),
    sth_2_cd char(2),
    west_1_cd char(2),
    west_2_cd char(2),
    nrth_2_cd char(2),
    dlv_bld_tx char(10),
    dlv_desc_tx char(20),
    park_tx char(15),
    door_tx char(15),
    cart_cd char(1),
    hand_1_tx char(60),
    hand_2_tx char(60),
    hand_3_tx char(60),
    shop_1_tx char(60),
    shop_2_tx char(60),
    shop_3_tx char(60)
  ) in curdlydbs1 extent size 540000 next size 20000 lock mode row;
revoke all on cdt_cnsm_dlv from "public";
create unique index cdt_cnsm_dlv_pk on cdt_cnsm_dlv
    (cnsm_id)  fillfactor 100 in curdlyidxdbs1 ;
alter table cdt_cnsm_dlv add constraint primary key
    (cnsm_id) constraint cdt_cnsm_dlv_pk  ;
--################################################
create table cdt_caps_cnsm_info
  (
    caps_id integer not null ,
    cnsm_id char(8) not null ,
    pmt_meth_cd char(5) not null ,
    pmt_mask_id varchar(18),
    bank_tx varchar(25),
    crdt_card_cnsm_tx varchar(20),
    crdt_dnc_type_tx varchar(20),
    crdt_card_type_cd char(4),
    crdt_card_exp_dt char(5),
    gift_card_cd char(1),
    chk_acct_rtng_id varchar(9),
    stat_cd char(1),
    audt_cr_id varchar(12),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id varchar(12),
    audt_upd_dt_tm datetime year to second
  ) in curdlydbs1  extent size 10000 next size 10000 lock mode row;
revoke all on cdt_caps_cnsm_info from "public";
create unique index cdt_caps_cnsm_idx1 on cdt_caps_cnsm_info
    (caps_id) using btree  in curdlyidxdbs1;
alter table cdt_caps_cnsm_info add constraint primary
    key (caps_id) constraint cdt_caps_cnsm_info_pk ;
--###################################################
create table cdt_user_atr
  (
    user_id integer,
    atr_id integer,
    atr_val_cd varchar(50),
    audt_cr_dt date,
    audt_upd_dt date
  ) in curdlydbs1  extent size 10000 next size 1000 lock mode row;
revoke all on cdt_user_atr from "public";
create unique index cdt_user_atr_pk on cdt_user_atr
    (user_id,atr_id) using btree  in curdlyidxdbs1;
alter table cdt_user_atr add constraint primary key
    (user_id,atr_id) constraint pk_cdt_user_atr;
--###################################################
create table cdt_groc_rtl_tax
  (
    pod_id integer not null ,
    tax_grp_cd char(3) not null ,
    stor_id char(6) not null ,
    it_tax_qy decimal(7,5) not null ,
    btl_dep_qy decimal(3,2) not null ,
    audt_upd_dt date not null ,
    audt_upd_id char(10)
  ) in curdlydbs1 extent size 16000 next size 8000 lock mode row;
revoke all on cdt_groc_rtl_tax from "public";
create unique index cdt_groc_rtl_tx_pk on cdt_groc_rtl_tax
    (pod_id,tax_grp_cd,stor_id) using btree  in curdlyidxdbs1;
alter table cdt_groc_rtl_tax add constraint primary
    key (pod_id,tax_grp_cd,stor_id)
    constraint pk_cdt_groc_rtl_tx ;
--###################################################
create table cur_groc_upc_dept
  (
    upc_cd decimal(14,0) not null ,
    sku_2_cd integer,
    cmdy_cd char(10)
  ) in curdlydbs1 extent size 3500 next size 1000 lock mode row;
revoke all on cur_groc_upc_dept from "public";
create unique index cur_groc_upc_dept1 on cur_groc_upc_dept
    (upc_cd)  fillfactor 100 in curdlyidxdbs1 ;
alter table cur_groc_upc_dept add constraint primary key
    (upc_cd) constraint cur_groc_upc_dept1  ;
--###################################################
create table cdt_mgrp_btl_tax
  (
    pod_id integer not null ,
    btl_num_qy smallint
  ) in curdlydbs1 extent size 1000 next size 1000 lock mode row;
revoke all on cdt_mgrp_btl_tax from "public";
create unique index cdt_mgrp_btl_tax_pk on cdt_mgrp_btl_tax
    (pod_id) in curdlyidxdbs1;
alter table cdt_mgrp_btl_tax add constraint primary key
    (pod_id) constraint cdt_mgrp_btl_tax_pk
     ;
--###################################################
create table cdt_mgrp_cost
  (
    pod_id integer not null ,
    stor_id char(7) not null ,
    cost_eff_dt date not null ,
    supl_cd char(3) not null ,
    base_cost_pr_qy decimal(11,4),
    case_pack_qy integer,
    seq_cd INTEGER
  ) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_mgrp_cost from "public";
create unique index cdt_mgrp_cost_pk on cdt_mgrp_cost
    (pod_id,stor_id,cost_eff_dt) fillfactor 100 in curdlyidxdbs1;
alter table cdt_mgrp_cost add constraint primary key
    (pod_id,stor_id,cost_eff_dt) constraint cdt_mgrp_cost_pk
     ;
--###################################################
create table cdt_mgrp_it_mstr
  (
    pod_id integer,
    dc_cd char(3),
    stnd_rtl_prod_cd decimal(14,0),
    mfg_it_num char(15),
    it_desc varchar(50),
    it_size char(8),
    vend_num char(6),
    vend_sku varchar(9),
    mktg_cat char(3),
    mktg_sub_cat char(3),
    mktg_sub_sub_cat char(3),
    cigt_cat char(1),
    purc_cat char(3),
    purc_sub_cat char(3),
    purc_sub_sub_cat char(3),
    prc_cat char(3),
    prc_sub_cat char(3),
    prc_sub_sub_cat char(3),
    brnd_name varchar(25),
    vend_rtio integer,
    jit_invt_fl integer,
    buyr integer,
    last_rcv_dt date,
    last_qty_rcv integer,
    slot_cap integer,
    whse_area char(2),
    whse_aisle char(3),
    whse_bay char(3),
    whse_pos char(2),
    buy_stat_cd char(1),
    sell_stat_cd char(1),
    rcd_stat char(1),
    cw_avg decimal(9,4),
    it_type char(1),
    unit_of_issue CHAR(3),
    flo_cd CHAR(1),
    on_hand_qy integer,
    mds_sel_sec_cd char(6),
    mds_repl_sec_cd char(6),
    mds_cnt_sec_cd  char(6),
    in_dc_cd char(1)
  ) in curdlydbs1  extent size 40000 next size 1000 lock mode row;
revoke all on cdt_mgrp_it_mstr from "public";
create unique index cdt_mgrp_it_mstr1 on cdt_mgrp_it_mstr
    (pod_id,dc_cd) using btree  in curdlyidxdbs1 ;
alter table cdt_mgrp_it_mstr add constraint primary
    key (pod_id,dc_cd) constraint cdt_mgrp_it_mstr1
    ;
--###################################################
create table cdt_mgrp_vend_mstr
  (
    vend_num char(6) NOT NULL,
    vend_name char(50)
  ) in curdlydbs1  extent size 2048 next size 2048 lock mode row;
revoke all on cdt_mgrp_vend_mstr from "public";
create unique index cdt_mgrp_vend_mstr1 on cdt_mgrp_vend_mstr
    (vend_num) using btree  in curdlyidxdbs1 ;
alter table cdt_mgrp_vend_mstr add constraint primary
    key (vend_num) constraint cdt_mgrp_vend_mstr1 ;
--###################################################
create table cdt_omt_prim_cat
  (
    prim_cat_id integer,
    cat_name_tx varchar(100),
    cat_srch_name_tx varchar(100),
    type_cd char(1),
    actv_cd char(1),
    audt_cr_dt_tm datetime year to minute,
    audt_cr_user_cd varchar(10),
    audt_upd_dt_tm datetime year to minute,
    audt_upd_user_cd varchar(10)
  ) in curdlydbs1  extent size 500 next size 500 lock mode row;
revoke all on cdt_omt_prim_cat from "public";
create unique index cdt_omt_prim_pk on cdt_omt_prim_cat
    (prim_cat_id) using btree  in curdlyidxdbs1;
--###################################################
create table cdt_omt_prim_cat_tree
  (
    prim_cat_tree_id integer,
    tree_id smallint,
    seq_id smallint,
    chld_cat_id integer,
    parn_cat_tree_id integer,
    actv_cd char(1),
    audt_cr_dt_tm datetime year to minute,
    audt_cr_user_cd varchar(10),
    audt_upd_dt_tm datetime year to minute,
    audt_upd_user_cd varchar(10)
  ) in curdlydbs1  extent size 500 next size 500 lock mode row;
revoke all on cdt_omt_prim_cat_tree from "public";
create unique index cdt_omt_prim_cat_tree_pk on
    cdt_omt_prim_cat_tree (prim_cat_tree_id) using btree  in
    curdlyidxdbs1;
--###################################################
create table cdt_csi_cpn_sum
  (
    list_id     integer not null ,
    ord_id      char(10),
    fund_src_cd char(1) not null ,
    user_id     integer,
    tot_cpn_amt_qy decimal(5,2)
  ) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_csi_cpn_sum from "public";
create unique index cdt_csi_cpn_sum_pk on cdt_csi_cpn_sum
    (list_id,fund_src_cd) using btree  in curdlyidxdbs1;
alter table cdt_csi_cpn_sum add constraint primary key
    (list_id,fund_src_cd) constraint pk_cdt_csi_cpn_sum;
--################################################
create table cdt_csi_offr_dtl
  (
    csi_offr_dtl_id     integer,
    list_id             integer,
    csi_offr_id         integer,
    cpn_amt_qy          decimal(5,2)
  ) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_csi_offr_dtl from "public";
create unique index cdt_csi_offr_dtl_i1 on cdt_csi_offr_dtl
    (csi_offr_dtl_id,list_id) using btree  in curdlyidxdbs1;
alter table cdt_csi_offr_dtl add constraint primary
    key (csi_offr_dtl_id,list_id) constraint pk_cdt_csi_offr_dtl;
--################################################
create table cdt_csi_offr_hdr
  (
    csi_offr_id         integer,
    csi_mkt_id          char(4),
    ds_type_cd          integer,
    mult_qy             smallint,
    ds_amt_qy           decimal(5,2),
    lim_type_cd         char(1),
    lim_amt_qy          integer,
    min_it_reqd_qy      integer,
    offr_src_fl         char(1),
    csi_feat_cd         integer,
    pre_amt_qy          integer,
    csi_offr_type_cd    smallint,
    pre_cond_cd         smallint,
    csi_offr_desc_tx    varchar(255)
  ) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_csi_offr_hdr from "public";
create unique index cdt_csi_offr_hdr_pk on cdt_csi_offr_hdr
    (csi_offr_id,csi_mkt_id) using btree  in curdlyidxdbs1;
alter table cdt_csi_offr_hdr add constraint primary
    key (csi_offr_id,csi_mkt_id) constraint pk_cdt_csi_offr_hdr;

--################################################
create table cdt_csi_offr_upc
  (
    csi_offr_upc_id     integer,
    list_id             integer,
    csi_offr_dtl_id     integer,
    upc_cd              varchar(18),
    prod_id             integer
  ) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_csi_offr_upc from "public";
create unique index cdt_csi_offr_upc_pk on cdt_csi_offr_upc
    (csi_offr_upc_id,list_id) using btree  in curdlyidxdbs1 ;
alter table cdt_csi_offr_upc add constraint primary
    key (csi_offr_upc_id,list_id) constraint pk_cdt_csi_offr_upc ;
--################################################
create table cdt_nwtn_it_sub
  (
    pod_id integer,
    stor_id char(3),
    sub_pod_id integer,
    sub_qy smallint,
    seq_qy smallint,
    stat_cd char(1)
  ) in curdlydbs1  extent size 4000 next size 2000 lock mode row;
revoke all on cdt_nwtn_it_sub from public;
--################################################
create table cdt_rte_drvr
    (drvr_id char(8),
    stor_id char(3),
    depo_id char(3),
    frst_name_tx char(35),
    last_name_tx char(35),
    actv_cd char(1),
    sns_ee_id char(10),
    team_cd char(3),
    phon_1_cd char(13),
    phon_2_cd char(13),
    phon_3_cd char(13),
    mail_tx varchar(60),
    msg_mail_tx varchar(60)
  ) in curdlydbs1 extent size 1000 next size 500 lock mode row;
revoke all on cdt_rte_drvr from "public";
create unique index cdt_rte_drvr1 on cdt_rte_drvr
    (drvr_id) using btree  in curdlyidxdbs1;
alter table cdt_rte_drvr add constraint primary key
    (drvr_id) constraint cdt_rte_drvr_pk;
--################################################
CREATE TABLE cdt_uprm_user
(
  user_id INTEGER,
  cnsm_id CHAR(8),
  stat_cd CHAR(1),
  audt_upd_dt DATE
) in curdlydbs1 extent size 16 next size 16 lock mode row;
revoke all on cdt_uprm_user from public;
create unique index cdt_uprm_user_pk on cdt_uprm_user
(user_id) using btree  in curdlyidxdbs1;
alter table cdt_uprm_user add constraint primary key
(user_id) constraint pk_cdt_uprm_user;
--################################################
create table cur_cnsm_cpn_dtl
  (
    bskt_id integer,
    cpn_id varchar(16),
    cpn_ttl_tx varchar(20),
    cpn_desc_tx varchar(45),
    cpn_tot_used_qy smallint,
    cpn_tot_svgs_qy decimal(7,2),
    audt_cr_id char(10),
    audt_cr_dt_tm datetime year to second
  ) in curdlydbs1 extent size 12000 next size 6000 lock mode row;
revoke all on cur_cnsm_cpn_dtl from "public";
create unique index cur_cnsm_cpn_dtl1 on cur_cnsm_cpn_dtl
    (bskt_id,cpn_id)   in curdlyidxdbs1 ;
alter table cur_cnsm_cpn_dtl add constraint primary
    key (bskt_id,cpn_id) constraint cur_cnsm_cpn_dtl1;
--################################################
create table cur_cnsm_cpn_hdr
  (
    bskt_id integer,
    cnsm_id char(8),
    list_id integer,
    ord_id char(10),
    rdm_proc_cd char(1),
    rdm_dt_tm datetime year to second,
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second
  ) in curdlydbs1 extent size 2000 next size 1000 lock mode row;
revoke all on cur_cnsm_cpn_hdr from "public";
create unique index cur_cnsm_cpn_hdr1 on cur_cnsm_cpn_hdr
    (bskt_id) in curdlyidxdbs1 ;
alter table cur_cnsm_cpn_hdr add constraint primary
    key (bskt_id) constraint cur_cnsm_cpn_hdr1  ;
--################################################
create table cur_cnsm_cpn_upc
  (
    bskt_id integer,
    cpn_id varchar(16),
    upc_cd char(13)
  ) in curdlydbs1 extent size 12000 next size 6000 lock mode row;
revoke all on cur_cnsm_cpn_upc from "public";
create unique index cur_cnsm_cpn_upc1 on cur_cnsm_cpn_upc
    (bskt_id,cpn_id,upc_cd)   in curdlyidxdbs1;
alter table cur_cnsm_cpn_upc add constraint primary
    key (bskt_id,cpn_id,upc_cd) constraint cur_cnsm_cpn_upc1 ;
--################################################
create table cur_dly_shop_ctl 
  (
    main_stor_id char(7),
    it_shop_cd char(1),
    it_dnld_cd char(1),
    audt_upd_id char(10),
    audt_upd_dt date,
    audt_upd_tm datetime hour to second
  ) in curdlydbs1 extent size 16 next size 16 lock mode row;
revoke all on cur_dly_shop_ctl from public;
create unique index cur_dly_shop_ctl1 on cur_dly_shop_ctl
    (main_stor_id,it_shop_cd)  fillfactor 100 in curdlyidxdbs1;
alter table cur_dly_shop_ctl add constraint primary key
    (main_stor_id,it_shop_cd) constraint cur_dly_shop_ctl1;
--###################################################
create table cur_hr_ee
(
file_id char(6),
clck_id char(5),
stor_id char(7),
last_name_tx char(30),
frst_name_tx char(30),
job_ttl_tx char(24),
hire_dt date,
rhre_dt date,
ej_dept_cd char(2),
dept_cd char(5),
loc_cd char(3),
term_dt date,
hrs_week_qy decimal (3,1),
cat_cd char(1),
pmt_cd char(1),
job_cd char(8),
snty_dt date,
i9_stat_tx varchar(30),
aln_exp_dt date,
i9_doc1_tx varchar(30),
i9_doc1_exp_dt date,
i9_doc2_tx varchar(30),
i9_doc2_exp_dt date,
spvs_frst_name_tx varchar(35),
spvs_last_name_tx varchar(35)
) in curdlydbs1 extent size 3200 next size 1600 lock mode row;
revoke all on cur_hr_ee from "public";
create unique index cur_hr_ee_pk on cur_hr_ee
    (file_id)  fillfactor 100 in curdlyidxdbs1 ;
alter table cur_hr_ee add constraint primary key
    (file_id) constraint cur_hr_ee_pk  ;
--###################################################
create table cdt_oct_atr_val
  (
    atr_val_id integer,
    lvl_id smallint,
    othr_key_id integer,
    atr_id integer,
    atr_val_cd varchar(255),
    audt_cr_dt_tm datetime year to second,
    audt_cr_user_id varchar(10),
    audt_upd_dt_tm datetime year to second,
    audt_upd_user_cd varchar(10)
  ) in curdlydbs1  extent size 500 next size 500 lock mode row;
revoke all on "c00dba".cdt_oct_atr_val from "public";
create unique index cdt_oct_atr_val_pk on cdt_oct_atr_val
    (atr_val_id) using btree  in curdlyidxdbs1;
alter table cdt_oct_atr_val add constraint primary key
    (atr_val_id) constraint pk_cdt_oct_atr_val  ;
--###################################################
create table cdt_olt_list_atr_val
  (
    list_atr_val_id integer,
    list_id integer,
    valu_cd varchar(12),
    valu_tx lvarchar(3000),
    audt_upd_dt_tm datetime year to second,
    audt_upd_user_id varchar(10)
  ) in curdlydbs1  extent size 500 next size 500 lock mode row;
revoke all on cdt_olt_list_atr_val from "public";
create unique index cdt_olt_list_atr_val_pk on
    cdt_olt_list_atr_val (list_atr_val_id) using btree  in curdlyidxdbs1;
alter table cdt_olt_list_atr_val add constraint primary
    key (list_atr_val_id) constraint pk_cdt_olt_list_atr_val;
--###################################################
create table cdt_olt_list_dtl
  (
    list_dtl_id integer,
    list_id integer,
    line_qy smallint,
    valu_type_cd varchar(20),
    audt_upd_dt_tm datetime year to second,
    audt_upd_user_id varchar(10)
  ) in curdlydbs1  extent size 500 next size 500 lock mode row;
revoke all on cdt_olt_list_dtl from "public";
create unique index cdt_olt_list_dtl_pk on cdt_olt_list_dtl
    (list_dtl_id) using btree  in curdlyidxdbs1;
alter table cdt_olt_list_dtl add constraint primary
    key (list_dtl_id) constraint pk_cdt_olt_list_dtl;
--###################################################
create table cdt_olt_list_hdr
  (
    list_id integer,
    srce_cd varchar(30),
    user_id integer,
    list_name_tx varchar(100),
    audt_cr_dt_tm datetime year to second,
    audt_cr_user_id varchar(10),
    audt_upd_dt_tm datetime year to second,
    audt_upd_user_id varchar(10)
  ) in curdlydbs1  extent size 500 next size 500 lock mode row;
revoke all on cdt_olt_list_hdr from "public";
create unique index cdt_olt_list_hdr_pk on cdt_olt_list_hdr
    (list_id) using btree  in curdlyidxdbs1;
alter table cdt_olt_list_hdr add constraint primary
    key (list_id) constraint pk_cdt_olt_list_hdr;
--###################################################
create table cdt_olt_list_sub_dtl
  (
    list_sub_dtl_id integer,
    list_dtl_id integer,
    valu_cd varchar(20),
    valu_tx lvarchar(1000),
    audt_upd_dt_tm datetime year to second,
    audt_upd_user_id char(10)
  ) in curdlydbs1  extent size 500 next size 500 lock mode row;
revoke all on cdt_olt_list_sub_dtl from "public";
create unique index "c00dba".cdt_olt_list_sub_dtl_pk on
    cdt_olt_list_sub_dtl (list_sub_dtl_id) using btree  in curdlyidxdbs1;
alter table cdt_olt_list_sub_dtl add constraint primary
    key (list_sub_dtl_id) constraint pk_cdt_olt_list_sub_dtl;
--###################################################
create table cdt_img_exst
(
pod_id integer,
upc_cd decimal(14,0),
actv_cd CHAR(1),
cont_id integer,
med_cont_id integer,
sm_cont_id integer,
prim_cont_id integer
) in curdlydbs1 extent size 1500 next size 1000 lock mode row ;
revoke all on cdt_img_exst from public;
create unique index cdt_img_exst_pk on cdt_img_exst
    (pod_id)  fillfactor 100 in curdlyidxdbs1 ;
alter table cdt_img_exst add constraint primary key
    (pod_id) constraint cdr_img_exst_pk;
--###################################################
create table cdt_cp_cat
(
  minr_cat_id integer not null,
  cp_cat_id integer not null,
  cp_cat_tx char(30) not null,
  audt_upd_dt date not null
) in curdlydbs1 extent size 64 next size 64 lock mode row;
revoke all on cdt_cp_cat from public;
--####################################
create table cdt_it_dict_cat
  (
    it_dict_cat_id integer,
    cat_type_cd char(5),
    cat_tx char(60)
  ) in curdlydbs1  extent size 64 next size 64 lock mode row;
revoke all on cdt_it_dict_cat from "public";
create unique index cdt_it_dict_cat_i1 on cdt_it_dict_cat
    (it_dict_cat_id,cat_type_cd) using btree  in curdlyidxdbs1 ;
create unique index cdt_it_dict_cat_i2 on cdt_it_dict_cat
    (cat_tx) using btree  in curdlyidxdbs1 ;
--############################################
create table cdt_rfr_mail_trk
  (
    rfr_id integer,
    send_mail_tx varchar(60),
    rcv_mail_tx varchar(60),
    rfr_user_id integer,
    send_dt_tm datetime year to second
  ) in curdlydbs1 extent size 8000 next size 8000 lock mode row;
revoke all on cdt_rfr_mail_trk from "public";
create unique index cdt_rfr_mail_tr_pk on cdt_rfr_mail_trk
    (rfr_id,send_mail_tx,rcv_mail_tx) fillfactor 100 in curdlyidxdbs1;
alter table cdt_rfr_mail_trk add constraint primary
    key (rfr_id,send_mail_tx,rcv_mail_tx) constraint pk_cdt_rfr_mail_tr;
--############################################
create table cdt_user_atr_look
  (
    atr_id integer,
    atr_name_tx varchar(20),
    atr_dflt_val_cd varchar(50)
  ) in curdlydbs1 extent size 32 next size 32 lock mode page;
revoke all on cdt_user_atr_look from "public";
create unique index cdt_user_atr_look1 on cdt_user_atr_look
    (atr_id) in curdlyidxdbs1;
alter table cdt_user_atr_look add constraint primary
    key (atr_id) constraint cdt_user_atr_look1;
--############################################
create table cdt_user_mail_subs
  (
    user_id integer ,
    mail_subs_id integer
  ) in curdlydbs1 extent size 80000 next size 1000 lock mode row;
revoke all on cdt_user_mail_subs from public;
create unique index cdt_user_mail_subs_i1 on cdt_user_mail_subs (user_id,mail_subs_id)
using btree  in curdlyidxdbs1;
alter table cdt_user_mail_subs add constraint primary key (user_id,mail_subs_id)
constraint pk_cdt_user_mail_subs;
--############################################
create table cur_dly_ord_card
  (
    ord_id char(10),
    shop_card_tx char(13)
  ) in curdlydbs1 extent size 1000 next size 1000 lock mode row;
revoke all on cur_dly_ord_card from "public";
create unique index cur_dly_ord_card1 on cur_dly_ord_card
    (ord_id)  fillfactor 100 in curdlyidxdbs1 ;
alter table cur_dly_ord_card add constraint primary key
    (ord_id) constraint cur_dly_ord_card1;
--####################################
create table cdt_mail_job_ctl
  (
    job_id integer,
    job_name_tx varchar(35),
    job_desc_tx varchar(255),
    targ_name_tx varchar(25),
    targ_file_name_tx varchar(50),
    targ_type_cd smallint,
    url_tx varchar(100),
    subj_tx varchar(60),
    from_tx varchar(60),
    rply_mail_tx varchar(60),
    run_type_cd smallint,
    run_arg_tx varchar(35),
    job_trk_cd smallint ,
    send_max_qy integer ,
    job_cat_cd integer,
    schd_type_cd smallint,
    days_btwn_run_qy smallint,
    qa_targ_name_tx varchar(25),
    qa_send_fl smallint,
    mnt_targ_name_tx varchar(25),
    mnt_send_fl smallint,
    actv_fl smallint,
    arc_fl smallint,
    lim_num_dlv_fl smallint,
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id char(8),
    audt_upd_dt_tm datetime year to second
  ) in curdlydbs1 extent size 512 next size 512 lock mode row;
revoke all on cdt_mail_job_ctl from "public" ;
create unique index cdt_mail_job_ctl_i1 on cdt_mail_job_ctl
    (job_id) using btree  in curdlyidxdbs1;
create unique index cdt_mail_job_ctl_i2 on cdt_mail_job_ctl
    (job_name_tx) using btree in curdlyidxdbs1;
--####################################
create table cdt_mail_job_trk
  (
    job_id integer,
    bch_id integer,
    sent_dt_tm datetime year to second,
    strt_dt_tm datetime year to second,
    end_dt_tm datetime year to second,
    to_send_qy integer,
    sent_qy integer,
    not_sent_qy integer,
    sent_fl smallint
  ) in curdlydbs1  extent size 2000 next size 2000 lock mode row;
revoke all on cdt_mail_job_trk from "public";
create unique index cdt_mail_job_trk_i1 on cdt_mail_job_trk
    (job_id,bch_id) using btree  in curdlyidxdbs1;
--####################################
create table cdt_mail_user_trk
  (
    job_id integer,
    user_id integer,
    bch_id integer,
    sent_dt_tm datetime year to second,
    sent_fl smallint
  ) in curdlydbs1 extent size 4000 next size 4000 lock mode row;
revoke all on cdt_mail_user_trk from "public" ;
create unique index cdt_mail_user_trk_i1 on cdt_mail_user_trk
    (job_id,user_id,bch_id) using btree  in curdlyidxdbs1;
--####################################
CREATE  table cdt_it_ntrn_fl
   (
        prod_id integer,
        kshr_cd         char(1),
        orgn_cd         char(1),
        parv_cd         char(1),
        pnut_fl         char(1),
        gltn_fl         char(1),
        cmh_dtry_fl     smallint,
        ww_fl           smallint,
        stsh_hlth_fl    smallint,
        harv_fl         smallint,
        kshr_1_id       smallint,
        kshr_2_id       smallint,
        kshr_3_id       smallint,
        kshr_4_id       smallint,
        ctry_of_orig    varchar(255),
        ctry_of_orig_fl char(1),
        egg_fl          char(1),
        dary_fl         char(1),
        src_cd          char(15),
        audt_upd_id     char(8),
        audt_upd_dt     date
   ) in curdlydbs1 extent size 1024 next size 128 lock mode row;
revoke all on cdt_it_ntrn_fl from "public";
CREATE unique index cdt_it_ntrn_fl_pk on cdt_it_ntrn_fl(prod_id) in curdlyidxdbs1;
alter  table cdt_it_ntrn_fl ADD constraint primary key (prod_id) constraint pk_cdt_it_ntrn_fl;
--####################################
create table cdt_mrch_po
(
  po_num_tx varchar(12) NOT NULL,
  ord_id CHAR(10),
  list_id INTEGER
) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_mrch_po from public;
create unique index cdt_mrch_po1 ON cdt_mrch_po (po_num_tx)  in curdlyidxdbs1;
--####################################
create table cdt_offr_cpn_ctl
  (
    cpn_cd varchar(30),
    offr_cd varchar(30) ,
    ecom_stor_id integer,
    cpn_type_cd char(3),
    cont_id integer,
    strt_dt_tm datetime year to second,
    end_dt_tm datetime year to second,
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id char(8) ,
    audt_upd_dt_tm datetime year to second
  ) in curdlydbs1 extent size 4000 next size 2000 lock mode row;
revoke all on cdt_offr_cpn_ctl from "public";
create unique index cdt_offr_cpn_ct_pk on cdt_offr_cpn_ctl
    (cpn_cd) in curdlyidxdbs1;
alter table cdt_offr_cpn_ctl add constraint primary
    key (cpn_cd) constraint pk_cdt_offr_cpn_ct;
--####################################
create table cdt_offr_cpn_qlfy
  (
    cpn_qlfy_cd varchar(30),
    cpn_cd varchar(30),
    obj_name_tx varchar(40),
    key_prop_cd varchar(60),
    key_prop_opr_cd varchar(20),
    key_prop_val_cd varchar(60),
    next_cpn_qlfy_cd varchar(30),
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id char(8),
    audt_upd_dt_tm datetime year to second
  ) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_offr_cpn_qlfy from "public";
create unique index cdt_offr_cpn_ql_pk on cdt_offr_cpn_qlfy
    (cpn_qlfy_cd) fillfactor 100 in curdlyidxdbs1 ;
alter table cdt_offr_cpn_qlfy add constraint primary
    key (cpn_qlfy_cd) constraint pk_cdt_offr_cpn_ql;
--####################################
--####################################
create table cdt_offr_ctl
  (
    offr_cd varchar(30),
    prgm_id integer,
    desc_tx varchar(50),
    web_disp_tx varchar(35),
    rdm_type_cd varchar(6),
    tot_allw_qy smallint,
    tot_rdm_qy smallint,
    user_allw_qy smallint,
    amt_offr_qy decimal(7,4),
    prod_id integer,
    prod_grp_id integer,
    it_cd char(5),
    offr_tax_cd char(1),
    actv_stat_cd char(1),
    cmnc_veh_cd varchar(15),
    est_rdm_qy smallint,
    lbly_per_rdm_qy decimal(5,2),
    cont_id integer,
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id char(8),
    audt_upd_dt_tm datetime year to second
  ) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_offr_ctl from "public";
create unique index cdt_offr_ctl_pk on cdt_offr_ctl
    (offr_cd) in curdlyidxdbs1;
alter table cdt_offr_ctl add constraint primary key
    (offr_cd) constraint pk_cdt_offr_ctl;
--####################################
create table cdt_offr_evnt_ctl
  (
    evnt_id integer,
    java_evnt_lstn_cd varchar(255),
    qlfy_cd varchar(30),
    ecom_stor_id integer,
    strt_dt_tm datetime year to second,
    end_dt_tm datetime year to second,
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id char(8),
    audt_upd_dt_tm datetime year to second
  ) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_offr_evnt_ctl from "public";
create unique index cdt_offr_evnt_c_pk on cdt_offr_evnt_ctl
    (evnt_id) in curdlyidxdbs1;
alter table cdt_offr_evnt_ctl add constraint primary
    key (evnt_id) constraint pk_cdt_offr_evnt_c;
--####################################
create table cdt_offr_evnt_qlfy
  (
    qlfy_cd varchar(30),
    offr_cd varchar(30),
    obj_name_tx varchar(40),
    key_prop_cd varchar(40),
    key_prop_opr_cd varchar(40),
    key_prop_val_cd varchar(40),
    next_qlfy_cd varchar(30),
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id char(8),
    audt_upd_dt_tm datetime year to second
  ) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_offr_evnt_qlfy from "public";
create unique index cdt_offr_evnt_q_pk on cdt_offr_evnt_qlfy
    (qlfy_cd) in curdlyidxdbs1;
alter table cdt_offr_evnt_qlfy add constraint primary
    key (qlfy_cd) constraint pk_cdt_offr_evnt_q;
--####################################
create table cdt_offr_mktg_prgm
  (
    prgm_id integer,
    name_tx varchar(50),
    desc_tx varchar(200),
    ownr_last_name_tx varchar(30),
    targ_cd varchar(15),
    clss_cd varchar(15),
    prgm_strt_dt date,
    prgm_end_dt date,
    bdgt_mtrl_cost_qy decimal(9,2),
    bdgt_lbly_cost_qy decimal(9,2),
    actl_mtrl_cost_qy decimal(9,2),
    actl_lbly_cost_qy decimal(9,2),
    prgm_vald_cd char(1),
    chrt_exst_cd char(1),
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id char(8),
    audt_upd_dt_tm datetime year to second
  ) in curdlydbs1 extent size 2000 next size 2000 lock mode row;
revoke all on cdt_offr_mktg_prgm from "public";
create unique index cdt_offr_mktg_p_pk on cdt_offr_mktg_prgm
    (prgm_id) fillfactor 100 in curdlyidxdbs1;
alter table cdt_offr_mktg_prgm add constraint primary
    key (prgm_id) constraint pk_cdt_offr_mktg_p ;
--####################################
create table cdt_offr_trk
  (
    user_id integer,
    list_id integer,
    offr_cd varchar(30),
    rdm_dt_tm datetime year to second,
    rdm_cd varchar(30) ,
    rdm_amt_qy decimal(7,4),
    it_cd char(5),
    ord_id char(15),
    prod_id integer,
    evnt_id integer,
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id char(8),
    audt_upd_dt_tm datetime year to second
  ) in curdlydbs1 extent size 200000 next size 40000 lock mode row;
revoke all on cdt_offr_trk from "public";
create unique index cdt_offr_trk_pk on cdt_offr_trk
    (user_id,list_id,offr_cd) in curdlyidxdbs1;
alter table cdt_offr_trk add constraint primary key
    (user_id,list_id,offr_cd) constraint pk_cdt_offr_trk ;
--####################################
create table cdt_pmtt_resp_ctl
  (
    resp_cd char(3),
    resp_type_cd char(4),
    desc_tx varchar(100),
    act_cd char(2),
    web_msg_tx varchar(255),
    web_rtrn_page_cd char(4),
    cf_cd char(3)
  ) in curdlydbs1 extent size 128 next size 128 lock mode row;
revoke all on cdt_pmtt_resp_ctl from "public";
create unique index cdt_pmtt_resp_c_pk on cdt_pmtt_resp_ctl
    (resp_cd,resp_type_cd) using btree  in curdlyidxdbs1;
alter table cdt_pmtt_resp_ctl add constraint primary
    key (resp_cd,resp_type_cd) constraint pk_cdt_pmtt_resp_c;
--####################################
create table cdt_pmtt_trk_rtrn
  (
    pmtt_id integer not null ,
    user_id integer not null ,
    list_id integer,
    prgm_name_cd char(8),
    trk_audt_cr_dt_tm datetime year to second,
    mrch_ord_nbr_tx char(16),
    resp_rsn_cd char(3),
    resp_mmddyy_tx char(6),
    auth_cd char(6),
    avs_cd char(2),
    card_sec_resp_cd char(1),
    acct_nbr_tx char(19),
    exp_mmyy_tx char(4),
    meth_of_pay_cd char(2),
    rcur_pay_cd char(2),
    cur_bal_qy decimal(10,2),
    prev_bal_qy decimal(10,2),
    rtrn_audt_cr_dt_tm datetime year to second
  ) in curdlydbs1 extent size 8000 next size 8000 lock mode row;
revoke all on cdt_pmtt_trk_rtrn from "public";
create unique index cdt_pmtt_trk_rt_pk on cdt_pmtt_trk_rtrn
    (pmtt_id) using btree  in curdlyidxdbs1;
alter table cdt_pmtt_trk_rtrn add constraint primary key
    (pmtt_id) constraint pk_cdt_pmtt_trk_rt;
--####################################
create table cdt_web_page_cont
    (web_page_cont_id integer,
    ecom_stor_id integer,
    seq_id smallint,
    page_desc_tx varchar(50),
    page_file_tx varchar(255),
    parm_type_cd char(1),
    parm_tx varchar(70),
    actv_cd char(1),
    strt_dt_tm datetime year to minute,
    end_dt_tm datetime year to minute,
    audt_upd_id char(8),
    audt_upd_dt date,
    link_type_cd varchar(20)
  ) in curdlydbs1  extent size 1024 next size 1024 lock mode row;
revoke all on cdt_web_page_cont from public;
create unique index cdt_web_page_co_pk on cdt_web_page_cont
    (web_page_cont_id) fillfactor 100 in curdlyidxdbs1;
alter table cdt_web_page_cont add constraint primary
    key (web_page_cont_id) constraint pk_cdt_web_page_co;
--##############################
create table cdt_emp_hdr
  (
    ee_id char(11),
    dc_id char(6),
    ee_name_tx varchar(25),
    alpt_user_id varchar(16)
  )  in curdlydbs1 extent size 1000 next size 500 lock mode row;
revoke all on cdt_emp_hdr from "public";
create unique index cdt_emp_hdr_pk on cdt_emp_hdr
    (ee_id,dc_id) using btree in curdlyidxdbs1;
alter table cdt_emp_hdr add constraint primary key (ee_id,dc_id)
    constraint cdt_emp_hdr_pk;
--##############################
create table cdt_x_it
  (
    stor_id char(7),
    pod_id integer,
    upc_cd decimal(14,0),
    sku_1_cd integer,
    sku_2_cd integer,
    hi_cone_cd char(1),
    unit_scal_qy decimal(7,2),
    pr_scal_qy decimal(7,4),
    pr_meth_cd char(2),
    pr_src_cd char(1),
    pr_qy decimal(6,2),
    it_tax_qy decimal(7,5),
    spec_cd char(1),
    loc_cd char(9),
    it_shop_cd char(1),
    actv_stat_cd char(1),
    old_actv_stat_cd char(1),
    actv_stat_upd_dt date,
    supl_cd char(3),
    btl_dep_qy decimal(4,2),
    sub_pod_id integer,
    sub_qy smallint,
    reg_pr_qy decimal(6,2),
    on_hand_qy integer,
    avg_day_sold_qy integer,
    mgrp_stat_cd char(1),
    it_long_name_tx varchar(100),
    it_size_cd varchar(11),
    it_dict_cat_id integer,
    majr_cat_id integer,
    uprm_actv_cd char(1),
    audt_cr_id char(8),
    audt_cr_dt date,
    audt_upd_id char(8),
    audt_upd_dt date
  ) in curdlydbs1 extent size 20000 next size 8000 lock mode row;
revoke all on cdt_x_it from "public";
create unique index cdt_x_it_pk on cdt_x_it (pod_id, stor_id)
        fillfactor 100 in curdlyidxdbs1;
alter table cdt_x_it add constraint primary key
    (pod_id, stor_id) constraint pk_cdt_x_it ;
