#!/bin/bash

# Time in minute to consider recent
RCNMIN=500
# Time in minute to consider recent on running file
RCNRUNMIN=100
KDIR=/opt/pentaho/data-integration
#KDIR=/Applications/pentaho503/design-tools/data-integration
TESTDIR=/data/pentaho/requests/mlti_shtl
CDIR=/code/pentaho/manhattan/mlti_shtl
ARCHDIR=$TESTDIR/archive
LDIR=/logs/pentaho/manhattan/mlti_shtl
BOOLEAN=N

cd $TESTDIR
FILE=run.txt
RUNFILE=run_is_running.txt

RCNTFILE=`find $FILE -mmin -$RCNMIN -print 2>/dev/null` 
RCNRUNFILE=`find $RUNFILE -mmin -$RCNRUNMIN -print 2> /dev/null`

if [[ -e "$FILE" && "$FILE" = "$RCNTFILE" ]]
then
   BOOLEAN=Y 
elif [ -e "$FILE" ]
then
   rm -r $TESTDIR/$FILE;
   exit 0;
else
   exit 1;
fi

if [[ -e "$RUNFILE" && "$RUNFILE" = "$RCNRUNFILE" ]] 
then
   if [ -e "$FILE" ] 
   then
       mv $TESTDIR/$FILE $ARCHDIR/$FILE.`date +'%Y%m%d_%H%M%S'`.cancel
   fi
   exit 2;
fi

if [ "$BOOLEAN" = "Y" ]
then 
  mv $TESTDIR/$FILE $TESTDIR/$RUNFILE
  STORE=`cat $TESTDIR/$RUNFILE|sed -n '1s/.*://p'`;
  EMAILS=`cat $TESTDIR/$RUNFILE|sed -n '2s/.*://p'`;

  cd $KDIR
  ./kitchen.sh -file="$CDIR/main_mlti.kjb" -param:STOR="$STORE" -param:MAIL="$EMAILS" -Level=Basic > $LDIR/log.main_mlti 2 >&1 
  echo $STORE
  echo $EMAILS
  mv $TESTDIR/$RUNFILE $ARCHDIR/$RUNFILE.`date +'%Y%m%d_%H%M%S'`.complete
fi
