{
 # If time field id bad, just skip the row.  Too hard to fix!
 if (length($2) == 8)  {

  # If date field length is OK, do not change it.
  if (length($1) == 10)
   FIX_DT=$1;

  # If date field is too big, fix common problems.
  if (length($1) > 10)  {
   # 0004/26/2003
   # 04/0026/2003
   split($1,DT,"/");
   FIX_DT=sprintf("%2.2d/%2.2d/%4.4d",DT[1],DT[2],DT[3]);
  }

  # Examine FIND RESULTS column for escape characters.
  # These are ignored by app, so they are valid, but Informix will not load.
  i=index($8,"\\"); 
  if (i==0) 
   FIX_COL8 = $8;
  else
   FIX_COL8 = sprintf("%s",substr($8,1,i-1));

  # Print the fixed up line
  printf("%s|%s|%s|%s|%s|%s|%s|%s|%d\n",FIX_DT,$2,$3,$4,$5,$6,$7,FIX_COL8,$9);
 }
}
