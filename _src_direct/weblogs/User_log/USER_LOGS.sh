#!/bin/ksh
#######################################################################
#
# SHELL NAME: USER_LOGS.sh
#
# History: 10/28/2011
# Description: This scrip is get log data according to define function cd 
#
#######################################################################
CODEDIR=${CODEDIR_BASE}${USER_LOG}
DATADIR=${DATADIR_BASE}${USER_LOG}
FULLFILE=${DATADIR}user_log.ful
ULOGFILE=${DATADIR}user_log.unl
UBUYFILE=${DATADIR}user_log_buy.unl

rm -f ${FULLFILE}
rm -f ${ULOGFILE}
rm -f ${UBUYFILE}

echo "cating to dynamo file to ${FULLFILE}"
cat ${DATADIR}dynamologs.* >> ${FULLFILE}

echo "  Compressing..."
gzip ${DATADIR}dynamo*.*

echo "  Moving to SAVE directory..."
mv  ${DATADIR}dynamo*.*.gz ${DATADIR}SAVE/.

date;
echo "Filtering user log files - only certain logs and only where time string is correct length.";
cat ${FULLFILE} |awk -F\| -f ${CODEDIR}user_log_clean.awk |awk -F\| -f ${CODEDIR}user_log_filter.awk > ${ULOGFILE}
echo "----------------------------------";

date;
echo "Filtering user log files - BUY only";
cat ${FULLFILE} | awk -F\| -f ${CODEDIR}user_log_clean.awk |awk -F\| -f ${CODEDIR}user_log_buy_filter.awk > ${UBUYFILE};
echo "----------------------------------";

date;
echo "End of job.";
