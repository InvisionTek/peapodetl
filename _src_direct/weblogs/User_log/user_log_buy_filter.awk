BEGIN {
 #
 # Make sure to update web_user_log_ctl with any changes!!!!
 #
 FUNC_TX[57] = "AD_BUY"
 FUNC_TX[58] = "CONTENT_BUY"
}
{
 # For every line...
 FUNC_CD=0;
 for (i=57; i<=58; i++)  {
  if ($6 == FUNC_TX[i])  {
   FUNC_CD=i;
   break;
  }
 }
if (FUNC_CD > 0)  {
   
   ##Data in this phase example
   ##04/28/2003|16:01:04|5599128|420200|10|58|12244|234,2,*,2|14
   col8=split($8,col,",");
   newcol1=sprintf("%s|%s|%s|%s",col[1],col[2],col[3],col[4]);
   
   ##Data in this phase example
   ##04/28/2003|16:01:04|5599128|420200|10|57|12244|234|2|*|2|14
   printf("%s|%s|%s|%s|%s|%s|%s|%s|%s\n",$1,$2,$3,$4,$5,FUNC_CD,$7,newcol1,$9);
 }
}
