#!/bin/bash

KDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/stardb
LDIR=/logs/pentaho/stardb
i=40;

while [ $i -le 40 -a $i -ge 0 ];
do
date;
echo "running for $i days"
cd $KDIR
./kitchen.sh -file="$CDIR/jb_main_vol_star1.kjb" -param:DAYS="$i" -Level=Basic > $LDIR/log.run_dly_vol_star1 2 >&1
((i-=1));
done
