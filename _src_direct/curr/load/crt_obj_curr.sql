create table cur_acct_bal
  (
    cnsm_id char(8) not null,
    ord_id char(10) not null,
    ord_dt date,
    line_id smallint,
    it_cd char(15),
    it_name_tx char(35),
    ord_qy decimal(10),
    pr_qy decimal(12),
    dbt_crdt_cd char(2),
    acct_stmt_blck_cd smallint,
    pmt_meth_cd char(6),
    pmt_meth_id varchar(64),
    bill_cd char(1),
    used_cd char(1),
    used_ord_id char(10),
    xfer_cd char(1),
    xfer_dt date,
    adjt_pr_qy decimal(12)
  ) in curdbs2 extent size 400000 next size 20000 lock mode row ;
revoke all on cur_acct_bal from "public";
create index cur_acct_bal_idx1 on cur_acct_bal (cnsm_id)  fillfactor 100 in curidxdbs2;
create index cur_acct_bal_idx2 on cur_acct_bal (ord_Id)  fillfactor 100 in curidxdbs2;
--###################################
create table cur_cmpy_card_asgn
  (
    cmpy_id integer not null,
    card_id char(35) not null,
    cnsm_id char(8),
    asgn_dt date
  ) in curdbs2 extent size 7000 next size 1000 lock mode row;
revoke all on cur_cmpy_card_asgn from "public";

create unique index cur_cmpy_crd_asgn1 on cur_cmpy_card_asgn
    (cmpy_id, card_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_cmpy_card_asgn add constraint primary key
    (cmpy_id, card_id) constraint cur_cmpy_crd_asgn1  ;
--####################################
create table cur_cmpy_hdr
  (
    cmpy_id integer not null ,
    cmpy_tx char(40),
    adr_1_tx char(30),
    adr_2_tx char(30),
    city_tx char(25),
    st_cd char(2),
    zip_cd char(10),
    idx_cd char(15)
  ) in curdbs2 lock mode row;
revoke all on cur_cmpy_hdr from "public";
create unique index cur_cmpy_hdr_pk on cur_cmpy_hdr
    (cmpy_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_cmpy_hdr add constraint primary key
    (cmpy_id) constraint cur_cmpy_hdr_pk  ;
--####################################
create table cur_cnsm_dlv
  (
    cnsm_id char(8),
    last_name_tx char(30),
    frst_name_tx char(20),
    mi_tx char(1),
    adr_1_tx char(30),
    adr_2_tx char(30),
    city_tx char(20),
    st_cd char(2),
    zip_cd char(10),
    day_phon_cd char(13),
    day_phon_ext_cd char(4),
    map_cd char(5),
    map_sect_cd char(1),
    istr_1_tx char(60),
    istr_2_tx char(60),
    istr_3_tx char(60),
    istr_oth_tx char(60),
    isct_tx char(25),
    ew_str_tx char(24),
    ns_str_tx char(24),
    nrth_1_cd char(2),
    east_1_cd char(2),
    east_2_cd char(2),
    sth_1_cd char(2),
    sth_2_cd char(2),
    west_1_cd char(2),
    west_2_cd char(2),
    nrth_2_cd char(2),
    dlv_bld_tx char(10),
    dlv_desc_tx char(20),
    park_tx char(15),
    door_tx char(15),
    cart_cd char(1),
    hand_1_tx char(60),
    hand_2_tx char(60),
    hand_3_tx char(60),
    shop_1_tx char(60),
    shop_2_tx char(60),
    shop_3_tx char(60)
  ) in curdbs2 extent size 540000 next size 20000 lock mode row;
revoke all on cur_cnsm_dlv from "public";
create unique index cur_cnsm_dlv_pk on cur_cnsm_dlv
    (cnsm_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_cnsm_dlv add constraint primary key
    (cnsm_id) constraint cur_cnsm_dlv_pk  ;
create index cur_cnsm_dlv_idx1 on cur_cnsm_dlv (last_name_tx,
    frst_name_tx)     fillfactor 100 in curidxdbs2;
--#######################################
create table cur_cnsm_hdr
  (
    cnsm_id char(8),
    last_name_tx varchar(30),
    frst_name_tx varchar(20),
    mi_tx char(1),
    ttl_tx char(6),
    adr_1_tx varchar(30),
    adr_2_tx varchar(30),
    city_tx varchar(20),
    st_cd char(2),
    zip_cd char(10),
    home_phon_cd char(13),
    work_phon_cd char(13),
    work_phon_ext_cd char(4),
    stor_id char(7),
    cnsm_dt date,
    mbr_fee_freq_cd char(1),
    cnsm_exp_dt date,
    pmt_meth_cd char(6),
    crdt_card_id varchar(32),
    crdt_card_exp_dt char(5),
    crdt_card_cnsm_tx char(20),
    chk_acct_id varchar(64),
    chk_acct_rtng_id varchar(9),
    bank_tx varchar(25),
    tax_cd char(1),
    ord_lim_qy decimal(10),
    ar_acct_dflt_cd integer,
    ar_dept_dflt_cd char(3),
    acct_bal_qy decimal(10),
    on_acct_qy decimal(10),
    stat_cd char(1),
    type_cd char(1),
    cptr_cd char(1),
    phon_pswd_tx varchar(15),
    ds_rate_cd char(1),
    allw_smpl_tx char(1),
    time_ord_qy smallint,
    last_mnth_fee_dt date,
    last_ord_dt date,
    cnsm_alrt_qy smallint
  ) in curdbs2 extent size 210000 next size 20000 lock mode row ;
revoke all on cur_cnsm_hdr from "public";
create unique index cur_cnsm_hdr_pk on cur_cnsm_hdr
    (cnsm_id)   fillfactor 100 in curidxdbs2 ;
alter table cur_cnsm_hdr add constraint primary key
    (cnsm_id) constraint cur_cnsm_hdr_pk;
create index cur_cnsm_hdr_idx1 on cur_cnsm_hdr (last_name_tx)   fillfactor 100 in curidxdbs2;
--###############################################
create table cur_cnsm_rte
  (
    cnsm_id char(8) not null ,
    last_sent_adr1_tx varchar(30) not null ,
    last_sent_city_tx varchar(20) not null ,
    last_sent_st_cd varchar(2) not null ,
    last_sent_zip_cd varchar(10) not null ,
    ovrd_lat_qy float,
    ovrd_long_qy float,
    ovrd_stop_min_qy smallint,
    lrnd_stop_min_qy smallint,
    num_ord_avgd_qy smallint,
    hold_stop_min_qy decimal(6,2)
  ) in curdbs2  extent size 64000 next size 4000 lock mode row;
revoke all on cur_cnsm_rte from "public";
create unique index cur_cnsm_rte1 on cur_cnsm_rte
    (cnsm_id) using btree  in curidxdbs2 ;
alter table cur_cnsm_rte add constraint primary key
    (cnsm_id) constraint cur_cnsm_rte_pk  ;
--#######################################
create table cur_cont_ctl
  (
    cont_id integer not null ,
    app_type_cd char(4) not null ,
    file_ext_cd char(8),
    file_loc_cd char(1),
    cont_name_tx char(40),
    file_name_tx char(256),
    cont_ttl_tx char(40),
    src_cd char(15),
    audt_cr_id char(8),
    audt_cr_dt date,
    audt_upd_id char(8),
    audt_upd_dt date
  ) in curdbs2 extent size 160000 next size 8000 lock mode row ;
revoke all on cur_cont_ctl from "public";
create unique index cur_cont_ctl_idx1 on cur_cont_ctl
    (cont_id,app_type_cd)    fillfactor 100 in curidxdbs2;
--#############################
create table cur_cpn_calc
  (
    ord_id char(10),
    cpn_calc_qy decimal(12),
    it_cpn_cd char(5),
    mfg_cmpy_cd integer,
    fam_cd smallint,
    pod_id integer,
    it_tax_qy decimal(5,5),
    cpn_upc_cd decimal(13,0),
    it_qy smallint,
    min_it_for_cpn_qy smallint,
    tax_rfnd_qy decimal(6,2),
    tax_levl_cd char(10),
    mfg_rtlr_cd char(1),
    tax_cd char(1)
  ) in curdbs2 extent size 81920 next size 5120 lock mode row;
revoke all on cur_cpn_calc from "public";
create index cur_cpn_calc_idx1 on cur_cpn_calc
    (ord_id) using btree  in curidxdbs2;
--###################################
create table cur_hypr_cnvt_ctl
  (
    hypr_city_cd        char(1),
    hypr_cat_cd         char(4),
    dbt_crdt_cd         char(2),
    hypr_acct_no        char(5),
    hypr_dept_no        char(4),
    hypr_loc_no         char(4),
    hypr_prod_line_cd   char(2)
  ) in curdbs2  extent size 32 next size 32 lock mode row;
revoke all on cur_hypr_cnvt_ctl from public;
create index cur_hypr_cnvt_ctl_idx1 on cur_hypr_cnvt_ctl
    (hypr_city_cd) using btree  in curidxdbs2;
create index cur_hypr_cnvt_ctl_idx2 on cur_hypr_cnvt_ctl
    (hypr_cat_cd) using btree  in curidxdbs2;
--#############################
create table cur_it_invt
  (
    it_cd char(20),
    it_name_tx char(35),
    hypr_cat_cd char(4),
    desc_tx char(50)
  ) in curdbs2 extent size 1000 next size 1000 lock mode row ;
revoke all on cur_it_invt from "public";
create unique index cur_it_invt_pk on cur_it_invt
    (it_cd)  fillfactor 100 in curidxdbs2 ;
alter table cur_it_invt add constraint primary key
    (it_cd) constraint cur_it_invt_pk  ;
--######################################
create table cur_misc_look
  (
    look_id integer not null ,
    look_grp_id smallint,
    type_cd char(15),
    type_id integer,
    look_word_tx char(30)
  ) in curdbs2 extent size 1000 next size 1000 lock mode row;
revoke all on cur_misc_look from "public";
create unique index cur_misc_look_pk on cur_misc_look
    (look_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_misc_look add constraint primary key
    (look_id) constraint cur_misc_look_pk  ;
create index cur_misc_look_idx1 on cur_misc_look (look_word_tx)   fillfactor 100 in curidxdbs2;
create index cur_misc_look_idx2 on cur_misc_look (type_cd)    fillfactor 100 in curidxdbs2;
--###############################################
create table cur_msg_hdr
  (
    msg_id integer not null ,
    app_type_cd char(4) not null,
    ttl_tx char(60),
    desc_tx char(60),
    type_cd char(15),
    proj_id char(20),
    col_incl_cd char(1),
    msg_text_bx text,
    img_file_tx char(20)
  ) in curdbs2 extent size 9000 next size 2000 lock mode row;
revoke all on cur_msg_hdr from "public";
create unique index cur_msg_hdr_pk on cur_msg_hdr
    (msg_id,app_type_cd)  fillfactor 100 in curidxdbs2 ;
alter table cur_msg_hdr add constraint primary key
    (msg_id,app_type_cd) constraint cur_msg_hdr_pk  ;
--##################################################
create table cur_oos_data
(
        stor_id char(7),
        pod_id integer,
        tot_sale decimal(6,2),
        num_ord smallint,
        num_oos smallint,
        begn_dt date,
        end_dt date
) in curdbs2 extent size 8000 next size 2000 lock mode row;

revoke all on cur_oos_data from "public";


create unique index cur_oos_data_pk on cur_oos_data
    (stor_id, pod_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_oos_data add constraint primary key
    (stor_id, pod_id) constraint cur_oos_data_pk  ;
--#####################################################
create table cur_ord_dtl
  (
    ord_id char(10),
    line_id smallint,
    tax_cd char(1),
    it_cd char(6),
    it_name_tx char(30),
    ord_qy decimal(10),
    pr_qy decimal(10),
    xfer_cd char(1),
    xfer_dt date
  ) in curdbs2 extent size 38000 next size 10000 lock mode row;
revoke all on cur_ord_dtl from "public";

create index cur_ord_dtl_idx1 on cur_ord_dtl
    (ord_id,line_id)  fillfactor 100 in curidxdbs2 ;
--##############################################################
create table cur_ord_hdr
  (
    ord_id char(10),
    orig_ord_id char(10),
    cnsm_id char(8),
    mbr_id smallint,
    stor_id char(7),
    last_name_tx char(30),
    frst_name_tx char(20),
    adr_1_tx char(30),
    adr_2_tx char(30),
    city_tx char(20),
    st_cd char(2),
    zip_cd char(10),
    ord_type_cd char(3),
    ord_desc_tx char(30),
    ord_dt date,
    dlv_dt date,
    rqst_dt date,
    rqst_tm datetime hour to minute,
    shop_ee_id char(10),
    dlv_ee_id char(10),
    prdc_ee_id char(10),
    pmt_meth_cd char(6),
    pmt_meth_id varchar(64),
    slot_id integer,
    ord_plc_cd char(3),
    dlv_type_cd char(1),
    note_1_tx char(60),
    note_2_tx char(60),
    note_3_tx char(60),
    prt_pick_cd char(1),
    ord_stat_cd char(1),
    ord_cond_cd char(1),
    post_cd char(1),
    post_id integer,
    pmt_qy decimal(10),
    ord_it_qy decimal(10),
    ord_st_tx_qy decimal(10),
    prev_qy decimal(10),
    over_undr_qy decimal(10),
    ord_tot_qy decimal(10),
    elec_pmt_xfer_cd char(1),
    audt_cr_dt date,
    audt_cr_tm char(8),
    audt_cr_id char(10),
    audt_upd_dt date,
    audt_upd_tm char(8),
    audt_upd_id char(10),
    xfer_cd char(1),
    xfer_dt date
  ) in curdbs2 extent size 220000 next size    20000 lock mode row ;
revoke all on cur_ord_hdr from "public";
create unique index cur_ord_hdr_pk on cur_ord_hdr
    (ord_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_ord_hdr add constraint primary key
    (ord_id) constraint cur_ord_hdr_pk  ;
create index cur_ord_hdr_idx1 on cur_ord_hdr (cnsm_id)    fillfactor 100 in curidxdbs2 ;
create index cur_ord_hdr_idx2 on cur_ord_hdr (dlv_dt)    fillfactor 100 in curidxdbs2 ;
create index cur_ord_hdr_idx3 on cur_ord_hdr (ord_dt)   fillfactor 100 in curidxdbs2 ;
--#####################################################
create table cur_ord_it
  (
    ord_id char(10) not null,
    pod_id integer not null,
    upc_cd decimal(14,0),
    it_qy smallint,
    cmt_tx char(35),
    it_pr_qy decimal(6,2),
    it_tax_qy decimal(4,3),
    spec_cd char(1),
    it_loc_cd char(9),
    out_qy smallint,
    sbst_made_cd char(1),
    back_room_cd char(1),
    shlf_tag_cd char(1),
    it_cpn_flg_cd char(1),
    it_size_cd char(11),
    it_uom_cd char(6),
    unit_scal_qy decimal(7,2),
    pr_scal_qy decimal(7,4)
  ) in curdbs2 extent size 250000 next size 10000 lock mode row;
revoke all on cur_ord_it from "public";
create unique index cur_ord_it_pk on cur_ord_it
    (ord_id,pod_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_ord_it add constraint primary key
    (ord_id,pod_id) constraint cur_ord_it_pk  ;
create index cur_ord_it_idx1 on cur_ord_it (pod_id)   fillfactor 100 in curidxdbs2;
--######################################################################
create table cur_rfr_cpn_id
(
cpn_id          CHAR(7) NOT NULL,
issu_dt         DATE NOT NULL,
new_log_id              CHAR(8) NOT NULL,
new_e_mail_adr_tx       CHAR(100) NOT NULL,
rfr_e_mail_adr_tx       CHAR(100) NOT NULL,
new_ord_id              CHAR(10),
e_mail_send_cd  CHAR(1) NOT NULL,
e_mail_send_dt  DATE,
crdt_rdm_cd             CHAR(1) NOT NULL ,
crdt_log_id             CHAR(8),
crdt_ord_id             CHAR(10),
crdt_rdm_dt             DATE
) in curdbs2 extent size 60000 next size 4000 lock mode row;


create unique index cur_rfr_cpn_id_pk on cur_rfr_cpn_id
    (cpn_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_rfr_cpn_id add constraint primary key
    (cpn_id) constraint cur_rfr_cpn_id_pk  ;
--##########################a
create table cur_shop_ctl
  (
    main_stor_id char(7) not null ,
    it_shop_cd char(1) not null ,
    it_dnld_cd char(1),
    audt_upd_id char(10),
    audt_upd_dt date,
    audt_upd_tm datetime hour to second
  ) in curdbs2 extent size 16 next size 16 lock mode row;
revoke all on cur_shop_ctl from "public";
create unique index cur_shop_ctl_idx1 on cur_shop_ctl
    (main_stor_id,it_shop_cd)  fillfactor 100 in curidxdbs2;
create index cur_shop_ctl_idx2 on cur_shop_ctl (main_stor_id)  fillfactor 100 in curidxdbs2;
--#########################################
create table cur_stor_zip
  (
    zip_city_id SERIAL,
    zip_cd CHAR(10) not null,
    city_tx CHAR(20) not null,
    st_cd CHAR(3) not null,
    stor_id CHAR(7) not null,
    mbr_type_cd CHAR(1),
    dlv_area_id SMALLINT not null,
    stop_time_cd SMALLINT not null,
    tax_grp_cd CHAR(3) not null,
    rpt_stor_id CHAR(4) not null,
    prom_pr_zone_cd smallint not null,
    base_pr_zone_cd smallint not null,
    time_diff_qy smallint
  ) in curdbs2 extent size 1024 next size 1024 lock mode row ;
revoke all on cur_stor_zip from "public";
create unique index cur_stor_zip_pk on cur_stor_zip
    (zip_city_id)  fillfactor 100  in curidxdbs2 ;
alter table cur_stor_zip add constraint primary key
    (zip_city_id) constraint cur_stor_zip_pk  ;
create index cur_stor_zip_idx1 on cur_stor_zip (zip_cd)   fillfactor 100 in curidxdbs2;
--#######################################################
create table cur_zone_ctl
  (
    stor_id char(7),
    chn_tx char(20),
    stor_nbr_cd char(20),
    stor_tx char(20),
    adr_1_tx char(20),
    adr_2_tx char(20),
    city_tx char(20),
    st_cd char(3),
    zip_cd char(10),
    zone_voic_phon_cd char(20),
    ms_voic_phon_cd char(20),
    ms_fax_phon_cd char(20),
    mdm_2400_phon_cd char(20),
    mdm_9600_phon_cd char(20),
    zone_beep_phon_cd char(13),
    mgr_ee_id char(10),
    tax_hi_qy decimal(8,6),
    tax_med_qy decimal(8,6),
    tax_low_qy decimal(8,6),
    zone_tab_tx char(20),
    max_pers_it_qy smallint,
    max_pers_hdr_qy smallint,
    mbr_exp_day_qy smallint,
    mbr_fee_freq_cd char(1),
    mbr_fee_freq_it_cd char(7),
    rcv_pmt_cd char(1),
    ds_qy decimal(4,3),
    ee_ds_qy decimal(4,3),
    groc_e_mail_tx char(35),
    stor_e_mail_tx char(35),
    path_id char(64),
    vend_cd char(10),
    ap_dept_cd char(3),
    ar_dept_cd char(3),
    groc_dept_cd char(3),
    cash_dept_cd char(3),
    zone_actv_cd char(1),
    type_cd char(1),
    pckp_avl_cd char(1),
    cnsm_pckp_allw_cd char(1),
    stor_drct_msg_id integer,
    stor_drct_bx text,
    inet_mail_allw_cd char(1),
    sess_bill_cd char(1),
    cnsm_max_ord_pr_qy decimal(8,2),
    mrch_max_ord_pr_qy decimal(8,2),
    hr_e_mail_tx char(35),
    pckp_msg_id integer,
    max_prev_ord_qy smallint,
    main_stor_id char(7),
    mdm_phon_audt_cd char(1),
    cnvt_800_to_cnc_cd char(1),
    bill_pr_msg_id integer,
    non_bill_pr_msg_id integer,
    mrch_pr_msg_id integer,
    hypr_dept_cd smallint,
    hypr_loc_cd smallint,
    hypr_acct_cd smallint,
    hypr_city_cd char(1),
    svc_modl_vrsn_cd char(6),
    cnsm_poi_avl_cd char(1),
    cnsm_pom_avl_cd char(1),
    cnsm_pon_avl_cd char(1),
    cnsm_poa_avl_cd char(1),
    mrch_poi_avl_cd char(1),
    mrch_pom_avl_cd char(1),
    mrch_pon_avl_cd char(1),
    mrch_poa_avl_cd char(1),
    ppod_by_chn_tx char(20),
    it_tot_tab_tx char(20),
    ops_modl_type_cd char(3),
    zm_sys_vrsn_cd char(6),
    actv_stat_cd char(1),
    slot_bx text,
    entr_ee_ord_cd char(1),
    it_cmt_type_cd char(10),
    btl_dep_cd char(1),
    co_type_cd char(4),
    sale_tax_cd char(1),
    rx_msg_id integer,
    rx_e_mail_id char(50),
    use_pr_disp_cd char(1),
    is_natl_zone_cd char(1),
    slot_modl_type_cd char(3),
    scan_var_wgt_cd char(1),
    mgrp_dc_id char(6),
    mgrp_bus_type_cd char(1),
    upd_mgrp_tote_cd char(1) not null,
    sub_flo_cd char(1),
    one_clip_site_id integer,
    cer_div_cd varchar(10),
    mult_tax_zone char(1) not null,
    pick_modl_type_cd varchar(10),
    opco_id char(4),
    rte_db_name_tx varchar(18),
    rte_est_tote_qy smallint,
    prom_strt_tm datetime hour to minute,
    prom_end_tm datetime hour to minute,
    fig_cd smallint not null ,
    nwtn_ops_live_dt date not null ,
    show_tote_on_invc_cd char(1) not null ,
    show_tip_on_invc_cd char(1) not null ,
    slot_labl_cd char(1) not null ,
    inv_aloc_in_proc_cd char(1) not null
  ) in curdbs2 extent size 256 next size 64 lock mode row ;
revoke all on cur_zone_ctl from "public";
create unique index cur_zone_ctl_pk on cur_zone_ctl
    (stor_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_zone_ctl add constraint primary key
    (stor_id) constraint cur_zone_ctl_pk  ;
--##################################################
create table cwt_caps_cnsm_info
  (
    caps_id integer not null ,
    cnsm_id char(8) not null ,
    pmt_meth_cd char(5) not null ,
    pmt_mask_id varchar(18),
    bank_tx varchar(25),
    crdt_card_cnsm_tx varchar(20),
    crdt_dnc_type_tx varchar(20),
    crdt_card_type_cd char(4),
    crdt_card_exp_dt char(5),
    gift_card_cd char(1),
    chk_acct_rtng_id varchar(9),
    stat_cd char(1),
    audt_cr_id varchar(12),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id varchar(12),
    audt_upd_dt_tm datetime year to second
  ) in curdbs2  extent size 10000 next size 10000 lock mode row;
revoke all on cwt_caps_cnsm_info from "public";
create unique index cwt_caps_cnsm_idx1 on cwt_caps_cnsm_info
    (caps_id) using btree  in curidxdbs2;
create index cwt_caps_cnsm_idx2 on cwt_caps_cnsm_info
    (cnsm_id,pmt_meth_cd) using btree  in curidxdbs2;
alter table cwt_caps_cnsm_info add constraint primary
    key (caps_id) constraint cwt_caps_cnsm_info_pk ;
--#######################################################
create table cwt_caps_ord_info
  (
    caps_id integer,
    ord_id char(10) not null ,
    cnsm_id char(8) not null ,
    pmt_meth_cd char(5) not null ,
    pmt_mask_id varchar(18),
    bank_tx varchar(25),
    crdt_card_cnsm_tx varchar(20),
    crdt_dnc_type_tx varchar(20),
    crdt_card_type_cd char(4),
    crdt_card_exp_dt char(5),
    gift_card_cd char(1),
    chk_acct_rtng_id varchar(9),
    stat_cd char(1),
    audt_cr_id varchar(12),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id varchar(12),
    audt_upd_dt_tm datetime year to second
  ) in curdbs2  extent size 6000 next size 3000 lock mode row;
revoke all on cwt_caps_ord_info from "public" ;
create unique index cwt_caps_ord_idx1 on cwt_caps_ord_info
    (caps_id,ord_id) using btree  in curidxdbs2;
create index cwt_caps_ord_idx2 on cwt_caps_ord_info
    (cnsm_id) using btree  in curidxdbs2;
--###################################################
create table cwt_cmpy_ctl
  (
    city_id char(1) not null ,
    actv_stat_cd char(1) not null
  ) in curdbs2 lock mode row;
revoke all on cwt_cmpy_ctl from "public";
create unique index cwt_cmpy_ctl_pk on cwt_cmpy_ctl
    (city_id)  fillfactor 100 in curidxdbs2 ;
alter table cwt_cmpy_ctl add constraint primary key
    (city_id) constraint cwt_cmpy_ctl_pk  ;
--###################################################
create table cwt_dlv_wave
  (
    uniq_wave_id                serial not null ,
    wave_pick_id                smallint not null ,
    stor_id                     char(7) not null ,
    shft_num_cd                 smallint not null ,
    wave_desc_tx                char(35) not null ,
    truk_rte_id                 smallint not null ,
    strt_van_rte_id             smallint not null ,
    trsh_van_rte_id             smallint,
    mrch_min_stop_allw_qy       smallint,
    mrch_max_stop_allw_qy       smallint,
    mrch_min_ord_lrnd_qy        smallint,
    cnsm_min_stop_allw_qy       smallint,
    cnsm_max_stop_allw_qy       smallint,
    cnsm_min_ord_lrnd_qy        smallint,
    mrch_tote_dolr_calc_qy      decimal(5,2),
    mrch_svc_dur_mult_qy        decimal(5,2),
    cnsm_tote_dolr_calc_qy      decimal(5,2),
    cnsm_svc_dur_mult_qy        decimal(5,2),
    depo_id                     char(3) not null
  ) in curdbs2 extent size 2000 next size 2000 lock mode row;
revoke all on cwt_dlv_wave from "public";
create unique index cwt_dlv_wave_i1 on cwt_dlv_wave
    (uniq_wave_id) using btree  in curidxdbs2;
create unique index cwt_dlv_wave_i2 on cwt_dlv_wave
    (wave_pick_id,stor_id,shft_num_cd) using btree  in curidxdbs2;
create index cwt_dlv_wave_i3 on cwt_dlv_wave
    (stor_id,shft_num_cd) using btree  in curidxdbs2 ;
create unique index cwt_dlv_wave_i4 on cwt_dlv_wave
    (stor_id,truk_rte_id) using btree  in curidxdbs2 ;
alter table cwt_dlv_wave add constraint primary key
    (uniq_wave_id) constraint pk_cwt_dlv_wave;
--#######################################################
create table cwt_elec_pmt_clc
  (
    bill_id char(15) not null ,
    ord_id char(10) not null ,
    cnsm_id char(8) not null ,
    crtn_dt date not null ,
    clc_user_id char(10),
    clc_stat_cd char(1) not null ,
    last_pmt_cd char(6),
    last_rej_pmt_cd char(3),
    no_of_rsnd smallint,
    wait_stat_cd char(1),
    next_cntc_dt date,
    audt_upd_dt date,
    audt_upd_tm char(8),
    audt_upd_id char(10)
  ) in curdbs2  extent size 15000 next size 5000 lock mode row;
revoke all on cwt_elec_pmt_clc from "public";
create unique index cwt_elec_pmt_clc_idx on cwt_elec_pmt_clc
    (bill_id) using btree  in curidxdbs2;
alter table cwt_elec_pmt_clc add constraint primary
    key (bill_id) constraint cwt_elec_pmt_clc_pk  ;
--###############################################
create table cwt_it_tote_excp
  (
    pod_id integer,
    tote_reqd_qy smallint
  ) in curdbs2  extent size 16 next size 16 lock mode row;
revoke all on cwt_it_tote_excp from "public";
create unique index cwt_it_tote_excp_pk on cwt_it_tote_excp
    (pod_id) using btree  in curidxdbs2 ;
alter table cwt_it_tote_excp add constraint primary
    key (pod_id) constraint pk_cwt_it_tote_excp;
--###############################################
create table cwt_rte_eta_ctl
  (
    ecom_stor_id integer ,
    min_dlv_wnd_tm interval hour to minute,
    max_dlv_wnd_tm interval hour to minute,
    pre_eta_var_tm interval hour to minute,
    post_eta_var_tm interval hour to minute,
    user_type_cd char(1),
    cnsm_type_cd char(1)
  ) in curdbs2  extent size 64 next size 64 lock mode row;
revoke all on cwt_rte_eta_ctl from "public";


create unique index cwt_rte_eta_ctl_pk on cwt_rte_eta_ctl
    (ecom_stor_id,min_dlv_wnd_tm) using btree fillfactor 100 in curidxdbs2 ;
alter table cwt_rte_eta_ctl add constraint primary key
    (ecom_stor_id,min_dlv_wnd_tm) constraint pk_cwt_rte_eta_ctl
     ;
--#############################################
create table cwt_site_type
  (
    ship_type_cd char(10) not null ,
    mbr_type_cd char(1) not null ,
    stop_time_cd smallint not null ,
    stop_mint_qy smallint
  )in curdbs2  extent size 16 next size 16 lock mode row;
revoke all on cwt_site_type from "public";
create unique index cwt_site_type_i1 on cwt_site_type
    (ship_type_cd,mbr_type_cd,stop_time_cd) fillfactor 100 in curidxdbs2
    ;
alter table cwt_site_type add constraint primary key
    (ship_type_cd,mbr_type_cd,stop_time_cd) constraint
    pk_cwt_site_type  ;
--####################################################
create table cwt_tote_eqtn_ctl
  (
    stor_id char(3),
    type_cd char(1),
    parm_cnst_qy float,
    parm_doll_qy float,
    parm_doll_sq_qy float,
    parm_am_pm_qy float,
    parm_manl_adj_qy float
  ) in curdbs2  extent size 16 next size 16 lock mode row;
revoke all on cwt_tote_eqtn_ctl from public;

create unique index cwt_tote_eqtn_ctl_pk on cwt_tote_eqtn_ctl
    (stor_id,type_cd) using btree fillfactor 100 in curidxdbs2;
alter table cwt_tote_eqtn_ctl add constraint primary
    key (stor_id,type_cd) constraint pk_cwt_tote_eqtn_ctl;
--#########################################
create table cur_cost_mnt_link
  (
    stor_id char(7) not null ,
    upc_cd decimal(14,0) not null ,
    supl_cd char(3) not null ,
    base_cost_pr_qy decimal(6,2),
    base_pr_qy decimal(6,2),
    base_mrgn_qy decimal(6,2),
    base_pr_meth_cd char(2),
    sale_cost_pr_qy decimal(6,2),
    sale_pr_qy decimal(6,2),
    sale_mrgn_qy decimal(6,2),
    sale_pr_meth_cd char(2),
    rtl_it_name_tx char(35),
    case_pack_qy integer,
    is_item_actv_cd char(1) not null ,
    cost_strt_dt date not null ,
    cost_end_dt date,
    disp_strt_dt date not null ,
    disp_end_dt date,
    spec_cd char(3),
    disp_used_strt_cd char(1) not null ,
    disp_used_end_cd char(1) not null ,
    audt_cr_dt datetime year to minute,
    audt_cr_id char(8),
    audt_upd_dt datetime year to minute,
    audt_upd_id char(8)
  ) in curdbs2 extent size 24000 next size 4000 lock mode row;
revoke all on cur_cost_mnt_link from "public";

create unique index cur_cost_mnt_link1 on cur_cost_mnt_link
    (stor_id,upc_cd,supl_cd,cost_strt_dt)  fillfactor 100 in curidxdbs2 ;
alter table cur_cost_mnt_link add constraint primary key
    (stor_id,upc_cd,supl_cd,cost_strt_dt) constraint cur_cost_mnt_link1  ;
--#########################################
create table cur_groc_rtl_dflt
  (
    city_id char(1) not null ,
    pod_id integer not null ,
    upc_cd decimal(14,0) not null ,
    sku_1_cd integer,
    sku_2_cd integer,
    hi_cone_cd char(1),
    unit_scal_qy decimal(7,2),
    pr_scal_qy decimal(7,4),
    pr_meth_cd char(2),
    pr_src_cd char(1),
    tax_type_cd char(1),
    it_shop_cd char(1),
    cmdy_cd char(10),
    sub_cmdy_cd char(10),
    dept_cd char(10),
    supl_cd char(3),
    blt_dep_qy decimal(4,2),
    audt_cr_id char(8),
    audt_cr_dt date,
    audt_upd_id char(8),
    audt_upd_dt date
  ) in curdbs2 extent size 24000 next size 8000 lock mode row;
revoke all on cur_groc_rtl_dflt from "public";

create unique index cur_groc_rtl_df_pk on cur_groc_rtl_dflt
    (city_id,pod_id)  fillfactor 100  in curidxdbs2 ;

alter table cur_groc_rtl_dflt add constraint primary key
    (city_id,pod_id) constraint pk_cur_groc_rtl_df  ;
--##########################################################
create table cur_groc_rtl_tax
  (
    pod_id integer not null ,
    tax_grp_cd char(3) not null ,
    stor_id char(6) not null,
    it_tax_qy decimal(7,5) not null,
    btl_dep_qy decimal(3,2) not null,
    audt_upd_dt date not null,
    audt_upd_id char(10)
  ) in curdbs2 extent size 20000 next size 8000 lock mode row ;
revoke all on cur_groc_rtl_tax from "public";


create unique index cur_groc_rtl_tx_pk on cur_groc_rtl_tax
    (pod_id, tax_grp_cd, stor_id)  fillfactor 100  in curidxdbs2 ;
alter table cur_groc_rtl_tax add constraint primary key
    (pod_id, tax_grp_cd, stor_id) constraint pk_cur_groc_rtl_tx ;
create index cur_groc_rtl_tax_2 on cur_groc_rtl_tax
    (pod_id, stor_id)  fillfactor 100  in curidxdbs2 ;
--##############################################
create table cur_it_in_zone
(
 stor_id CHAR(7),
 pod_id integer,
 upc_cd DECIMAL(14,0),
 it_shop_cd CHAR(1),
 actv_stat_cd CHAR(1),
 old_actv_stat_cd CHAR(1),
 actv_stat_upd_dt DATE
) in curdbs2 extent size 16000 next size 4000 lock mode row ;
revoke all on cur_it_in_zone from public;

create unique index cur_it_in_zone_pk on cur_it_in_zone
    (stor_id, pod_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_it_in_zone add constraint primary key
    (stor_id, pod_id) constraint cur_it_in_zone_pk  ;
--##############################################
create table "c00dba".cur_ord_post_ctl
  (
    post_id integer,
    ord_post_cnt smallint,
    city_id char(1),
    audt_cr_dt date
        default today,
    audt_cr_id char(8)
        default user
  ) in curdbs2 extent size 8000 next size 2000 lock mode page;
revoke all on "c00dba".cur_ord_post_ctl from "public";

create unique index cur_ord_pst_ctl_pk on cur_ord_post_ctl ( post_id, city_id )
  fillfactor 100 in curidxdbs2;
alter table cur_ord_post_ctl add constraint primary key ( post_id, city_id )
  constraint pk_cur_ord_pst_ctl;
--########################################################
create table cur_img_exst
(
pod_id integer,
upc_cd decimal(14,0),
actv_cd CHAR(1),
cont_id integer,
med_cont_id integer,
sm_cont_id integer
) in curdbs2 extent size 1500 next size 1000 lock mode row ;
revoke all on cur_img_exst from public;


create unique index cur_img_exst_pk on cur_img_exst
    (pod_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_img_exst add constraint primary key
    (pod_id) constraint cur_img_exst_pk  ;
--############################################
create table cur_onln_prom
  (
    prom_id serial not null ,
    stor_id char(7) not null ,
    loc_id integer not null ,
    disp_type_id integer not null ,
    disp_tx varchar(255),
    pr_info_tx varchar(255),
    func_id integer not null ,
    func_arg_id integer not null ,
    cont_id integer,
    cp_cat_id integer,
    desc_tx varchar(100),
    size_tx varchar(50),
    reg_pr_tx varchar(30),
    prom_pr_tx varchar(30),
    seq_qy smallint not null ,
    strt_dt date not null ,
    strt_tm datetime hour to minute not null ,
    end_dt date not null ,
    end_tm datetime hour to minute not null ,
    check (end_dt >= strt_dt )
  ) in curdbs2 extent size 22000 next size 1000 lock mode row;
revoke all on cur_onln_prom from "public";

create unique index cur_onln_prom_pk on cur_onln_prom
    (stor_id,loc_id,seq_qy,strt_dt) in curidxdbs2;
alter table cur_onln_prom add constraint primary key
    (stor_id,loc_id,seq_qy,strt_dt) constraint  cur_onln_prom_pk ;
--##################################################
create table cur_grp_sku
  (
    pod_id integer not null ,
    pod_grp_id integer not null
  ) in curdbs2 extent size 8000 next size 1000 lock mode row ;
revoke all on cur_grp_sku from "public";

create unique index cur_grp_sku_pk on cur_grp_sku
    (pod_id,pod_grp_id)  fillfactor 100 in curidxdbs2 ;
alter table cur_grp_sku add constraint primary key
    (pod_id,pod_grp_id) constraint cur_grp_sku_pk  ;
--#####################################################
create table cur_it_dict_cat
  (
    it_dict_cat_id integer not null,
    cat_type_cd char(5),
    cat_tx char(60) not null
  ) in curdbs2 extent size 64 next size 64 lock mode row ;
revoke all on cur_it_dict_cat from "public";

create unique index cur_it_dict_cat1 on cur_it_dict_cat
    (it_dict_cat_id,cat_type_cd)  fillfactor 100 in curidxdbs2 ;
alter table cur_it_dict_cat add constraint primary key
    (it_dict_cat_id,cat_type_cd) constraint cur_it_dict_cat_pk  ;
create index cur_it_dict_cat2 on cur_it_dict_cat (cat_tx)
fillfactor 100 in curidxdbs2;
--#################################
create table cur_pod_grp
  (
    pod_grp_id integer not null ,
    grp_tx char(25),
    prim_dict_tx varchar(50,0),
    grp_type_cd char(1),
    actv_cd char(1),
    adv_id integer,
    page_file_tx char(50)
  ) in curdbs2 extent size 8000 next size 2000 lock mode row;
revoke all on cur_pod_grp from "public";
create unique index cur_pod_grp_pk on cur_pod_grp
    (pod_grp_id)  fillfactor 100  in curidxdbs2 ;
alter table cur_pod_grp add constraint primary key
    (pod_grp_id) constraint cur_pod_grp_pk  ;
create index cur_pod_grp_idx1 on cur_pod_grp (grp_tx)   fillfactor 100 in curidxdbs2;
--#################################
create table cur_user
  (
    user_id integer not null ,
    cnsm_id char(8) ,
    log_id char(8),
    hndl_id varchar(20) not null,
    rtlr_card_id varchar(20),
    rtlr_card_trk_id varchar(20),
    mail_tx varchar(60),
    othr_mail_tx varchar(60),
    mail_type_cd smallint not null ,
    mail_stat_fl smallint,
    othr_mail_stat_fl smallint,
    mail_opt_news_cd  char(1),
    mail_opt_spec_cd  char(1),
    gst_cd char(1),
    audt_cr_dt_tm datetime year to second,
    check (mail_type_cd IN (0 ,1 ,2 ))
  ) in curdbs2 extent size 140000 next size 8000 lock mode row;

revoke all on cur_user from "public";

create unique index cur_user_pk on cur_user (user_id)
         fillfactor 100 in curidxdbs2;
alter table cur_user add constraint primary key
    (user_id) constraint cur_user_pk;
create index cur_user_idx2 on cur_user (cnsm_id)
         fillfactor 100 in curidxdbs2;
--###########################################
create table cwt_pref_list_box
  (
    prog_id varchar(10) not null ,
    ecom_stor_id integer not null ,
    seq_qy smallint not null ,
    pref_val_cd varchar(60) not null ,
    disp_tx varchar(50) not null
  ) in curdbs2 extent size 128 next size 128 lock mode row;
revoke all on cwt_pref_list_box from "public";

create unique index cwt_pref_list_b_pk on cwt_pref_list_box
    (prog_id,ecom_stor_id,seq_qy) fillfactor 100 in curidxdbs2;
alter table cwt_pref_list_box add constraint primary
    key (prog_id,ecom_stor_id,seq_qy) constraint pk_cwt_pref_list_b ;
--###########################
create table cwt_prsp_user
  (
    prsp_id serial not null ,
    last_name_tx varchar(30),
    frst_name_tx varchar(30),
    mi_tx char(1),
    ttl_tx varchar(6),
    adr_1_tx varchar(30),
    adr_2_tx varchar(30),
    city_tx varchar(20),
    st_cd char(2),
    zip_cd varchar(10),
    home_phon_cd varchar(13),
    work_phon_cd varchar(13),
    work_phon_ext_cd varchar(4),
    src_cd varchar(20),
    mail_tx varchar(60),
    audt_cr_id char(8) not null ,
    audt_cr_dt_tm datetime year to second not null
  ) in curdbs2 extent size 48000 next size 4000 lock mode row;
revoke all on cwt_prsp_user from "public";

create unique index cwt_prsp_user_pk on cwt_prsp_user
    (prsp_id) fillfactor 100 in curidxdbs2;
alter table cwt_prsp_user add constraint primary key
    (prsp_id) constraint pk_cwt_prsp_user  ;
--############################################
create table cwt_rtlr_card
  (
    user_id integer not null ,
    rtlr_card_id varchar(20) not null
  ) in curdbs2  extent size 20000 next size 2000 lock mode row;
revoke all on cwt_rtlr_card from public;
create unique index cwt_rtlr_card_idx on cwt_rtlr_card(user_id)
    fillfactor 100 in curidxdbs2;

alter table cwt_rtlr_card add constraint primary key(user_id)
    constraint cwt_rtlr_card_pk;
--######################################
create table cwt_user_gst
  (
    user_id integer not null ,
    ecom_stor_id integer,
    city_tx varchar(20),
    st_cd char(2),
    zip_cd varchar(10,5),
    type_cd char(1)
  ) in curdbs2 extent size 60000 next size 4000 lock mode row;
revoke all on cwt_user_gst from "public";

create unique index cwt_user_gst_pk on cwt_user_gst
    (user_id) fillfactor 100 in curidxdbs2;
alter table cwt_user_gst add constraint primary key
    (user_id) constraint pk_cwt_user_gst  ;
--################################################
create table cwt_user_pref
  (
    user_id integer not null ,
    pref_id integer not null ,
    pref_val_cd varchar(64)
  ) in curdbs2 extent size 32000 next size 4000 lock mode row;
revoke all on  cwt_user_pref from "public";
create unique index cwt_user_pref_i1 on  cwt_user_pref
    (user_id,pref_id) using btree fillfactor 100 in curidxdbs2 ;
alter table  cwt_user_pref add constraint primary key
    (user_id,pref_id) constraint  pk_cwt_user_pref ;
--#####################################################
create table cur_ecom_stor_xref
  (
    ecom_stor_id integer not null ,
    stor_id char(7) not null,
    pma_id char(10) 
  ) extent size 16 next size 16 lock mode row;
revoke all on cur_ecom_stor_xref from "public";

create unique index cur_ecom_stor_xre1 on cur_ecom_stor_xref
    (ecom_stor_id);
alter table cur_ecom_stor_xref add constraint primary
    key (ecom_stor_id) constraint cur_ecom_stor_xre1  ;
--###########################################
create table cur_prom_onln
  (
    prom_id integer not null ,
    ecom_stor_id integer not null ,
    loc_id integer not null ,
    seq_qy smallint not null ,
    strt_dt_tm datetime year to minute not null ,
    end_dt_tm datetime year to minute not null ,
    actv_stat_cd         char(1) ,
    disp_type_id integer not null ,
    cont_id integer,
    cp_cat_id integer,
    desc_tx varchar(100),
    size_tx varchar(50),
    reg_pr_tx varchar(30),
    prom_pr_tx varchar(30),
    func_id integer not null ,
    func_arg_id integer not null ,
    check (end_dt_tm >= strt_dt_tm )
  ) in curdbs2 extent size 6000 next size 1000 lock mode row;
revoke all on cur_prom_onln from "public";

create unique index cur_prom_onln_pk on cur_prom_onln
    (prom_id) in curidxdbs2;
alter table cur_prom_onln add constraint primary key
    (prom_id) constraint cur_prom_onln_pk  ;
create unique index cur_prom_onln_i1 on cur_prom_onln
    (ecom_stor_id,loc_id,seq_qy,strt_dt_tm) in curidxdbs2;
--##################################################
create table cwt_brnd_hdr
  (
    brnd_id integer not null ,
    brnd_desc_tx varchar(50) not null ,
    cont_id integer not null ,
    parm_type_cd char(1),
    parm_tx char(35),
    brnd_prty_id smallint not null ,
    actv_cd char(1) not null ,
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id char(8),
    audt_upd_dt_tm datetime year to second
  ) in curdbs2  extent size 1024 next size 1024 lock mode row;
revoke all on cwt_brnd_hdr from "public";
create unique index cwt_brnd_hdr_pk on cwt_brnd_hdr
    (brnd_id) using btree  in curidxdbs2 ;
alter table cwt_brnd_hdr add constraint primary key
    (brnd_id) constraint pk_cwt_brnd_hdr  ;
--#################################
create table cwt_cp_cat
  (
    minr_cat_id integer,
    cp_cat_id integer,
    cp_cat_tx char(35),
    audt_upd_dt date
  ) in curdbs2 extent size 1000 next size 1000 lock mode row;
revoke all on cwt_cp_cat from "public";

create unique index cwt_cp_cat_pk on cwt_cp_cat
    (minr_cat_id) fillfactor 100 in curidxdbs2;
alter table cwt_cp_cat add constraint primary key (minr_cat_id)
    constraint cwt_cp_cat_pk  ;
--##########################################
create table cwt_menu_dtl
  (
    menu_id char(20),
    ecom_stor_id integer,
    menu_tx varchar(50),
    cont_id integer,
    func_id smallint,
    func_arg_cd char(35),
    seq_qy decimal(5,2),
    actv_cd char(1)
  ) in curdbs2  extent size 5000 next size 2000 lock mode row;
revoke all on cwt_menu_dtl from "public";
create index cwt_menu_dtl_i1 on cwt_menu_dtl
    (menu_id,menu_tx,ecom_stor_id) using btree  in curidxdbs2 ;
create index cwt_menu_dtl_i2 on cwt_menu_dtl
    (ecom_stor_id) using btree  in curidxdbs2 ;
create index cwt_menu_dtl_i3 on cwt_menu_dtl
    (menu_id,ecom_stor_id) using btree  in curidxdbs2 ;
--##############################################
create table cwt_menu_hdr
  (
    menu_id char(20),
    ecom_stor_id integer,
    menu_ttl_tx varchar(50),
    actv_cd char(1),
    cp_cat_id integer
  ) in curdbs2  extent size 64 next size 64 lock mode row;
revoke all on cwt_menu_hdr from "public";
create unique index cwt_menu_hdr_i1 on cwt_menu_hdr
    (menu_id,ecom_stor_id) using btree  in curidxdbs2 ;
create index cwt_menu_hdr_i2 on cwt_menu_hdr
    (ecom_stor_id) using btree  in curidxdbs2 ;
alter table cwt_menu_hdr add constraint primary key
    (menu_id,ecom_stor_id) constraint pk_cwt_menu_hdr
     ;
--alter table cpt_menu_hdr add constraint (foreign key
--    (ecom_stor_id) references cpt_ecom_stor_ctl  constraint
--    fk_cpt_menu_hdr);
--###########################################
create table cwt_ntrn_prof_cont
  (
    prof_id integer not null ,
    prof_val_qy decimal(4,1),
    cont_id integer,
    ttl_tx char(15),
    seq_qy integer
  ) in curdbs2 extent size 1000 next size 1000 lock mode row;
revoke all on cwt_ntrn_prof_cont from "public";
create index cwt_ntrn_prof_cont_i1 on cwt_ntrn_prof_cont
    (prof_id) using btree fillfactor 100 in curidxdbs2 ;
create unique index cwt_ntrn_prof_cont_i15 on
    cwt_ntrn_prof_cont (prof_id,seq_qy) using btree  fillfactor 100 in curidxdbs2;
create unique index cwt_ntrn_prof_cont_pk on
    cwt_ntrn_prof_cont (prof_id,prof_val_qy) using btree  fillfactor 100 in curidxdbs2;
alter table cwt_ntrn_prof_cont add constraint primary
    key (prof_id,prof_val_qy) constraint pk_cwt_ntrn_prof_cont;
--####################################################
create table cwt_ntrn_prof_elmt
  (
    elmt_id integer not null ,
    elmt_name_tx char(25),
    elmt_uom_cd char(5),
    elmt_rcmd_tx varchar(255),
    elmt_boln_fl char(1),
    web_obj_name_tx varchar(40),
    web_prop_name_tx varchar(60)
  ) in curdbs2 extent size 1000 next size 1000 lock mode row;
revoke all on cwt_ntrn_prof_elmt from "public";

create unique index cwt_ntrn_prof_elmt_i1 on
    cwt_ntrn_prof_elmt (elmt_id) using btree fillfactor 100 in curidxdbs2 ;
alter table cwt_ntrn_prof_elmt add constraint primary
    key (elmt_id) constraint pk_cwt_ntrn_prof_elmt  ;
--#######################################
create table cwt_ntrn_prof_hdr
  (
    prof_id integer not null ,
    user_id integer not null ,
    prof_name_tx char(30),
    cont_id integer,
    audt_cr_id char(8),
    audt_cr_dt_tm datetime year to second,
    audt_upd_id char(8),
    audt_upd_dt_tm datetime year to second
  ) in curdbs2 extent size 4000 next size 2000 lock mode row;
revoke all on cwt_ntrn_prof_hdr from "public";

create unique index cwt_ntrn_prof_hdr_i1 on cwt_ntrn_prof_hdr
    (prof_id) using btree fillfactor 100 in curidxdbs2 ;
alter table cwt_ntrn_prof_hdr add constraint primary
    key (prof_id) constraint pk_cwt_ntrn_prof_hdr  ;
--##############################################
create table cwt_ntrn_prof_mkt
  (
    prof_id integer not null ,
    ecom_stor_id integer
  ) in curdbs2 extent size 1000 next size 1000 lock mode row;
revoke all on cwt_ntrn_prof_mkt from "public";
create unique index cwt_ntrn_prof_mkt_i1 on cwt_ntrn_prof_mkt
    (prof_id,ecom_stor_id) using btree fillfactor 100 in curidxdbs2 ;
alter table cwt_ntrn_prof_mkt add constraint primary
    key (prof_id,ecom_stor_id) constraint pk_cwt_ntrn_prof_mkt;
create        index cwt_ntrn_prof_mkt_i2 on cwt_ntrn_prof_mkt
    (prof_id) using btree fillfactor 100 in curidxdbs2 ;
--########################################
create table cwt_ntrn_prof_optn
  (
    optn_id serial not null ,
    elmt_id integer not null ,
    web_prop_name_tx varchar(60),
    optn_name_tx varchar(60),
    optn_seq_qy integer,
    opr_tx char(5),
    prop_val_tx varchar(60)
  ) in curdbs2 extent size 1000 next size 1000 lock mode row;
revoke all on cwt_ntrn_prof_optn from "public";

create unique index cwt_ntrn_prof_optn_i1 on
    cwt_ntrn_prof_optn (optn_id,elmt_id,optn_seq_qy) using btree fillfactor 100 in curidxdbs2;

create unique index cwt_ntrn_prof_optn_i25 on
    cwt_ntrn_prof_optn (elmt_id,optn_seq_qy) using btree  fillfactor 100 in curidxdbs2;
alter table cwt_ntrn_prof_optn add constraint primary
    key (optn_id,elmt_id,optn_seq_qy) constraint pk_cwt_ntrn_prof_optn;
create        index cwt_ntrn_prof_optn_i3 on
    cwt_ntrn_prof_optn (elmt_id) using btree  fillfactor 100 in curidxdbs2;
--##########################################################
create table cwt_ntrn_prof_rule
  (
    rule_id serial not null ,
    prof_id integer not null ,
    elmt_id integer not null ,
    optn_id integer not null ,
    web_obj_name_tx varchar(40),
    web_prop_name_tx varchar(60),
    opr_tx char(5),
    prop_val_tx char(60),
    primary key (rule_id)  constraint pk_cwt_ntrn_prof_rule
  ) in curdbs2 extent size 10000 next size 8000 lock mode row;
revoke all on cwt_ntrn_prof_rule from "public";
create unique index cwt_ntrn_prof_rule_i1 on
    cwt_ntrn_prof_rule (rule_id,prof_id) using btree fillfactor 100 in curidxdbs2 ;
create        index cwt_ntrn_prof_rule_i2 on
    cwt_ntrn_prof_rule (prof_id) using btree fillfactor 100 in curidxdbs2 ;
--#######################################
create table cwt_ord_avgo_dtl
  (
    sess_id integer not null ,
    prod_id integer not null ,
    form_type_cd char(1) not null ,
    upld_sess_id integer,
    user_id integer not null ,
    list_id integer,
    it_qy smallint,
    stat_cd char(1),
    audt_cr_dt_tm datetime year to second ,
    audt_upd_dt_tm datetime year to second
  ) in curdbs2 extent size 10000 next size 1000 lock mode row;
revoke all on cwt_ord_avgo_dtl from "public";

create unique index cwt_ord_avgo_dt_pk on cwt_ord_avgo_dtl
    (sess_id,prod_id,form_type_cd) fillfactor 100 in curidxdbs2;
alter table cwt_ord_avgo_dtl add constraint primary
    key (sess_id,prod_id,form_type_cd) constraint pk_cwt_ord_avgo_dt
     ;
create index cwt_ord_avgo_dtl_1 on cwt_ord_avgo_dtl
    (list_id) fillfactor 100 in curidxdbs2;
--#############################################
create table cwt_ord_cpn
  (
    list_id integer not null ,
    cpn_id varchar(16) not null ,
    cpn_ttl_tx varchar(20),
    cpn_desc_tx varchar(45),
    cpn_type_cd smallint
        default 1 not null ,
    targ_cat_tx varchar(30),
    cpn_tot_used_qy smallint,
    cpn_tot_svgs_qy decimal(7,2),
    audt_cr_id char(10) not null ,
    audt_cr_dt_tm datetime year to second not null ,
    check (cpn_type_cd IN (1 ,2 )) constraint cwt_ord_cpn_type
  ) in curdbs2 extent size 48000 next size 4000 lock mode row;
revoke all on cwt_ord_cpn from "public";

create unique index cwt_ord_cpn_pk on cwt_ord_cpn
    (list_id,cpn_id) fillfactor 100 in curidxdbs2;

alter table cwt_ord_cpn add constraint primary key (list_id,
    cpn_id) constraint pk_cwt_ord_cpn  ;
--################################################
create table cwt_prod_rqst
  (
    prod_rqst_id integer not null ,
    cnsm_id char(8) not null ,
    time_ord_qy smallint,
    stor_id char(6) not null ,
    cp_cat_id integer not null ,
    brnd_name_tx varchar(35),
    desc_tx varchar(65),
    size_tx varchar(20),
    upc_cd varchar(14),
    cmt_tx varchar(60),
    new_prod_cd char(1),
    stat_cd char(1) not null ,
    resp_cd char(1) not null ,
    asgn_to_user_id char(8),
    pod_id integer,
    msg_id integer,
    audt_cr_dt_tm datetime year to second not null ,
    clse_dt_tm datetime year to minute,
    audt_upd_dt_tm datetime year to minute not null ,
    audt_upd_user_id char(8) not null,
    arc_ld_dt DATE
  ) in curdbs2 extent size 5000 next size 4000 lock mode row;
revoke all on cwt_prod_rqst from "public";

create unique index cwt_prod_rqst1 on cwt_prod_rqst
    (prod_rqst_id) using btree fillfactor 100 in curidxdbs2 ;
alter table cwt_prod_rqst add constraint primary key
    (prod_rqst_id) constraint pk_cwt_pod_rqst  ;
--###############################################
create table cwt_mail_job_ctl
  (
    job_id integer not null ,
    job_name_tx varchar(35) ,
    job_desc_tx varchar(255),
    targ_name_tx varchar(25) ,
    targ_file_name_tx varchar(50),
    targ_type_cd smallint  ,
    url_tx varchar(100) ,
    subj_tx varchar(60) ,
    from_tx varchar(60) ,
    rply_mail_tx varchar(60) ,
    run_type_cd smallint ,
    run_arg_tx varchar(35),
    job_trk_cd smallint ,
    send_max_qy integer ,
    job_cat_cd integer ,
    schd_type_cd smallint ,
    days_btwn_run_qy smallint,
    qa_targ_name_tx varchar(25),
    qa_send_fl smallint ,
    mnt_targ_name_tx varchar(25),
    mnt_send_fl smallint ,
    actv_fl smallint ,
    arc_fl smallint ,
    audt_cr_id char(8) ,
    audt_cr_dt_tm datetime year to second ,
    audt_upd_id char(8) ,
    audt_upd_dt_tm datetime year to second
  )
in curdbs2 extent size 1000 next size 1000 lock mode row;
revoke all on cwt_mail_job_ctl from "public";
create unique index cwt_mail_job_ctl1 on cwt_mail_job_ctl
    (job_id) fillfactor 100 in curidxdbs2 ;
alter table cwt_mail_job_ctl add constraint primary key
    (job_id) constraint pk_mail_job_ctl;
create unique index cwt_mail_job_ctl2 on cwt_mail_job_ctl
    (job_name_tx) fillfactor 100 in curidxdbs2;
create index cwt_mail_job_ctl3 on cwt_mail_job_ctl
    (job_cat_cd) fillfactor 100 in curidxdbs2 ;
--#####################################################
create table cwt_mail_job_trk
  (
    job_id integer not null ,
    bch_id integer not null ,
    sent_dt_tm datetime year to second  ,
    strt_dt_tm datetime year to second  ,
    end_dt_tm datetime year to second,
    to_send_qy integer,
    sent_qy integer,
    not_sent_qy integer,
    sent_fl smallint
  ) in curdbs2 extent size 8000 next size 2000 lock mode row;
revoke all on cwt_mail_job_trk from "public";
create unique index cwt_mail_job_trk1 on cwt_mail_job_trk
    (job_id, bch_id) fillfactor 100 in curidxdbs2;
alter table cwt_mail_job_trk add constraint primary key
    (job_id, bch_id) constraint pk_mail_job_trk;
create index cwt_mail_job_trk2 on cwt_mail_job_trk
    (job_id) fillfactor 100 in curidxdbs2 ;
--#########################################
CREATE TABLE cwt_wpt_stor_ctl(
  stor_id varchar(36),
  stor_type_ls varchar(15),
  opco_id varchar(4),
  vrsn_cd integer,
  name_tx varchar(100),
  stor_no varchar(10),
  shop_cntr_name_tx varchar(100),
  adr1_tx varchar(100),
  adr2_tx varchar(100),
  city_tx varchar(100),
  st_cd char(2),
  zip_cd varchar(5),
  zip4_cd varchar(4),
  lat_qy float,
  long_qy float,
  tm_zone_qy smallint,
  desc_tx varchar(255),
  open_dt datetime year to second,
  clos_dt datetime year to second,
  audt_cr_dt datetime year to second,
  audt_upd_dt datetime year to second,
  audt_upd_user_id varchar(50),
  actv_fl char(1)
)in curdbs2 extent size 256 next size 256 lock mode row;
revoke all on cwt_wpt_stor_ctl from public;
create unique index cwt_wpt_stor_ctl_pk on cwt_wpt_stor_ctl
    (stor_id) using btree in curidxdbs2 ;
alter table cwt_wpt_stor_ctl add constraint primary key
    (stor_id) constraint pk_cwt_wpt_stor_ctl;
--#######################################
