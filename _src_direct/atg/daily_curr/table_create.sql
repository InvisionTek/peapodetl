create table upt_user_prop_tx
(
  user_id integer,
  prop_key_tx varchar(50),
  prop_vl varchar(100)
) in curdlydbs1 extent size 80000 next size 40000 lock mode row;
revoke all on upt_user_prop_tx from public;

create table dps_user
(
  user_id integer,
  login varchar(60)
) in curdlydbs1 extent size 120000 next size 40000 lock mode row;
revoke all on dps_user from public;

create table upt_comc_info
(
  comc_id varchar(40),
  user_id integer,
  mail_tx varchar(100),
  txt_msg_mail_tx varchar(100),
  cell_phon_tx varchar(40)
) in curdlydbs1 extent size 240000 next size 80000 lock mode row;
revoke all on upt_comc_info from public;

create table upt_user
(
  user_id integer,
  opco_id varchar(10),
  auto_log_in_fl smallint,
  frst_name_tx varchar(40),
  mid_tx varchar(40),
  last_name_tx varchar(40),
  lcal_id smallint,
  last_actv_dt datetime year to second,
  reg_dt datetime year to second,
  actv_fl smallint,
  gst_user_fl smallint,
  comc_id varchar(40)
) in curdlydbs1 extent size 360000 next size 80000 lock mode row;
revoke all on upt_user from public;

create table upt_user_dflt_adr
(
  user_id integer,
  opco_id varchar(10),
  adr_id varchar(40),
  type_cd varchar(10)
) in curdlydbs1 extent size 120000 next size 40000 lock mode row;
revoke all on upt_user_dflt_adr from public;

create table dps_contact_info
(
  id varchar(40),
  user_id integer,
  phone_number varchar(30)
) in curdlydbs1 extent size 120000 next size 40000 lock mode row;
revoke all on dps_contact_info from public;

create table upt_cntc_info
(
  adr_id varchar(40),
  adr_name_tx varchar(100),
  alt_phon_tx varchar(40),
  alt_phon_ext_tx varchar(40)
) in curdlydbs1 extent size 120000 next size 40000 lock mode row;
revoke all on upt_cntc_info from public;

create table upt_rtlr_card
(
  rtlr_card_id varchar(40),
  user_id integer,
  rtlr_card_num_tx varchar(40),
  opco_id varchar(10)
) in curdlydbs1 extent size 120000 next size 40000 lock mode row;
revoke all on upt_rtlr_card from public;

