<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="UTF-8" />
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="node()"  />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="Name" >
        <Name><xsl:value-of select="."  /></Name>
    </xsl:template>

    <xsl:template match="SizeString">
      <Attributes><Attribute id='size'><Value><xsl:value-of select="." /></Value></Attribute></Attributes>
    </xsl:template>

    <xsl:template match="UPCs">
        <UPCs>
            <xsl:for-each select="tokenize(current(), ',')">
            <xsl:choose>
                <xsl:when test=".">
                    <UPC>
                        <xsl:value-of select="." />
                    </UPC>
                </xsl:when>
            </xsl:choose>
            </xsl:for-each>
        </UPCs>
    </xsl:template>
</xsl:stylesheet>
