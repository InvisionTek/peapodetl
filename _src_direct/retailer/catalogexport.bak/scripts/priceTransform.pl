use strict;
use warnings;
 
my ($filename) = @ARGV;
open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";
 
while (my $row = <$fh>) {
  chomp $row;
  
$row =~ s/<SPECIALSTARTREPLACE>VALUE<\/SPECIALSTARTREPLACE>/<special>/;
$row =~ s/<SPECIALENDREPLACE>VALUE<\/SPECIALENDREPLACE>/<\/special>/;

$row =~ s/<NEWARRIVALSTARTREPLACE>VALUE<\/NEWARRIVALSTARTREPLACE>/<newArrival>/;
$row =~ s/<NEWARRIVALENDREPLACE>VALUE<\/NEWARRIVALENDREPLACE>/<\/newArrival>/;

$row =~ s/<PREFFEREDSTARTREPLACE>VALUE<\/PREFFEREDSTARTREPLACE>/<preferred>/;
$row =~ s/<PREFFEREDENDREPLACE>VALUE<\/PREFFEREDENDREPLACE>/<\/preferred>/;

$row =~ s/<BOGOSTARTREPLACE>VALUE<\/BOGOSTARTREPLACE>/<bogo>/;
$row =~ s/<BOGOENDREPLACE>VALUE<\/BOGOENDREPLACE>/<\/bogo>/;

$row =~ s/<SALESTARTREPLACE>VALUE<\/SALESTARTREPLACE>/<sale>/;
$row =~ s/<SALEENDREPLACEKEY>VALUE<\/SALEENDREPLACEKEY>/<\/sale>/;

$row =~ s/<LOWEVERYDAYSTARTREPLACE>VALUE<\/LOWEVERYDAYSTARTREPLACE>/<lowEveryday>/;
$row =~ s/<LOWEVERYDAYENDREPLACE>VALUE<\/LOWEVERYDAYENDREPLACE>/<\/lowEveryday>/;

  print "$row\n";
}
close($fh);
