use strict;
use warnings;
 
my ($filename) = @ARGV;
open(my $fh, '<:encoding(UTF-8)', $filename)
    or die "Could not open file '$filename' $!";
 
while (my $row = <$fh>) {
    chomp $row;

    # These is needed due to a pentaho bug (PDI-11919) in adding xmlns attribute incorrectly for root node.
    # Was unable to add xmlns only to root node using Set Variables or Replace in String steps  
    $row =~ s/namespace=/xmlns=/;

    print "$row\n";
}

close($fh);
