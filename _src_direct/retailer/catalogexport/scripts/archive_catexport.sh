#!/bin/bash
# ----------------------------------------------------------------------------
# This script runs under sftpadmin user in crontab daily on datafeeds.peapod.com
#
# - Purge old catalog export zip files and archived zip files
# - Copy current catalog export zip file to an archived dated file
# ----------------------------------------------------------------------------

PURGE_NUM_DAYS_OLD=14
SFTP_FILE_DIR=/sftp
CATEXPORT_ZIP_FILE=catalog_export.zip
MTIME_PURGE=`expr $PURGE_NUM_DAYS_OLD - 1`

if [ "$MTIME_PURGE" -gt "0" ]; then
    MTIME_PURGE="+$MTIME_PURGE"
fi 

NOW="$(date +'%Y%m%d')"

echo "-----"
echo "purging zips older than $PURGE_NUM_DAYS_OLD days"

find $SFTP_FILE_DIR/*.zip* -mtime $MTIME_PURGE -exec rm -f {} \;

# archive current zip file
set -x
cp "$SFTP_FILE_DIR/$CATEXPORT_ZIP_FILE" "$SFTP_FILE_DIR/$CATEXPORT_ZIP_FILE.$NOW"
