<?xml version="1.0" encoding="UTF-8"?>
<xs:schema elementFormDefault="qualified"
           xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns="http://www.peapod.com/schema/catalog-export/price-zones"
           targetNamespace="http://www.peapod.com/schema/catalog-export/price-zones">

    <xs:include schemaLocation="common-1.0.xsd"/>

    <xs:annotation>
        <xs:documentation>Product pricing and additional data that is location specific.</xs:documentation>
    </xs:annotation>

    <xs:element name="productPrices">
        <xs:complexType>
            <xs:choice minOccurs="0" maxOccurs="unbounded">
                <xs:element name="productPrice" type="productPrice"/>
            </xs:choice>
        </xs:complexType>
    </xs:element>
    <xs:complexType name="productPrice">
        <xs:annotation>
            <xs:documentation>
                Key is combination of productId and priceZoneId.
                priceZoneId can be derived from zip, clientType, and city from service locations.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="unitPrice" type="decimalType">
                <xs:annotation>
                    <xs:documentation>Price per unit. Unit is itemUnit field in products.xml</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="price" type="decimalType">
                <xs:annotation>
                    <xs:documentation>Total price</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="upc" minOccurs="0" type="xs:long">
                <xs:annotation>
                    <xs:documentation>UPC. Can differ for same product id across locations, but usually is the same. 
                        This UPC data is not 100% accurate - could be a variation (e.g. multi-pack) of a product.</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="substitute" minOccurs="0" type="xs:nonNegativeInteger">
                <xs:annotation>
                    <xs:documentation>The product id of a recommended substitute</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="sample" minOccurs="0" type="flagType">
                <xs:annotation>
                    <xs:documentation>sample indicator</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="active" minOccurs="0" type="flagType">
                <xs:annotation>
                    <xs:documentation>active indicator</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="outOfStock" minOccurs="0" type="flagType">
                <xs:annotation>
                    <xs:documentation>temporarily out of stock indicator</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="upromise" minOccurs="0" type="flagType">
                <xs:annotation>
                    <xs:documentation>upromise indicator</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="newArrival" minOccurs="0" type="flagType">
                <xs:annotation>
                    <xs:documentation>new arrival indicator</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="special" minOccurs="0" type="special">
                <xs:annotation>
                    <xs:documentation>specials type</xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="priceZoneId" use="required">
            <xs:annotation>
                <xs:documentation>id indicating a price zone area. price zone id that can be derived from zip, clientType, and city from service locations.</xs:documentation>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="productId" use="required">
            <xs:annotation>
                <xs:documentation></xs:documentation>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>
    <xs:complexType name="special">
        <xs:annotation>
            <xs:documentation>
                Indicates if a product is a special (sale, bogo, preferred, low price everyday)
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="specialCd" type="specialCdType">
                <xs:annotation>
                    <xs:documentation>one character special code indicator: sale(*), bogo(B), preferred(P), or low price everyday($)</xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:choice minOccurs="0" maxOccurs="1">
                <xs:element name="preferred" type="flagType"/>
                <xs:element name="bogo" type="flagType"/>
                <xs:element name="sale" type="flagType"/>
                <xs:element name="lowEveryday" type="flagType"/>
            </xs:choice>
        </xs:sequence>
    </xs:complexType>
    <xs:simpleType name="specialCdType">
        <xs:annotation>
        <xs:documentation>
        The special code type indicates the type of special for the item they are mapped as:
        P - preferred
        B - buy one get one 
        * - Sale item
        $ - Low price everyday
        ""(empty string) - normal
        </xs:documentation>
    </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="P"/>
            <xs:enumeration value="B"/>
            <xs:enumeration value="*"/>
            <xs:enumeration value="$"/>
            <xs:enumeration value=""/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>
