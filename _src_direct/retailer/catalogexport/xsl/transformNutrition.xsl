<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:p="http://www.peapod.com/schema/catalog-export/products">
<xsl:output method="xml" encoding="UTF-8" indent="yes" />
    
    <xsl:strip-space elements="*"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="p:products/p:product/p:nutrition[contains(p:nutritionShow,'N')]" />

    <xsl:template match="p:nutrition/*">
        <xsl:variable name="cName" select="name()"/>
        <xsl:choose>
            <xsl:when test="following-sibling::node()[name()=concat($cName,'Unt')]">
                <xsl:copy>
                    <xsl:attribute name="amount">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                    <xsl:if test="following-sibling::node()[name()=concat($cName,'Pct')]">
                        <xsl:attribute name="percent">
                            <xsl:value-of select="following-sibling::node()[name()=concat($cName,'Pct')]"/>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:attribute name="unit">
                        <xsl:value-of select="following-sibling::node()[name()=concat($cName,'Unt')]"/>
                    </xsl:attribute> 
                </xsl:copy>
            </xsl:when>
            <xsl:when test="contains(name() ,'Unt') or contains(name() ,'Pct')"/>
            <xsl:when test="matches(name() ,'nutritionShow')"/>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates />
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
