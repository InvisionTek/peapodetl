#!/bin/bash

KDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/dba/mnitor
LDIR=/logs/pentaho/dba/mnitor

SVR="ifxp31 ifxp32 ifxdb ifxet ifxdw"
date;
for svr in $SVR
do

echo "running for ${svr}...";
cd $KDIR
./kitchen.sh -file="$CDIR/infor_mon.kjb" -param:SRVR="$svr" -param:DAY=1  -Level=Basic > $LDIR/log.infor_mon 2 >&1

done
date;
