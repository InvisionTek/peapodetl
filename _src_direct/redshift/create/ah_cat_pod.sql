drop table if exists ah_cat_pod;
create table ah_cat_pod 
(pod_id integer,
ah_dept varchar(50),
ah_cat varchar(50),
ah_sub_cat varchar(50)
);
grant select on all tables in schema public to group "analytics";
grant all on all tables in schema public to group "dba";
