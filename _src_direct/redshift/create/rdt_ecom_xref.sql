drop table if exists rdt_ecom_xref;
create table rdt_ecom_xref
  (
    ecom_stor_id integer,
    stor_id char(3),
    city_tx char(30),
    short_name char(10),
    opco_name char(30),
    sort_num integer,
    mkt_id char(2),
    mkt_desc_tx char(30),
    sns_mkt_area_tx char(20),
    sns_stor_no smallint,
    sns_zone smallint,
    sns_stor_id_no char(8),
    min_dlv_dt date,
    price_zone integer,
    fin_stor_id char(3),
    sns_cat_id integer,
    mwg_plat_id integer,
    wms_cd char(4)
);
grant select on all tables in schema public to group "analytics";
grant all on all tables in schema public to group "dba";
