drop table if exists rdt_acn_hdr;
create table rdt_acn_hdr
(
pod_id integer,
brnd_name_tx varchar(70),
ownr_name_tx varchar(70),
it_long_name_tx varchar(120),
it_size_cd varchar(20),
actv_cd char(1),
arc_ld_dt date,
arc_upd_dt date
);
grant select on all tables in schema public to group "analytics";
grant all on all tables in schema public to group "dba";
