drop table if exists rdt_rpt_stor_lookup;
create table rdt_rpt_stor_lookup
(
rpt_stor_id varchar(4),
stor_id char(3),
rpt_stor_desc varchar(20),
cet_stor_id varchar(4),
mktg_stor_id varchar(4),
mktg_stor_desc varchar(20),
opco_id varchar(4),
sort_num_cd smallint,
cet_ivte_cnt smallint,
dvsn_cd varchar(4),
othr_grp_id varchar(10)
);
grant select on all tables in schema public to group "analytics";
grant all on all tables in schema public to group "dba";

