BEGIN;

INSERT INTO gxt_ord_hdr 
SELECT t.ord_id,
t.cnsm_id,
t.stor_id,
t.ord_type_cd,
t.rqst_dt,
to_char(t.rqst_tm,'HH24:MI:SS')rqst_tm,
to_char(t.rqst_end_tm,'HH24:MI:SS')rqst_end_tm,
t.slot_id,
t.ord_plc_cd,
t.dlv_type_cd,
t.seq_id,
t.ord_tot_qy,
t.ord_tax_qy,
t.tax_grp_cd,
t.ord_btl_dep_qy,
t.natl_tot_qy,
t.natl_tax_qy,
t.natl_ship_fee_qy,
t.natl_crdt_card_qy,
t.pr_zone_cd,
t.arc_ld_dt 
FROM gxt_ord_hdr_stage t
LEFT JOIN gxt_ord_hdr s
ON t.ord_id = s.ord_id 
WHERE s.ord_id IS NULL;

DROP TABLE gxt_ord_hdr_stage;

END;
