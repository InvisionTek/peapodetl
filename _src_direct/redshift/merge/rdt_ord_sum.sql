BEGIN;

update rdt_ord_sum
set cnsm_id = a.cnsm_id,
type_cd = a.type_cd,
stor_id = a.stor_id,
dlv_dt = a.dlv_dt,
ord_dt = a.ord_dt,
ord_seq_num = a.ord_seq_num,
days_btwn_ord_qy = a.days_btwn_ord_qy,
ship_type_cd = a.ship_type_cd,
zip_cd = a.zip_cd,
slot_id = a.slot_id,
dlv_meth_cd = a.dlv_meth_cd,
pup_id = a.pup_id,
rdlv_cd = a.rdlv_cd,
it_dmnd_qy = a.it_dmnd_qy,
it_dlv_qy = a.it_dlv_qy,
whse_it_dlv_qy = a.whse_it_dlv_qy,
it_mrgn_qy = a.it_mrgn_qy,
sale_inct_qy = a.sale_inct_qy,
dlv_fee_qy = a.dlv_fee_qy,
tot_each_qy = a.tot_each_qy,
tot_pod_qy = a.tot_pod_qy,
plan_tote_qy = a.plan_tote_qy,
tot_orig_oos_inv_qy = a.tot_orig_oos_inv_qy,
tot_orig_oos_pick_qy = a.tot_orig_oos_pick_qy,
tot_sub_ship_qy = a.tot_sub_ship_qy,
tot_sub_groc_amt_qy = a.tot_sub_groc_amt_qy,
tot_act_oos_amt_qy = a.tot_act_oos_amt_qy,
audt_cr_dt = a.audt_cr_dt,
audt_upd_dt = a.audt_upd_dt
from rdt_ord_sum_stage a
where rdt_ord_sum.ord_id = a.ord_id;

INSERT INTO rdt_ord_sum 
SELECT t.* FROM rdt_ord_sum_stage t
LEFT JOIN rdt_ord_sum s
ON t.ord_id = s.ord_id 
WHERE s.ord_id IS NULL;

DROP TABLE rdt_ord_sum_stage;

END;
