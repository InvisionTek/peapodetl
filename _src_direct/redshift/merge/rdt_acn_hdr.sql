BEGIN;

update rdt_acn_hdr
set brnd_name_tx = t.brnd_name_tx,
ownr_name_tx = t.ownr_name_tx,
it_long_name_tx = t.it_long_name_tx,
it_size_cd = t.it_size_cd,
actv_cd = t.actv_cd,
arc_ld_dt = t.arc_ld_dt,
arc_upd_dt = t.arc_upd_dt
from rdt_acn_hdr_stage t
where rdt_acn_hdr.pod_id = t.pod_id;

INSERT INTO rdt_acn_hdr 
SELECT t.* FROM rdt_acn_hdr_stage t
LEFT JOIN rdt_acn_hdr s
ON t.pod_id = s.pod_id 
WHERE s.pod_id IS NULL;

DROP TABLE rdt_acn_hdr_stage;

END;
