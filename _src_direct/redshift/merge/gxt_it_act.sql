BEGIN;

INSERT INTO gxt_it_act 
SELECT t.* FROM gxt_it_act_stage t
LEFT JOIN gxt_it_act s
ON t.ord_id = s.ord_id 
AND t.orig_pod_id = s.orig_pod_id
WHERE s.ord_id IS NULL;

DROP TABLE gxt_it_act_stage;

END;
