#!/bin/ksh
###################
# Variables
###################
SCRIPT=`basename $0`
#PDIR=/Applications/pentaho503/design-tools/data-integration
PDIR=/opt/pentaho/data-integration
CDIR=/code/pentaho/redshift
LDIR=/logs/pentaho/redshift

###############
# Functions
##############
#usage
usage()
(
  echo " 	USAGE						"
  echo "	syntax <crt/upl/cpy/mrg> <table name>	"
) 

#check logs
checklogs()
( 
   LOGFILE=$1
   
   cd $LDIR
   ERRCNT=`cat $LOGFILE|egrep -v "log4j:ERROR"|egrep -c "ERROR|FAILURE"`
   #ERRCNT=`egrep -c "ERROR|FAILURE" $LOGFILE`
   ERRCNNT=`egrep -c "not connected" $LOGFILE`
   if [ $ERRCNT -gt 0 ]
   then
      MSG=1
   elif [ $ERRCNNT -gt 0 ]
   then
      MSG=2
   else
      MSG=0
   fi 
   return $MSG
)

upload()
(

DB=$1
TAB=$2
COL=$3
DY=$4

echo "Running upload ${TAB}..."
num=$#
if [ $num -eq 4 ]
then 
  
  TDATE=`/bin/date --date="$DY day ago" +%Y%m%d`
  #TDATE=`date -v-${DY}d +%Y%m%d`

  cd $PDIR
  ./pan.sh -file="$CDIR/upload/with_upload_dt.ktr" -param:db="$DB" -param:tab="$TAB" -param:col="$COL" -param:dy="$DY" -param:tdate="$TDATE" -Level=ERROR > $LDIR/log.upload_${TAB} 2>&1 

elif [ $num -eq 2 ]
then
  
  cd $PDIR
  ./pan.sh -file="$CDIR/upload/wout_upload_dt.ktr" -param:db="$DB" -param:tab="$TAB" -Level=ERROR > $LDIR/log.upload_${TAB} 2>&1

else
    echo "wrong number of arguement"  
    usage
    exit
fi

checklogs log.upload_${TAB} 
OUTPUT=$?
if [ $OUTPUT -eq 0 ]
then
    echo "upload on ${TAB} is success!!!!!"
else
    echo "ERROR ${OUTPUT} on upload ${TAB}; Check the log...exiting" | mailx -s "ERROR:upload ${TAB}" ushresth@peapod.com
    echo "ERROR ${OUTPUT} ...exiting"
    exit
fi

)

create()
(
TAB=$1

echo "Running create ${TAB}..."
cd $PDIR
./kitchen.sh -file="$CDIR/create/crt_redshift_tab.kjb" -param:tab="$TAB" -Level=ERROR > $LDIR/log.create_${TAB} 2>&1

checklogs log.create_${TAB}
OUTPUT=$?
if [ $OUTPUT -eq 0 ]
then
    echo "create on ${TAB} is success!!!!!"
else
    echo "ERROR ${OUTPUT} on create ${TAB}; Check the log...exiting" | mailx -s "ERROR:create ${TAB}" ushresth@peapod.com
    echo "ERROR ${OUTPUT} ...exiting"
    exit
fi
)

copy()
(
DB=$1
TAB=$2

echo "Running copy ${TAB}..."
cd $PDIR
./kitchen.sh -file="$CDIR/copy/cpy_redshift.kjb" -param:db="$DB" -param:tab="$TAB" -Level=ERROR > $LDIR/log.copy_${TAB} 2>&1

checklogs log.copy_${TAB}
OUTPUT=$?
if [ $OUTPUT -eq 0 ]
then
    echo "copy on ${TAB} is success!!!!!"
else
    echo "ERROR ${OUTPUT} on copy ${TAB}; Check the log...exiting" | mailx -s "ERROR:copy ${TAB}" ushresth@peapod.com
    echo "ERROR ${OUTPUT} ...exiting"
    exit
fi
)

merge()
(

DB=$1
TAB=$2

echo " Running merge ${TAB} ..."
cd $PDIR
./kitchen.sh -file="$CDIR/create/crt_redshift_tab.kjb" -param:tab="${TAB}_stage" -Level=ERROR > $LDIR/log.crt_stage_${TAB} 2>&1

checklogs log.crt_stage_${TAB} 
OUTPUT=$?
if [ $OUTPUT -eq 0 ]
then
    continue
else 
    echo "ERROR ${OUTPUT} on creating stage ${TAB}; Check the log...exiting" | mailx -s "ERROR:creating staging ${TAB}" ushresth@peapod.com
    echo "ERROR ${OUTPUT} ...exiting"
    exit
fi

./kitchen.sh -file="$CDIR/copy/cpy_mrg_redshift.kjb" -param:db="$DB" -param:tab="$TAB" -Level=ERROR > $LDIR/log.mrg_copy_${TAB} 2>&1

checklogs log.mrg_copy_${TAB} 
OUTPUT=$?
if [ $OUTPUT -eq 0 ]
then
    continue
else
    echo "ERROR ${OUTPUT} on merge copy ${TAB}; Check the log...exiting" | mailx -s "ERROR:merge copy ${TAB}" ushresth@peapod.com
    echo "ERROR ${OUTPUT} ...exiting"
    exit
fi

./kitchen.sh -file="$CDIR/merge/mrg_redshift_tab.kjb" -param:tab="$TAB" -Level=ERROR > $LDIR/log.mrg_${TAB} 2>&1

checklogs log.mrg_${TAB} 
OUTPUT=$?
if [ $OUTPUT -eq 0 ]
then
    echo "Merge on ${TAB} is success!!!!!" 
else
    echo "ERROR ${OUTPUT} on merge DML ${TAB}; Check the log...exiting" | mailx -s "ERROR:merge DML ${TAB}" ushresth@peapod.com
    echo "ERROR ${OUTPUT} ...exiting"
    exit
fi
)
################
# MAIN
################
case $1 in
    upl) shift
         if [ $# -eq 4 ]
         then
             DB=$1
             TAB=$2
             COL=$3
             DY=$4 
             upload $DB $TAB $COL $DY
         elif [ $# -eq 2 ]
         then
             DB=$1
             TAB=$2
             upload $DB $TAB
         else 
             echo "wrong arg" 
         fi;;
    cpy) shift
         DB=$1
         TAB=$2
         copy $DB $TAB;;
    crt) shift
         TAB=$1
         create $TAB;;
    mrg) shift
         DB=$1
         TAB=$2
         merge $DB $TAB;;
      *) echo "Nothing";;
esac
