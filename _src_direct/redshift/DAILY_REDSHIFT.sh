#!/bin/ksh

CDIR=/code/pentaho/redshift
LDIR=/logs/pentaho/redshift

rm -rf $LDIR/daily_test.log

#TABLES="gxt_ord_hdr gxt_it_act rdt_ord_sum"
TABLES="gxt_ord_hdr gxt_it_act rdt_ord_sum rdt_acn_hdr rdt_rpt_stor_lookup rdt_ecom_xref dw:ah_cat_pod"

for TAB in $TABLES
do

echo "Running $TAB"
date;

case $TAB in
rdt_ord_sum) $CDIR/REDSHIFT.sh upl datamart $TAB audt_upd_dt 0 >> $LDIR/daily_test.log 2>&1
             $CDIR/REDSHIFT.sh mrg datamart $TAB >> $LDIR/daily_test.log 2>&1
             ;;
gxt_it_act) $CDIR/REDSHIFT.sh upl cyborgdw $TAB arc_ld_dt 1 >> $LDIR/daily_test.log 2>&1
            $CDIR/REDSHIFT.sh mrg cyborgdw $TAB >> $LDIR/daily_test.log 2>&1
            ;;
gxt_ord_hdr) $CDIR/REDSHIFT.sh upl cyborgdw $TAB rqst_dt 1 >> $LDIR/daily_test.log 2>&1
             $CDIR/REDSHIFT.sh mrg cyborgdw $TAB >> $LDIR/daily_test.log 2>&1
            ;;
dw:*) db=`echo $TAB|sed -n 's/:.*//p'`;
    tab=`echo $TAB|sed -n 's/.*://p'`; 
    $CDIR/REDSHIFT.sh upl $db $tab >> $LDIR/daily_test.log 2>&1 
    $CDIR/REDSHIFT.sh crt $tab >> $LDIR/daily_test.log 2>&1
    $CDIR/REDSHIFT.sh cpy $db $tab >> $LDIR/daily_test.log 2>&1
   ;;
*) $CDIR/REDSHIFT.sh upl datamart $TAB >> $LDIR/daily_test.log 2>&1
   $CDIR/REDSHIFT.sh crt $TAB >> $LDIR/daily_test.log 2>&1 
   $CDIR/REDSHIFT.sh cpy datamart $TAB >> $LDIR/daily_test.log 2>&1
   ;;
esac

done

ERROR_N=`egrep -c "ERROR" $LDIR/daily_test.log`
if [ $ERROR_N -gt 0 ]
then
   mailx -s "ERROR:REDSHIFT DAILY" ushresth@peapod.com < $LDIR/daily_test.log
   echo "FAIL"
else
   echo "OK";
fi    
