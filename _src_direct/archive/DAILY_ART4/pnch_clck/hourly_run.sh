#!/bin/bash

#PDIR=/Applications/pentaho61/design-tools/data-integration
PDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/archive/DAILY_ART4/pnch_clck
LDIR=/logs/pentaho/archive/DAILY_ART4/pnch_clck

#DATATIME='16100711'
DATATIME=`date +'%y%m%d%H'`

cd $PDIR
./kitchen.sh -file="$CDIR/jb_pnch_clck.kjb" -param:DATATIME="${DATATIME}" -Level=ERROR > $LDIR/log.jb_pnch_clck 2>&1;
