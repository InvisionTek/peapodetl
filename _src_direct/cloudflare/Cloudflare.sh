#!/bin/ksh
if [ $# -eq 2 ]
then
  #DAY
  ARG=$1
  #HOUR
  ARG2=$2 
 
  if [ "$ARG2" -lt 1 ] || [ "$ARG2" -gt 24 ]
  then
    echo "                                  "
    echo "***** WRONG HOURS NUMBER: 1-24 only ************"
    echo "                                  "
    exit
  fi
else
  echo "                                              "
  echo "***** PARAM NOT DECLARE OR TOO MANY PARAM ****"
  echo "***** TWO PARAMETER ONLY: Cloudflare.sh $DAY $HOUR(1-24) ***" 
  echo "                                  "
  exit
fi

 
ARG3=$((${ARG2}-1))

PENTAHO_DI_JAVA_OPTIONS="-Xms1024m -Xmx4096m -XX:MaxPermSize=256m"
export PENTAHO_DI_JAVA_OPTIONS

#PDIR=/Applications/pentaho503/design-tools/data-integration
PDIR=/opt/pentaho/design-tools/data-integration

CDIR=/code/pentaho/cloudflare
LDIR=/logs/pentaho/cloudflare

#TDATE=`date -v-${ARG}d +%m%d%00000%y`
#UDATE=`date -j ${TDATE} +%s`
TDATE=`date --date="${ARG} day ago" +"%a %b %d 00:00:00 %Z %Y"`
UDATE=`date --date="$TDATE" +"%s"`

EDATE=$((${UDATE}+(($ARG2*((60*60))))))
SDATE=$((${UDATE}+(($ARG3*((60*60))))))
echo $SDATE $EDATE

cd $PDIR
./kitchen.sh -file="$CDIR/jb_flare_log.kjb" -param:SDATE="$SDATE" -param:EDATE="$EDATE" -Level=ERROR > $LDIR/log.cloud_flare 2>&1
