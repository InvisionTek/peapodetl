#!/bin/ksh

CDIR=/code/pentaho/cloudflare
#HR="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24"
HR=" 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24"

i=0;
#j=41;
while [ $i -le 0 -a $i -ge 0 ];
do

  for hr in $HR
  do 
    echo "I am day $i and hour $hr"
    $CDIR/Cloudflare.sh $i $hr
  done
  ((i-=1));
done 
