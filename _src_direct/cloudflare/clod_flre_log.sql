drop table if exists clod_flre_log;
create table clod_flre_log
(
log_dttm timestamp sortkey,
rayId varchar(30),
client_ip varchar(100),
client_country varchar(100),
client_asNum varchar(100),
clientRequest_httpHost varchar(100),
clientRequest_uri varchar(3000),
clientRequest_userAgent varchar(600) distkey,
edgeResponse_status varchar(100)
);
grant select on all tables in schema public to group "analytics";
grant all on all tables in schema public to group "dba";
