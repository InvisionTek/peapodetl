#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

i=5007;
while [ $i -le 5007 -a $i -gt 0 ];
do
date;
echo "Running $i day";
 $CDIR/REDSHIFT.sh upl archive art_offr_trk backed audt_upd_dt_tm $i;
 ((i-=1));
done
