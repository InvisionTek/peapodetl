#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

rm -f $LDIR/dly_mrg_upl.log

TABLES="cyborgdw:gxt_ord_hdr cyborgdw:gxt_it_act datamart:rdt_ord_sum"

#Load to S3
for TAB in $TABLES
do

db=`echo $TAB|sed -n 's/:.*//p'`;
tab=`echo $TAB|sed -n 's/.*://p'`;

echo "Running upload merge table $tab"
date;
case $tab in
gxt_ord_hdr) $CDIR/REDSHIFT.sh upl $db ${tab} daily rqst_dt 0 >> $LDIR/dly_mrg_upl.log 2>&1;;
gxt_it_act)  $CDIR/REDSHIFT.sh upl $db ${tab} daily arc_ld_dt  0 >> $LDIR/dly_mrg_upl.log 2>&1;;
rdt_ord_sum) $CDIR/REDSHIFT.sh upl $db ${tab} daily audt_upd_dt 0 >> $LDIR/dly_mrg_upl.log 2>&1;;
esac
done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_mrg_upl.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED when uploading merge table"
   mailx -s "Failed on uploading merge table.existing here...merge load not running!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_mrg_upl.log
   exit;
else
   echo "OK"
fi

#sleep 10 sec
sleep 10

rm -f $LDIR/dly_mrg_ld.log

#Load merge to redshift 
for TAB in $TABLES
do

db=`echo $TAB|sed -n 's/:.*//p'`;
tab=`echo $TAB|sed -n 's/.*://p'`;

echo "Running load merge table $tab"
date;

$CDIR/REDSHIFT.sh mrg $db $tab >> $LDIR/dly_mrg_ld.log 2>&1;

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_mrg_ld.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while merge load $tab"
   mailx -s "Failed while merge load table $tab...check log!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_mrg_ld.log
else
   echo "OK"
fi

OTHER="art_offr_trk"
for other in $OTHER
do

echo "Running upload merge table $other"
date;
case $other in
art_offr_trk) $CDIR/DAILY_OFFR_MRG.sh > $LDIR/DAILY_OFFR_MRG.log 2>&1
esac
done
