#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

rm -f $LDIR/dly_dim_ms_upl.log

TABLES="datamart:dim_ms_stor datamart:dim_ms_ord datamart:dim_ms_item datamart:fact_dly_ms"

#Load to S3
for TAB in $TABLES
do

db=`echo $TAB|sed -n 's/:.*//p'`;
tab=`echo $TAB|sed -n 's/.*://p'`;

echo "Running upload merge table $tab"
date;
case $tab in
fact_dly_ms) $CDIR/REDSHIFT.sh upl $db ${tab} daily arc_ld_dt 0 >> $LDIR/dly_dim_ms_upl.log 2>&1;;
dim_ms_stor) $CDIR/REDSHIFT.sh upl $db ${tab} daily audt_upd_dt 0 >> $LDIR/dly_dim_ms_upl.log 2>&1;;
dim_ms_ord) $CDIR/REDSHIFT.sh upl $db ${tab} daily audt_upd_dt 0 >> $LDIR/dly_dim_ms_upl.log 2>&1;;
dim_ms_item) $CDIR/REDSHIFT.sh upl $db ${tab} daily audt_upd_dt 0 >> $LDIR/dly_dim_ms_upl.log 2>&1;;
dim_ms_ppass) $CDIR/REDSHIFT.sh upl $db ${tab} daily arc_ld_dt 0 >> $LDIR/dly_dim_ms_upl.log 2>&1;;
fact_dly_ppass) $CDIR/REDSHIFT.sh upl $db ${tab} daily arc_ld_dt 0  >> $LDIR/dly_dim_ms_upl.log 2>&1;;
esac
done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_dim_ms_upl.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED when uploading merge table"
   mailx -s "Failed on uploading merge table.existing here...merge load not running!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_dim_ms_upl.log
   exit;
else
   echo "OK"
fi

#sleep 10 sec
sleep 10

rm -f $LDIR/dly_dim_ms_ld.log

#Load merge to redshift
for TAB in $TABLES
do

db=`echo $TAB|sed -n 's/:.*//p'`;
tab=`echo $TAB|sed -n 's/.*://p'`;

echo "Running load merge table $tab"
date;

$CDIR/REDSHIFT.sh mrg $db $tab >> $LDIR/dly_dim_ms_ld.log 2>&1;

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_dim_ms_ld.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while merge load $tab"
   mailx -s "Failed while merge load table $tab...check log!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_dim_ms_ld.log
else
   echo "OK"
fi
