#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

rm -f $LDIR/dly_big_upl.log

BIG="daily_curr:cdt_mgrp_it_mstr"

for big in $BIG 
do

db=`echo $big|sed -n 's/:.*//p'`;
tab=`echo $big|sed -n 's/.*://p'`;

echo "Running daily upload table $tab"
date;

$CDIR/REDSHIFT.sh upl $db $tab big >> $LDIR/dly_big_upl.log 2>&1

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_big_upl.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while uploading big table"
   mailx -s "Failed on uploading big table...existing here!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_big_upl.log
   exit;
else
   echo "OK"
fi

#sleep
sleep 5

rm -f $LDIR/dly_big_crt.log

#create table
for big in $BIG
do

db=`echo $big|sed -n 's/:.*//p'`;
tab=`echo $big|sed -n 's/.*://p'`;

echo "Running daily big create table $tab"
date;

$CDIR/REDSHIFT.sh crt $tab >> $LDIR/dly_big_crt.log 2>&1

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_big_crt.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while creating big table...exiting here"
   mailx -s "Failed on creating big table!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_big_crt.log
   exit;
else
   echo "OK"
fi

#sleep
sleep 5

rm -f $LDIR/dly_big_ld.log

for big in $BIG
do

db=`echo $big|sed -n 's/:.*//p'`;
tab=`echo $big|sed -n 's/.*://p'`;

echo "Running loading big table $tab"
date;

$CDIR/REDSHIFT.sh cpy $db $tab daily >> $LDIR/dly_big_ld.log 2>&1

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_big_ld.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while big load...check log "
   mailx -s "FAILED while big load table!!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_big_ld.log
else
   echo "OK"
fi
