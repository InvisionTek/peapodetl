#!/bin/ksh
###################
# Variables
###################
SCRIPT=`basename $0`
#PDIR=/Applications/pentaho60/design-tools/data-integration
PDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

###############
# Functions
##############
#usage
usage()
(
  echo " 	USAGE						"
  echo "	syntax <crt/upl/cpy/mrg> <table name>	        "
  echo "        syntax <upl backed/daily>                       "
) 

#check logs
checklogs()
( 
   LOGFILE=$1
   
   cd $LDIR
   #ERRCNT=`cat $LOGFILE|egrep -v "ERROR*"|egrep -c "Kitchen - ERROR|FAILURE"`
   ERRCNT=`cat $LOGFILE|egrep -c "Finished with errors|Couldn't execute SQL|Invalid operation:|Could not read from"|egrep -v "ERROR*"`
   #ERRCNT=`egrep -c "ERROR|FAILURE" $LOGFILE`
   ERRCNNT=`egrep -c "not connected" $LOGFILE`
   if [ $ERRCNT -gt 0 ]
   then
      MSG=1
   elif [ $ERRCNNT -gt 0 ]
   then
      MSG=2
   else
      MSG=0
   fi 
   return $MSG
)

upload()
(
DB=$1
TAB=$2
TYPE=$3
COL=$4
DY=$5

echo "Running upload ${TAB}..."
num=$#
if [ $num -eq 5 ]
then

  if [ "$TYPE" = "backed" ]
  then
    TDATE=_`/bin/date --date="$DY day ago" +%Y%m%d`
    #TDATE=`date -v-${DY}d +%Y%m%d`
    continue
  elif [ "$TYPE" = "daily" ]
  then
     TDATE=""
     continue 
  else
    echo "wrong type in first argument"
    usage
    exit
  fi
  
  cd $PDIR
  ./pan.sh -file="$CDIR/upload/upl_in_$TYPE.ktr" -param:type="$TYPE" -param:db="$DB" -param:tab="$TAB" -param:col="$COL" -param:dy="$DY" -param:tdate="$TDATE" -Level=ERROR > $LDIR/log.upload_${TAB} 2>&1 

elif [ $num -eq 3 ]
then
  if [ "$TYPE" = "small" ]
  then
    cd $PDIR
    ./pan.sh -file="$CDIR/upload/upl_in_daily_$TYPE.ktr" -param:db="$DB" -param:tab="$TAB" -Level=ERROR > $LDIR/log.upload_${TAB} 2>&1
  elif [ "$TYPE" = "big" ]
  then
    cd $PDIR
    ./kitchen.sh -file="$CDIR/upload/upl_in_daily_$TYPE.kjb" -param:db="$DB" -param:tab="$TAB" -Level=ERROR > $LDIR/log.upload_${TAB} 2>&1
  else  
    echo "wrong type in first argument"
    usage
    exit
  fi   
     
else
    echo "wrong number of arguement"  
    usage
    exit
fi

checklogs log.upload_${TAB} 
OUTPUT=$?
if [ $OUTPUT -eq 0 ]
then
    echo "upload on ${TAB} is success!!!!!"
else
    echo "ERROR ${OUTPUT} on upload ${TAB}; Check the log...exiting" | mailx -s "ERROR:upload ${TAB}" ushresth@peapod.com,nmiles@peapod.com
    echo "ERROR ${OUTPUT} ...exiting"
    exit
fi

)

create()
(
TAB=$1

echo "Running create ${TAB}..."
cd $PDIR
./kitchen.sh -file="$CDIR/create/crt_redshift_tab.kjb" -param:tab="$TAB" -Level=ERROR > $LDIR/log.create_${TAB} 2>&1

checklogs log.create_${TAB}
OUTPUT=$?
if [ $OUTPUT -eq 0 ]
then
    echo "create on ${TAB} is success!!!!!"
else
    echo "ERROR ${OUTPUT} on create ${TAB}; Check the log...exiting" | mailx -s "ERROR:create ${TAB}" ushresth@peapod.com,nmiles@peapod.com
    echo "ERROR ${OUTPUT} ...exiting"
    exit
fi
)

copy()
(
DB=$1
TAB=$2
TYPE=$3

echo "Running copy ${TAB} $TYPE..."
cd $PDIR
./kitchen.sh -file="$CDIR/copy/cpy_${TYPE}_redshift.kjb" -param:db="$DB" -param:tab="$TAB" -Level=ERROR > $LDIR/log.copy_${TAB} 2>&1

checklogs log.copy_${TAB}
OUTPUT=$?
if [ $OUTPUT -eq 0 ]
then
    echo "copy on ${TAB} is success!!!!!"
else
    echo "ERROR ${OUTPUT} on copy ${TAB}; Check the log...exiting" | mailx -s "ERROR:copy ${TAB}" ushresth@peapod.com,nmiles@peapod.com
    echo "ERROR ${OUTPUT} ...exiting"
    exit
fi
)

merge()
(

DB=$1
TAB=$2

rm -f $LDIR/log.mrg_${TAB}

cd $PDIR

#run create stage
./kitchen.sh -file="$CDIR/create/crt_redshift_tab.kjb" -param:tab="${TAB}_stage" -Level=ERROR >> $LDIR/log.mrg_${TAB} 2>&1

#run stage load
./kitchen.sh -file="$CDIR/copy/cpy_daily_mrg_redshift.kjb" -param:db="$DB" -param:tab="$TAB" -Level=ERROR >> $LDIR/log.mrg_${TAB} 2>&1

#run merge sql
./kitchen.sh -file="$CDIR/merge/mrg_redshift_tab.kjb" -param:tab="$TAB" -Level=ERROR >> $LDIR/log.mrg_${TAB} 2>&1

checklogs log.mrg_${TAB} 
OUTPUT=$?
if [ $OUTPUT -eq 0 ]
then
    echo "Merge on ${TAB} is success!!!!!" 
else
    echo "ERROR ${OUTPUT} on merge DML ${TAB}; Check the log...exiting" | mailx -s "ERROR:merge DML ${TAB}" ushresth@peapod.com,nmiles@peapod.com
    echo "ERROR ${OUTPUT}...exiting"
    exit
fi
)
################
# MAIN
################
case $1 in
    upl) shift
         if [ $# -eq 5 ]
         then
             DB=$1
             TAB=$2
             TYPE=$3
             COL=$4
             DY=$5
             upload $DB $TAB $TYPE $COL $DY 
         elif [ $# -eq 3 ]
         then
             DB=$1
             TAB=$2
             TYPE=$3
             upload $DB $TAB $TYPE
         else 
             echo "wrong arg" 
         fi;;
    cpy) shift
         DB=$1
         TAB=$2
         TYPE=$3
         copy $DB $TAB $TYPE;;
    crt) shift
         TAB=$1
         create $TAB;;
    mrg) shift
         DB=$1
         TAB=$2
         merge $DB $TAB;;
      *) echo "Nothing";;
esac
