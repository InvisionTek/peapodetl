#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

i=160;
while [ $i -le 160 -a $i -gt 0 ];
do
date;
echo "Running $i day";
 $CDIR/REDSHIFT.sh upl datamart rdt_mktg_offr_dtl backed dlv_dt $i;
 ((i-=1));
done
