grant select on table gxt_it_act_dtl to group "analytics";
grant select on table gxt_it_act_dtl to group "dba";
grant select on table gxt_ord_ctnr to group "analytics";
grant select on table gxt_ord_ctnr to group "dba";
grant select on table gxt_ord_rtrn to group "analytics";
grant select on table gxt_ord_rtrn to group "dba";
grant select on table cdt_mgrp_it_mstr to group "analytics";
grant select on table cdt_mgrp_it_mstr to group "dba";
grant select on table cdt_omt_atr_val to group "analytics";
grant select on table cdt_omt_atr_val to group "dba";
