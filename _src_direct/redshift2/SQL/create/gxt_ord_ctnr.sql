drop table if exists gxt_ord_ctnr;
create table gxt_ord_ctnr
  (
    ord_id varchar(10),
    ctnr_type_cd varchar(10),
    ctnr_id integer,
    it_dnld_cd char(1),
    rack_loc_id varchar(6),
    arc_ld_dt date,
    primary key(ord_id,ctnr_type_cd,ctnr_id)
  )
distkey(ord_id)
interleaved sortkey(arc_ld_dt,rack_loc_id);
grant select on table gxt_ord_ctnr to group "dba";
grant select on table gxt_ord_ctnr to group "analytics";
