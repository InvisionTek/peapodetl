drop table if exists cdt_mahn_item_cbo;
create table cdt_mahn_item_cbo
  (
    item_id integer,
    whse varchar(4),
    item_name varchar(30),
    description varchar(30),
    unit_weight decimal(10,5),
    weight_uom_id integer,
    unit_volume decimal(10,5),
    volume_uom_id integer,
    primary key(item_id,whse)
  )
distkey(item_id)
sortkey(item_id);
grant select on table cdt_mahn_item_cbo to group "dba";
grant select on table cdt_mahn_item_cbo to group "analytics";
