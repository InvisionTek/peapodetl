drop table if exists cdt_it_dict_cat;
create table cdt_it_dict_cat 
  (
    it_dict_cat_id integer,
    cat_type_cd varchar(10),
    cat_tx varchar(100),
    primary key(it_dict_cat_id,cat_type_cd)
)
distkey(cat_tx)
interleaved sortkey(it_dict_cat_id,cat_type_cd,cat_tx);
grant select on table cdt_it_dict_cat to group "dba";
grant select on table cdt_it_dict_cat to group "analytics";
