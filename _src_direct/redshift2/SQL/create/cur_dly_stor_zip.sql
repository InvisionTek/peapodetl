drop table if exists cur_dly_stor_zip;
create table cur_dly_stor_zip 
 (
    zip_city_id integer,
    zip_cd varchar(10),
    city_tx varchar(20),
    st_cd varchar(3),
    stor_id varchar(7),
    mbr_type_cd char(1),
    dlv_area_id smallint,
    svc_type_cd char(1),
    pup_id smallint,
    stop_time_cd smallint,
    tax_grp_cd varchar(3),
    rpt_stor_id varchar(4),
    prom_pr_zone_cd smallint,
    base_pr_zone_cd smallint,
    time_diff_qy smallint,
    subs_pr_tier_id smallint,
    primary key(zip_city_id)
 )
distkey(zip_cd)
interleaved sortkey(zip_cd,stor_id,dlv_area_id,city_tx);
grant select on table cur_dly_stor_zip to group "dba";
grant select on table cur_dly_stor_zip to group "analytics";
