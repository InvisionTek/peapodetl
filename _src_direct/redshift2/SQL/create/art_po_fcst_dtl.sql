drop table if exists art_po_fcst_dtl;
create table art_po_fcst_dtl 
(
po_id integer,
pod_id integer,
ord_case_qy smallint,
vend_rtio_qy smallint,
on_hand_qy integer,
on_ord_qy integer,
sfty_fact_qy decimal(7,2),
lift_fact_qy decimal(10,2),
lift_fact_strt_dt date,
lift_fact_end_dt date,
sale_cd char(1),
aou_0_qy decimal(14,7),
aou_1_qy decimal(14,7),
aou_2_qy decimal(14,7),
aou_3_qy decimal(14,7),
aou_4_qy decimal(14,7),
aou_5_qy decimal(14,7),
aou_6_qy decimal(14,7),
prch_case_bulk_ord_qy smallint,
fwd_buy_qy integer,
xpct_dmnd_qy decimal(9,2),
load_bal_case_qy smallint,
pre_po_expt_use_qy decimal(7,2),
pre_load_bal_ord_qy decimal(7,2),
po_prd_expt_use_qy decimal(7,2)
)
distkey(po_id)
interleaved sortkey(pod_id);
grant select on table art_po_fcst_dtl to group "dba";
grant select on table art_po_fcst_dtl to group "analytics";
