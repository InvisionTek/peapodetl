drop table if exists cdt_cnsm_dlv;
create table cdt_cnsm_dlv 
  (
    cnsm_id varchar(10),
    last_name_tx varchar(40),
    frst_name_tx varchar(30),
    mi_tx char(1),
    adr_1_tx varchar(40),
    adr_2_tx varchar(40),
    city_tx varchar(30),
    st_cd char(2),
    zip_cd varchar(10),
    day_phon_cd varchar(15),
    day_phon_ext_cd varchar(5)
  )
distkey(cnsm_id)
sortkey(cnsm_id);
grant select on table cdt_cnsm_dlv to group "dba";
grant select on table cdt_cnsm_dlv to group "analytics";
