drop table if exists rdt_mktg_offr_dtl;
create table rdt_mktg_offr_dtl
  (
    stor_id varchar(5),
    zip_cd varchar(5),
    cnsm_id varchar(10),
    ord_id varchar(10),
    dlv_dt date,
    cpn_type_cd varchar(5),
    offr_cd varchar(50),
    cpn_cd varchar(50),
    offr_amt decimal(20,2),
    primary key(ord_id,offr_cd,cpn_cd) 
 )
distkey(ord_id)
interleaved sortkey(dlv_dt,cnsm_id,offr_cd,cpn_cd,zip_cd);
grant select on table rdt_mktg_offr_dtl to group "dba";
grant select on table rdt_mktg_offr_dtl to group "analytics";
