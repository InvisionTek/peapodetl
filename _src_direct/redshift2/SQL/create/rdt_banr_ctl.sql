drop table if exists rdt_banr_ctl;
create table rdt_banr_ctl 
(
banr_ad_id integer,
banr_tx varchar(50)
 ) 
diststyle all;
grant select on table rdt_banr_ctl to group "dba";
grant select on table rdt_banr_ctl to group "analytics";
