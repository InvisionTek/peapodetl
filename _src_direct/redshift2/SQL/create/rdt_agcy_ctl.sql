drop table if exists rdt_agcy_ctl; 
create table rdt_agcy_ctl 
 (
  agcy_id integer,
  agcy_tx varchar(50),
  emkt_fl char(1)
 )
diststyle all;
grant select on table rdt_agcy_ctl to group "dba";
grant select on table rdt_agcy_ctl to group "analytics";
