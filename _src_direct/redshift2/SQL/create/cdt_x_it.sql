drop table if exists cdt_x_it;
create table cdt_x_it
  (
    stor_id varchar(7),
    pod_id integer,
    upc_cd decimal(14,0),
    sku_1_cd integer,
    sku_2_cd integer,
    hi_cone_cd char(1),
    unit_scal_qy decimal(7,2),
    pr_scal_qy decimal(7,4),
    pr_meth_cd char(2),
    pr_src_cd char(1),
    pr_qy decimal(6,2),
    it_tax_qy decimal(7,5),
    spec_cd char(1),
    loc_cd varchar(10),
    it_shop_cd char(1),
    actv_stat_cd char(1),
    old_actv_stat_cd char(1),
    actv_stat_upd_dt date,
    supl_cd varchar(3),
    btl_dep_qy decimal(4,2),
    sub_pod_id integer,
    sub_qy smallint,
    reg_pr_qy decimal(6,2),
    on_hand_qy integer,
    avg_day_sold_qy integer,
    mgrp_stat_cd char(1),
    it_long_name_tx varchar(100),
    it_size_cd varchar(11),
    it_dict_cat_id integer,
    majr_cat_id integer,
    uprm_actv_cd char(1),
    audt_cr_id varchar(8),
    audt_cr_dt date,
    audt_upd_id varchar(8),
    audt_upd_dt date,
    primary key(stor_id,pod_id)
)
distkey(pod_id)
interleaved sortkey(stor_id,pod_id,upc_cd);
grant select on table cdt_x_it to group "dba";
grant select on table cdt_x_it to group "analytics";
