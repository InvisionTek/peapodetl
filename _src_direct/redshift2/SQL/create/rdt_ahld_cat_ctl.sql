drop table if exists rdt_ahld_cat_ctl;
create table rdt_ahld_cat_ctl
  (
    dept integer,
    dept_grp_id varchar(3),
    dept_desc varchar(25),
    cat integer,
    cat_grp_id varchar(3),
    cat_desc varchar(25),
    sub_cat integer,
    sub_cat_grp_id varchar(3),
    sub_cat_desc varchar(25)
 ) 
diststyle all;
grant select on table rdt_ahld_cat_ctl to group "dba";
grant select on table rdt_ahld_cat_ctl to group "analytics";
