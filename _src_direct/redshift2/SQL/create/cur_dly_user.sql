drop table if exists cur_dly_user;
create table cur_dly_user 
  (
    user_id integer,
    cnsm_id varchar(10),
    mail_tx varchar(80),
    mail_opt_news_cd char(1),
    mail_opt_spec_cd char(1),
    rtlr_card_id varchar(50),
    rtlr_card_trk_id varchar(50)
  )
distkey(user_id)
sortkey(cnsm_id);
grant select on table cur_dly_user to group "dba";
grant select on table cur_dly_user to group "analytics";
