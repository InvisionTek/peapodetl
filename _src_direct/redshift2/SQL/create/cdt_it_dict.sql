drop table if exists cdt_it_dict;
create table cdt_it_dict 
  (
    pod_id integer,
    it_name_tx varchar(70),
    it_size_cd varchar(15),
    it_uom_cd varchar(6),
    unit_scal_qy decimal(7,2),
    pr_scal_qy decimal(7,4),
    actv_cd char(1),
    proc_cd char(1),
    majr_cat_id integer,
    it_dict_cat_id integer,
    evnt_prod_id integer,
    it_long_name_tx varchar(100),
    addl_srch_tx varchar(256),
    brnd_id integer,
    adv_id integer,
    audt_cr_id varchar(8),
    audt_cr_dt date,
    audt_upd_id varchar(8),
    audt_upd_dt date,
    ingr_flag_cd char(1),
    pkg_flag_cd char(1),
    rule_id integer,
    ntrn_fact_cd char(1),
    primary key(pod_id)
)
distkey(pod_id)
interleaved sortkey(it_name_tx,it_dict_cat_id,it_long_name_tx,addl_srch_tx);
grant select on table cdt_it_dict to group "dba";
grant select on table cdt_it_dict to group "analytics";
