drop table if exists rdt_jump_page_ctl;
create table rdt_jump_page_ctl
(
    jump_page_id integer,
    jump_page_tx varchar(50),
    jump_page_msg varchar(10),
    cpn_cd varchar(20)
)
diststyle all;
grant select on table rdt_jump_page_ctl to group "dba";
grant select on table rdt_jump_page_ctl to group "analytics";
