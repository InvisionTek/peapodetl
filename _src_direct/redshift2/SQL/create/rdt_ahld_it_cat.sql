drop table if exists rdt_ahld_it_cat;
create table rdt_ahld_it_cat 
(   
    pod_id integer,
    dept_desc varchar(50),
    cat_desc varchar(50),
    sub_cat_desc varchar(50),
    primary key(pod_id)
)
distkey(pod_id)
interleaved sortkey(pod_id);
grant select on table rdt_ahld_it_cat to group "dba";
grant select on table rdt_ahld_it_cat to group "analytics";
