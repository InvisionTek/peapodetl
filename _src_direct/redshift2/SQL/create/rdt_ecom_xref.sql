drop table if exists rdt_ecom_xref;
create table rdt_ecom_xref
(
    ecom_stor_id integer,
    stor_id varchar(3),
    city_tx varchar(30),
    short_name varchar(10),
    opco_name varchar(30),
    sort_num integer,
    mkt_id char(2),
    mkt_desc_tx varchar(30),
    sns_mkt_area_tx varchar(20),
    sns_stor_no smallint,
    sns_zone smallint,
    sns_stor_id_no varchar(8),
    min_dlv_dt date,
    price_zone integer,
    fin_stor_id varchar(3),
    sns_cat_id integer,
    mwg_plat_id integer,
    wms_cd varchar(4)
 ) 
diststyle all;
grant select on table rdt_ecom_xref to group "dba";
grant select on table rdt_ecom_xref to group "analytics";
