drop table if exists gxt_it_act_dtl;
create table gxt_it_act_dtl
  (
    ord_id varchar(10),
    line_num_cd smallint,
    orig_pod_id integer,
    sub_pod_id integer,
    it_wgt_qy decimal(7,4),
    arc_ld_dt date,
    primary key(ord_id,line_num_cd)
  )
distkey(ord_id)
interleaved sortkey(arc_ld_dt,orig_pod_id);
grant select on table gxt_it_act_dtl to group "dba";
grant select on table gxt_it_act_dtl to group "analytics";
