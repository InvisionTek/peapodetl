drop table if exists rdt_site_ctl;
create table rdt_site_ctl
(
site_id varchar(10),
site_name varchar(50),
emkt_fl char(1),
site_cat_tx varchar(50),
primary key(site_id)
)
diststyle all;
grant select on table rdt_site_ctl to group "dba";
grant select on table rdt_site_ctl to group "analytics";
