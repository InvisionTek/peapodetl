drop table if exists ah_cat_pod;
create table ah_cat_pod 
(pod_id integer,
ah_dept varchar(50),
ah_cat varchar(50),
ah_sub_cat varchar(50)
)
diststyle all;
grant select on table ah_cat_pod to group "dba";
grant select on table ah_cat_pod to group "analytics";
