drop table if exists rdt_rpt_stor_lookup;
create table rdt_rpt_stor_lookup
  (
    rpt_stor_id varchar(4),
    stor_id varchar(3),
    rpt_stor_desc varchar(20),
    cet_stor_id varchar(4),
    mktg_stor_id varchar(4),
    mktg_stor_desc varchar(20),
    opco_id varchar(4),
    sort_num_cd smallint,
    cet_ivte_cnt smallint,
    dvsn_cd varchar(4),
    othr_grp_id varchar(10),
    primary key(rpt_stor_id)
)
diststyle all;
grant select on table rdt_rpt_stor_lookup to group "dba";
grant select on table rdt_rpt_stor_lookup to group "analytics";
