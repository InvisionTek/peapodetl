drop table if exists cdt_cont_ctl;
create table cdt_cont_ctl 
  (
    cont_id integer,
    app_type_cd varchar(5),
    file_ext_cd varchar(10),
    file_loc_cd char(1),
    cont_name_tx varchar(50),
    file_name_tx varchar(400),
    cont_ttl_tx varchar(50),
    src_cd varchar(20),
    audt_cr_id varchar(10),
    audt_cr_dt date,
    audt_upd_id varchar(10),
    audt_upd_dt date
)
distkey(cont_name_tx)
interleaved sortkey(cont_name_tx,cont_id);
grant select on table cdt_cont_ctl to group "dba";
grant select on table cdt_cont_ctl to group "analytics";
