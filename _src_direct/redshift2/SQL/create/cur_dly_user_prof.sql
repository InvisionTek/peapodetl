drop table if exists cur_dly_user_prof;
create table cur_dly_user_prof
  (
    user_id integer,
    cnsm_id varchar(10),
    stor_id varchar(10),
    stat_cd char(1),
    type_cd char(1),
    ecom_stor_id smallint,
    dlv_st_cd char(2),
    dlv_zip_cd char(5),
    dlv_area_id smallint,
    time_ord_qy smallint,
    pup_time_ord_qy smallint,
    last_sess_dt timestamp,
    frst_ord_dt date,
    frst_dlv_dt date,
    frst_pup_dt date,
    frst_svc_dt date,
    last_dlv_dt date,
    last_pup_dt date,
    last_svc_dt date,
    bpi_svc_rec_qy smallint,
    mktg_1_targ_cd char(1),
    primary key(user_id)
  )
distkey(user_id)
interleaved sortkey(cnsm_id,stor_id,ecom_stor_id,frst_ord_dt,frst_dlv_dt,frst_pup_dt,frst_svc_dt,last_svc_dt);
grant select on table cur_dly_user_prof to group "dba";
grant select on table cur_dly_user_prof to group "analytics";
