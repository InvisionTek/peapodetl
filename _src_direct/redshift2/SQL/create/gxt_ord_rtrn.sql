drop table if exists gxt_ord_rtrn;
create table gxt_ord_rtrn
  (
    ord_id varchar(10),
    act_ord_id varchar(10),
    seq_id integer,
    pod_id integer,
    upc_cd decimal(14,0),
    it_name_tx varchar(50),
    act_cd char(1),
    rsn_cd varchar(6),
    it_wgt_qy decimal(11,4),
    it_pr_qy decimal(11,4),
    it_tax_qy decimal(7,5),
    it_tax_pct_qy decimal(7,6),
    btl_dep_qy decimal(11,2),
    thrd_pty_lqr_cd char(1),
    audt_cr_id varchar(8),
    audt_cr_dt date,
    audt_cr_tm timestamp,
    arc_ld_dt date,
    primary key(ord_id,seq_id)
  )
distkey(ord_id)
interleaved sortkey(arc_ld_dt,act_ord_id,pod_id,rsn_cd);
grant select on table gxt_ord_rtrn to group "dba";
grant select on table gxt_ord_rtrn to group "analytics";
