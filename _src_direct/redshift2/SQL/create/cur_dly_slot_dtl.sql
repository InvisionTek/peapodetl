drop table if exists cur_dly_slot_dtl;
create table cur_dly_slot_dtl 
  (
    slot_id integer,
    slot_hdr_id integer,
    dlv_area_id smallint,
    dlv_tm varchar(10),
    ship_type_cd varchar(20),
    dlv_wnd_tm varchar(20),
    slot_avl_qy smallint,
    disc_qy decimal(7,2),
    slot_stat_cd char(1),
    dlv_msg_id char(1),
    stor_id varchar(5),
    shft_num_cd smallint,
    dlv_dt date,
    cut_off_dt_tm timestamp,
    max_ord_qy smallint,
    slot_used_qy smallint,
    slot_hdr_used_qy smallint,
    primary key(slot_id)
)
distkey(slot_id)
interleaved sortkey(slot_stat_cd);
grant select on table cur_dly_slot_dtl to group "dba";
grant select on table cur_dly_slot_dtl to group "analytics";
