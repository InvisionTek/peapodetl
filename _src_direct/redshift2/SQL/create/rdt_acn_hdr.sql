drop table if exists rdt_acn_hdr;
create table rdt_acn_hdr
(
    pod_id integer,
    brnd_name_tx varchar(50),
    ownr_name_tx varchar(50),
    it_long_name_tx varchar(100),
    it_size_cd varchar(11),
    actv_cd char(1),
    arc_ld_dt date,
    arc_upd_dt date,
    primary key(pod_id)
)
distkey(pod_id)
interleaved sortkey(brnd_name_tx,ownr_name_tx,it_long_name_tx,arc_upd_dt);
grant select on table rdt_acn_hdr to group "dba";
grant select on table rdt_acn_hdr to group "analytics";
