drop table if exists cdt_mgrp_vend_mstr;
create table cdt_mgrp_vend_mstr 
  (
    vend_num char(6),
    vend_name char(50),
    vend_stat_cd char(1),
    bar_code_type_cd varchar(30),
    mail_name_tx varchar(50),
    num_sku_only_cd char(1),
    ahld_ord_book_cd char(2),
    case_rdy_meat_cd char(1)
 )
distkey(vend_num)
interleaved sortkey(vend_num);
grant select on table cdt_mgrp_vend_mstr to group "dba";
grant select on table cdt_mgrp_vend_mstr to group "analytics";
