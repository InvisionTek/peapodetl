drop table if exists rdt_wk_cgs_trk; 
create table rdt_wk_cgs_trk 
  (
    week_end_dt date,
    stor_id varchar(5),
    supl_cd varchar(5),
    cat_tx varchar(50),
    pod_id integer,
    ext_sale decimal(20,2),
    qty decimal(20,2),
    ext_cost decimal(20,2),
    unit_qy decimal(20,2)
 )
distkey(pod_id)
interleaved sortkey(week_end_dt,stor_id,supl_cd,cat_tx);
grant select on table rdt_wk_cgs_trk to group "dba";
grant select on table rdt_wk_cgs_trk to group "analytics";
