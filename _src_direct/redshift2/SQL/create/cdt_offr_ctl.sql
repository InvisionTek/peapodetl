drop table if exists cdt_offr_ctl;
create table cdt_offr_ctl 
  (
    offr_cd varchar(50),
    prgm_id integer,
    desc_tx varchar(70),
    web_disp_tx varchar(50),
    rdm_type_cd varchar(6),
    tot_allw_qy smallint,
    tot_rdm_qy smallint,
    user_allw_qy smallint,
    amt_offr_qy decimal(7,4),
    pod_id integer,
    pod_grp_id integer,
    it_cd varchar(5),
    offr_tax_cd char(1),
    actv_stat_cd char(1),
    cmnc_veh_cd varchar(15),
    est_rdm_qy smallint,
    lbly_per_rdm_qy decimal(5,2),
    cont_id integer,
    audt_cr_id varchar(8),
    audt_cr_dt_tm timestamp,
    audt_upd_id varchar(8),
    audt_upd_dt_tm timestamp,
    primary key(offr_cd)
 )
distkey(pod_id)
interleaved sortkey(pod_id,offr_cd,prgm_id,web_disp_tx,rdm_type_cd);
grant select on table cdt_offr_ctl to group "dba";
grant select on table cdt_offr_ctl to group "analytics";
