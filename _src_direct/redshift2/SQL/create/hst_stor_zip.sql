drop table if exists hst_stor_zip;
create table hst_stor_zip 
(
    zip_city_id integer,
    hist_seq_qy smallint,
    zip_cd varchar(10),
    city_tx varchar(20),
    st_cd char(3),
    stor_id char(7),
    mbr_type_cd char(1),
    dlv_area_id smallint,
    pup_id smallint,
    stop_time_cd smallint,
    tax_grp_cd char(3),
    rpt_stor_id char(4),
    prom_pr_zone_cd smallint,
    base_pr_zone_cd smallint,
    hist_begn_dt date,
    hist_end_dt date,
    hist_stat_cd char(1),
    primary key(zip_city_id)
)
distkey(zip_cd)
interleaved sortkey(stor_id,rpt_stor_id);
grant select on table hst_stor_zip to group "dba";
grant select on table hst_stor_zip to group "analytics";
