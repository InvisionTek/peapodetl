drop table if exists cdt_omt_atr_val;
create table cdt_omt_atr_val
  (
    atr_val_id integer,
    lvl_id smallint,
    othr_key_id integer,
    atr_id integer,
    atr_val_cd varchar(500),
    srce_cd char(1),
    audt_cr_dt_tm timestamp,
    audt_cr_user_cd varchar(15),
    audt_upd_dt_tm timestamp,
    audt_upd_user_cd varchar(15),
    primary key(atr_val_id)
  )
distkey(atr_val_id);
grant select on table cdt_omt_atr_val to group "dba";
grant select on table cdt_omt_atr_val to group "analytics";
