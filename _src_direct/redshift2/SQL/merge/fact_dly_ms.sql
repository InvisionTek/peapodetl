BEGIN;

update fact_dly_ms 
set whse_id_dlv_qy = a.whse_id_dlv_qy,
whse_it_dlv_amt = a.whse_it_dlv_amt,
arc_ld_dt = a.arc_ld_dt
from fact_dly_ms_stage a
where fact_dly_ms.time_key = a.time_key
and fact_dly_ms.ord_key = a.ord_key
and fact_dly_ms.stor_key = a.stor_key
and fact_dly_ms.pod_key = a.pod_key;

INSERT INTO fact_dly_ms 
SELECT t.* FROM fact_dly_ms_stage t
LEFT JOIN fact_dly_ms s
ON t.time_key = s.time_key
and t.ord_key = s.ord_key
and t.stor_key = s.stor_key
and t.pod_key = s.pod_key 
WHERE (s.time_key IS NULL AND s.ord_key IS NULL);

DROP TABLE fact_dly_ms_stage;

END;
