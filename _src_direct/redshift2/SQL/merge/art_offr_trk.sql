BEGIN;

INSERT INTO art_offr_trk 
SELECT t.* FROM art_offr_trk_stage t
LEFT JOIN art_offr_trk s
ON t.user_id = s.user_id
and t.list_id = s.list_id
and t.offr_cd = t.offr_cd
WHERE s.list_id IS NULL;

DROP TABLE art_offr_trk_stage;

END;
