BEGIN;

update dim_ms_stor 
set stor_id = a.stor_id,
rpt_name_cd = a.rpt_name_cd,
hist_strt_dt = a.hist_strt_dt,
hist_end_dt = a.hist_end_dt,
hist_actv_cd = a.hist_actv_cd,
audt_upd_dt = a.audt_upd_dt
from dim_ms_stor_stage a
where dim_ms_stor.stor_key = a.stor_key;

INSERT INTO dim_ms_stor 
SELECT t.* FROM dim_ms_stor_stage t
LEFT JOIN dim_ms_stor s
ON t.stor_key = s.stor_key
WHERE s.stor_key IS NULL;

DROP TABLE dim_ms_stor_stage;

END;
