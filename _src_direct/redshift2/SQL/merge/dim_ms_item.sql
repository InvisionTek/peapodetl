BEGIN;

update dim_ms_item 
set pod_id = a.pod_id,
it_long_name_tx = a.it_long_name_tx,
shed_bite_fl = a.shed_bite_fl,
sh_dept = a.sh_dept,
sh_cat = a.sh_cat,
sh_sub_cat = a.sh_sub_cat,
hist_actv_cd = a.hist_actv_cd,
hist_strt_dt = a.hist_strt_dt,
hist_end_dt = a.hist_end_dt,
audt_upd_dt = a.audt_upd_dt
from dim_ms_item_stage a
where dim_ms_item.pod_key = a.pod_key;

INSERT INTO dim_ms_item 
SELECT t.* FROM dim_ms_item_stage t
LEFT JOIN dim_ms_item s
ON t.pod_key = s.pod_key
WHERE s.pod_key IS NULL;

DROP TABLE dim_ms_item_stage;

END;
