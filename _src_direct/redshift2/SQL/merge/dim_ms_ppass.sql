BEGIN;

update dim_ms_ppass 
set user_dlv_subs_id = a.user_dlv_subs_id,
dlv_subs_id = a.dlv_subs_id,
purc_dt = a.purc_dt,
term_exp_dt = a.term_exp_dt,
pr_qy  = a.pr_qy,
tax_pr_qy = a.tax_pr_qy,
auto_renw_cd = a.auto_renw_cd,
stat_cd = a.stat_cd,
free_try_cd = a.free_try_cd,
audt_cr_dt = a.audt_cr_dt,
audt_upd_dt = a.audt_upd_dt, 
arc_ld_dt = a.arc_ld_dt,
from dim_ms_ppass_stage a
where dim_ms_ppass.ppass_key = a.ppass_key;

INSERT INTO dim_ms_ppass 
SELECT t.* FROM dim_ms_ppass_stage t
LEFT JOIN dim_ms_ppass s
ON t.ppass_key = s.ppass_key
WHERE s.ppass_key IS NULL;

DROP TABLE dim_ms_ppass_stage;

END;
