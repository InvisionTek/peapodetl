BEGIN;

INSERT INTO dim_ms_time 
SELECT t.* FROM dim_ms_time_stage t
LEFT JOIN dim_ms_time s
ON t.time_key = s.time_key
WHERE s.time_key IS NULL;

DROP TABLE dim_ms_time_stage;

END;
