BEGIN;

update dim_ms_ord 
set ord_id = a.ord_id,
rdlv_ord_id = a.rdlv_ord_id,
dlv_meth_cd = a.dlv_meth_cd,
dlv_stat_cd = a.dlv_stat_cd,
ord_type_cd = a.ord_type_cd,
frst_tm_ord = a.frst_tm_ord,
ppass_used = a.ppass_used,
audt_upd_dt = a.audt_upd_dt
from dim_ms_ord_stage a
where dim_ms_ord.ord_key = a.ord_key;

INSERT INTO dim_ms_ord 
SELECT t.* FROM dim_ms_ord_stage t
LEFT JOIN dim_ms_ord s
ON t.ord_key = s.ord_key
WHERE s.ord_key IS NULL;

DROP TABLE dim_ms_ord_stage;

END;
