BEGIN;

update fact_dly_ppass 
set it_dmnd_qy = a.it_dmnd_qy,
it_dlv_qy = a.it_dlv_qy,
whse_it_dlv_qy = a.whse_it_dlv_qy,
arc_ld_dt = a.arc_ld_dt
from fact_dly_ppass_stage a
where fact_dly_ppass.time_key = a.time_key
and fact_dly_ppass.ord_key = a.ord_key
and fact_dly_ppass.stor_key = a.stor_key
and fact_dly_ppass.user_id = a.user_id
and fact_dly_ppass.ppass_key = a.ppass_key;

INSERT INTO fact_dly_ppass 
SELECT t.* FROM fact_dly_ppass_stage t
LEFT JOIN fact_dly_ppass s
ON t.time_key = s.time_key
and t.ord_key = s.ord_key
and t.stor_key = s.stor_key
and t.user_id = s.user_id
and t.ppass_key = s.ppass_key
WHERE (s.time_key IS NULL AND s.ord_key IS NULL);

DROP TABLE fact_dly_ppass_stage;

END;
