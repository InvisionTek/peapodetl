BEGIN;

INSERT INTO gxt_it_act 
SELECT 
t.ord_id,
t.orig_pod_id,
t.orig_upc_cd,
t.orig_pr_meth_cd,
t.orig_rqst_qy,
t.orig_ord_qy,
t.orig_oos_inv_qy,
t.orig_oos_pick_qy,
t.orig_oos_tot_qy,
t.orig_ship_qy,
t.orig_pr_qy,
t.orig_reg_pr_qy,
t.orig_spec_cd,
t.orig_btl_dep_qy,
t.orig_tax_pct_qy,
t.orig_tax_amt_qy,
t.orig_groc_amt_qy,
t.orig_cw_avg_qy,
t.orig_pick_wgt_qy,
t.sub_pod_id,
t.sub_upc_cd,
t.sub_pr_meth_cd,
t.sub_qy,
t.sub_ord_qy,
t.sub_oos_inv_ou_qy,
t.sub_oos_pick_ou_qy,
t.sub_oos_tot_ou_qy,
t.sub_ship_ou_qy,
t.sub_ship_qy,
t.sub_web_pr_qy,
t.sub_fill_pr_qy,
t.sub_btl_dep_qy,
t.sub_tax_pct_qy,
t.sub_tax_amt_qy,
t.sub_groc_amt_qy,
t.sub_cw_avg_qy,
t.sub_pick_wgt_qy,
t.tot_btl_dep_qy,
t.tot_groc_amt_qy,
t.tot_tax_amt_qy,
t.orig_oos_amt_qy,
t.sub_loss_amt_qy,
t.act_oos_qy,
t.act_oos_amt_qy,
t.arc_ld_dt 
FROM gxt_it_act_stage t
LEFT JOIN gxt_it_act s
ON t.ord_id = s.ord_id 
AND t.orig_pod_id = s.orig_pod_id
WHERE s.ord_id IS NULL;

DROP TABLE gxt_it_act_stage;

END;
