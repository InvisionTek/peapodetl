select
slot_id,
slot_hdr_id,
dlv_area_id,
to_char(dlv_tm,"%H:%M") dlv_tm,
ship_type_cd,
cast(dlv_wnd_tm as varchar(10))dlv_wnd_tm,
slot_avl_qy,
disc_qy,
slot_stat_cd,
dlv_msg_id,
stor_id,
shft_num_cd,
dlv_dt,
cut_off_dt_tm,
max_ord_qy,
slot_used_qy,
slot_hdr_used_qy
from daily_curr:cur_dly_slot_dtl;
