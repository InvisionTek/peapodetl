select
item_id,
whse,
item_name,
description,
nvl(unit_weight,0),
nvl(weight_uom_id,0),
nvl(unit_volume,0),
nvl(volume_uom_id,0)
from daily_curr:cdt_mahn_item_cbo;
