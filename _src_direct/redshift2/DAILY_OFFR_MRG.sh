#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

for ((i=1;i<=10;i++))
do 
date;
echo "Running upload art_offr_trk for day $i"
$CDIR/REDSHIFT.sh upl archive art_offr_trk daily audt_upd_dt_tm $i >> $LDIR/dly_mrg_upl.log 2>&1;

echo "Running merge art_offr_trk for day $i"
$CDIR/REDSHIFT.sh mrg archive art_offr_trk >> $LDIR/dly_mrg_ld.log 2>&1;
done
