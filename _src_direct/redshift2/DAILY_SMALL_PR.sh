#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

DAY_NO=`date '+%u'`
case $DAY_NO in
1) SMALL="daily_curr:cdt_it_dict_cat daily_curr:cdt_it_dict daily_curr:cur_dly_stor_zip daily_curr:cdt_cont_ctl dw:ah_cat_pod datamart:rdt_ecom_xref datamart:rdt_rpt_stor_lookup datamart:rdt_acn_hdr datamart:rdt_agcy_ctl datamart:rdt_ahld_cat_ctl datamart:rdt_banr_ctl datamart:rdt_jump_page_ctl datamart:rdt_site_ctl history:hst_stor_zip";;
*) SMALL="daily_curr:cdt_mgrp_vend_mstr daily_curr:cdt_it_dict_cat daily_curr:cdt_it_dict daily_curr:cur_dly_stor_zip daily_curr:cdt_cont_ctl dw:ah_cat_pod datamart:rdt_ecom_xref datamart:rdt_rpt_stor_lookup datamart:rdt_acn_hdr datamart:rdt_ahld_it_cat";;
esac

rm -f $LDIR/dly_small_upl.log

for small in $SMALL 
do

db=`echo $small|sed -n 's/:.*//p'`;
tab=`echo $small|sed -n 's/.*://p'`;

echo "Running daily small upload table $tab"
date;

$CDIR/REDSHIFT.sh upl $db $tab small >> $LDIR/dly_small_upl.log 2>&1

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_small_upl.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while uploading small table"
   mailx -s "Failed on uploading small table...existing here!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_small_upl.log
   exit;
else
   echo "OK"
fi

#sleep
sleep 5

rm -f $LDIR/dly_small_crt.log

#create table
for small in $SMALL
do

db=`echo $small|sed -n 's/:.*//p'`;
tab=`echo $small|sed -n 's/.*://p'`;

echo "Running daily small create table $tab"
date;

$CDIR/REDSHIFT.sh crt $tab >> $LDIR/dly_small_crt.log 2>&1

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_small_crt.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while creating small table...exiting here"
   mailx -s "Failed on creating small table!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_small_crt.log
   exit;
else
   echo "OK"
fi

#sleep
sleep 5

rm -f $LDIR/dly_small_ld.log

for small in $SMALL
do

db=`echo $small|sed -n 's/:.*//p'`;
tab=`echo $small|sed -n 's/.*://p'`;

echo "Running loading small table $tab"
date;

$CDIR/REDSHIFT.sh cpy $db $tab daily >> $LDIR/dly_small_ld.log 2>&1

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_small_ld.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while small load...check log "
   mailx -s "FAILED while small load table!!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_small_ld.log
else
   echo "OK"
fi
