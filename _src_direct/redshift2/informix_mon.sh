#!/bin/bash
#######################################################################
#
# SHELL NAME: informix_mon.sh
#
# DESCRIPTION:
#         ALIVE: ./informix_mon.sh ALIVE
#                0 = server is online
#                1 = server is not online (could be other mode like quescient, off etc etc)
#         DRI: ./informix_mon.sh DRI
#              Standard
#              Primary
#              HDR_secondary
#              RSS_secondary
#              SD_secondary(not available)
#               unknown(informix could be down)
#         LOG: /informix_mon.sh LOG
#              1=Between 25 to 75 % is filled, action needed
#              2=Between 75 to 100% is filled, red flag need to fix asap
#              0=no worry
#              unknown=file cannot be produce(informix could be down)
#         MEM: /informix_mon.sh MEM
#              1=os has about 25% to 5% memory available for informix
#              2=os has about 5% memory available for informix(action needed)
#              0=no worry
#              unknown=file cannot be produce(informix could be down)
#         LOCKS: /informix_mon.sh LOCKS
#              status 0 = nothing to worry
#              status 1 = indicator there is waiter locks
#              status 2 = indicator there is waiter lock from last run
#
# HISTORY:
#      Umanga Shrestha 12/21/2016
#
#######################################################################

# Directory List

#BASEDIR=/usr/lib64/nagios/plugins/informix_checks
BASEDIR=`echo $PWD`
MDATA=$BASEDIR/data

##################### Set up environment ################
#. /usr/lib64/nagios/plugins/informix_checks/envars

###########################
# FUNCTIONS
##########################

#usage function to inform user in case user used wrong arguments
usage()
(
  echo "   USAGE                                   "
  echo "   informix_mon.sh [ ALIVE ]          "
)

alive()
(
ALIV=`onstat - | awk '{ print $8 }'`
if ([ "${ALIV}" = "On-Line" ] || [ "${ALIV}" = "Read-Only" ])
then
  VAL=0;
else
  VAL=1;
fi

echo "$VAL"
)

dri()
(
rm -f $MDATA/dri.unl
dbaccess sysmaster - <<EOT! > /dev/null 2>&1
unload to $MDATA/dri.unl delimiter ''
SELECT decode(ha_type,1,"Primary",decode(ha_type,2,"HDR_Secondary",decode(ha_type,3,"SD_Secondary",decode(ha_type,4,"RSS_secondary",decode(ha_type,0,"Standard","unknown"))))) FROM sysha_type
;
EOT!

if [ -e $MDATA/dri.unl ]
then
   VAL=`cat "$MDATA/dri.unl"`
else
  VAL="unknown"
fi

echo "$VAL"

)

###########################
# VALIDATE USER ARGUMENT
###########################

if [ $# -eq 1 ]
then
   case $1 in
       ALIVE) alive;;  #TEST SERVER ALIVE
       DRI) dri;; #TEST if secondary,primary or RSS
       LOG) log_check;; #Test log used
       MEM) mem_check;; #Test memory availability
       LOCKS) lock_check;; #Test locks
       *) usage
          exit 1;;
   esac

else
   usage
   exit;
fi
