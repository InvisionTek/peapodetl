#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

DAY_NO=`date '+%u'`
case $DAY_NO in
*) TABLES="cyborgdw:gxt_it_act_dtl cyborgdw:gxt_ord_ctnr datamart:rdt_mktg_offr_dtl";;
#*) TABLES="cyborgdw:gxt_it_act_dtl cyborgdw:gxt_ord_ctnr cyborgdw:gxt_ord_rtrn datamart:rdt_mktg_offr_dtl";;
#*) TABLES="daily_curr:cdt_it_dict daily_curr:cur_dly_stor_zip datamart:rdt_ecom_xref datamart:rdt_rpt_stor_lookup datamart:rdt_acn_hdr";;
esac

rm -f $LDIR/dly_backed_upl1.log

for TAB in $TABLES 
do

db=`echo $TAB|sed -n 's/:.*//p'`;
tab=`echo $TAB|sed -n 's/.*://p'`;

echo "Running backed upload $tab"
date;

case $tab in
gxt_it_act_dtl) $CDIR/REDSHIFT.sh upl $db $tab backed arc_ld_dt 1 >> $LDIR/dly_backed_upl1.log 2>&1;;
gxt_ord_ctnr) $CDIR/REDSHIFT.sh upl $db $tab backed arc_ld_dt 1 >> $LDIR/dly_backed_upl1.log 2>&1;;
gxt_ord_rtrn) $CDIR/REDSHIFT.sh upl $db $tab backed arc_ld_dt 1 >> $LDIR/dly_backed_upl1.log 2>&1;;
rdt_mktg_offr_dtl) $CDIR/REDSHIFT.sh upl $db $tab backed dlv_dt 2 >> $LDIR/dly_backed_upl1.log 2>&1;;
esac

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_backed_upl1.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while uploading backed table"
   mailx -s "Failed on uploading backed table pr1...existing here!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_backed_upl1.log
   exit;
else
   echo "OK"
fi

#sleep
sleep 5

rm -f $LDIR/dly_backed_crt1.log

#create table
for TAB in $TABLES
do

db=`echo $TAB|sed -n 's/:.*//p'`;
tab=`echo $TAB|sed -n 's/.*://p'`;

echo "Running backed create $tab"
date;

$CDIR/REDSHIFT.sh crt $tab >> $LDIR/dly_backed_crt1.log 2>&1

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_backed_crt1.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while creating backed table pr1...exiting here"
   mailx -s "Failed on creating backed table pr1!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_backed_crt1.log
   exit;
else
   echo "OK"
fi

#sleep
sleep 5

rm -f $LDIR/dly_backed_ld1.log

for TAB in $TABLES
do

db=`echo $TAB|sed -n 's/:.*//p'`;
tab=`echo $TAB|sed -n 's/.*://p'`;

echo "Running backed load $tab"
date;

$CDIR/REDSHIFT.sh cpy $db $tab backed >> $LDIR/dly_backed_ld1.log 2>&1

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_backed_ld1.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while backed load pr1...check log "
   mailx -s "FAILED while backed load table pr1!!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_backed_ld1.log
else
   echo "OK"
fi
