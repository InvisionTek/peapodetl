#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

DAY_NO=`date '+%u'`
case $DAY_NO in
4) TABLES="datamart:rdt_wk_cgs_trk archive:art_po_hdr archive:art_po_dtl archive:art_po_fcst_hdr archive:art_po_fcst_dtl";;
*) TABLES="archive:art_po_hdr archive:art_po_dtl archive:art_po_fcst_hdr archive:art_po_fcst_dtl";;
#*) TABLES="daily_curr:cdt_it_dict daily_curr:cur_dly_stor_zip datamart:rdt_ecom_xref datamart:rdt_rpt_stor_lookup datamart:rdt_acn_hdr";;
esac

rm -f $LDIR/dly_backed_upl2.log

for TAB in $TABLES 
do

db=`echo $TAB|sed -n 's/:.*//p'`;
tab=`echo $TAB|sed -n 's/.*://p'`;

echo "Running backed upload $tab"
date;

case $tab in
rdt_wk_cgs_trk) $CDIR/REDSHIFT.sh upl $db $tab backed week_end_dt 5 >> $LDIR/dly_backed_upl2.log 2>&1;;
art_po_hdr) $CDIR/REDSHIFT_ALT.sh upl $tab 0 >> $LDIR/dly_backed_upl2.log 2>&1;;
art_po_dtl) $CDIR/REDSHIFT_ALT.sh upl $tab 0 >> $LDIR/dly_backed_upl2.log 2>&1;;
art_po_fcst_hdr) $CDIR/REDSHIFT_ALT.sh upl $tab 0 >> $LDIR/dly_backed_upl2.log 2>&1;;
art_po_fcst_dtl) $CDIR/REDSHIFT_ALT.sh upl $tab 0 >> $LDIR/dly_backed_upl2.log 2>&1;;
esac

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_backed_upl2.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while uploading backed table"
   mailx -s "Failed on uploading backed table pr2...existing here!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_backed_upl2.log
   exit;
else
   echo "OK"
fi

#sleep
sleep 5

rm -f $LDIR/dly_backed_crt2.log

#create table
for TAB in $TABLES
do

db=`echo $TAB|sed -n 's/:.*//p'`;
tab=`echo $TAB|sed -n 's/.*://p'`;

echo "Running backed create $tab"
date;

$CDIR/REDSHIFT.sh crt $tab >> $LDIR/dly_backed_crt2.log 2>&1

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_backed_crt2.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while creating backed table pr2...exiting here"
   mailx -s "Failed on creating backed table pr2!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_backed_crt2.log
   exit;
else
   echo "OK"
fi

#sleep
sleep 5

rm -f $LDIR/dly_backed_ld2.log

for TAB in $TABLES
do

db=`echo $TAB|sed -n 's/:.*//p'`;
tab=`echo $TAB|sed -n 's/.*://p'`;

echo "Running backed load $tab"
date;

$CDIR/REDSHIFT.sh cpy $db $tab backed >> $LDIR/dly_backed_ld2.log 2>&1

done

ERROR_N=`egrep -c "ERROR" $LDIR/dly_backed_ld2.log`
if [ $ERROR_N -gt 0 ]
then
   echo "FAILED while backed load pr2...check log "
   mailx -s "FAILED while backed load table pr2!!!!" ushresth@peapod.com,nmiles@peapod.com < $LDIR/dly_backed_ld2.log
else
   echo "OK"
fi
