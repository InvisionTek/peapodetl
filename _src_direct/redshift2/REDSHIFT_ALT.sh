#!/bin/ksh
###################
# Variables
###################
SCRIPT=`basename $0`
#PDIR=/Applications/pentaho60/design-tools/data-integration
PDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

###############
# Functions
##############
#usage
usage()
(
  echo " 	USAGE						"
  echo "        syntax <upl backed/daily>                       "
) 

#check logs
checklogs()
( 
   LOGFILE=$1
   
   cd $LDIR
   #ERRCNT=`cat $LOGFILE|egrep -v "ERROR*"|egrep -c "Kitchen - ERROR|FAILURE"`
   ERRCNT=`cat $LOGFILE|egrep -c "Finished with errors|Couldn't execute SQL|Invalid operation:|Could not read from"|egrep -v "ERROR*"` 
   #ERRCNT=`egrep -c "ERROR|FAILURE" $LOGFILE`
   ERRCNNT=`egrep -c "not connected" $LOGFILE`
   if [ $ERRCNT -gt 0 ]
   then
      MSG=1
   elif [ $ERRCNNT -gt 0 ]
   then
      MSG=2
   else
      MSG=0
   fi 
   return $MSG
)

upload()
(
TAB=$1
DY=$2

 TDATE=`/bin/date --date="$DY day ago" +%Y%m%d`
 #TDATE=`date -v-${DY}d +%Y%m%d`
  
 cd $PDIR
 ./pan.sh -file="$CDIR/upload/upl_${TAB}_daily.ktr" -param:tab="$TAB" -param:dy="$DY" -param:tdate="$TDATE" -Level=ERROR > $LDIR/log.upload_${TAB} 2>&1 

 checklogs log.upload_${TAB} 
 OUTPUT=$?
 if [ $OUTPUT -eq 0 ]
 then
    echo "upload on ${TAB} is success!!!!!"
 else
    echo "ERROR ${OUTPUT} on upload ${TAB}; Check the log...exiting" | mailx -s "ERROR:upload ${TAB}" ushresth@peapod.com,nmiles@peapod.com
    echo "ERROR ${OUTPUT} ...exiting"
    exit
 fi
)

################
# MAIN
################
case $1 in
    upl) shift
         if [ $# -eq 2 ]
         then
             TAB=$1
             DY=$2
             upload $TAB $DY
         else
             echo "wrong arg"
         fi;; 
      *) echo "Nothing";;
esac
