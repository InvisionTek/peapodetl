drop table if exists fact_dly_ppass;
create table fact_dly_ppass(
time_key integer,
ord_key integer,
stor_key integer,
user_id integer,
ppass_key integer,
it_dmnd_qy decimal(7,2),
it_dlv_qy decimal(7,2),
whse_it_dlv_qy decimal(10,2),
pr_qy decimal(7,2),
arc_ld_dt date,
primary key(time_key,ord_key,stor_key,user_id,ppass_key))
distkey(time_key)
interleaved sortkey(time_key,ord_key,stor_key,user_id,ppass_key);
