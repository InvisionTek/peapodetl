drop table if exists art_po_dtl;
create table art_po_dtl 
  (
   po_id integer,
   pod_id integer,
   ord_case_qy smallint,
   rcv_case_qy smallint,
   sku_cd varchar(14),
   vend_rtio_qy smallint,
   rcv_unit_qy integer
)
distkey(po_id)
interleaved sortkey(pod_id);
