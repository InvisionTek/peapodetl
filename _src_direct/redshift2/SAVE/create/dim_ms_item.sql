drop table if exists dim_ms_item;
create table dim_ms_item(
pod_key integer,
pod_id  varchar(10),
it_long_name_tx varchar(120),
shed_bite_fl char(1),
sh_dept varchar(50),
sh_cat varchar(40),
sh_sub_cat varchar(50),
hist_actv_cd char(1),
hist_strt_dt date,
hist_end_dt date,
audt_upd_dt date,
primary key(pod_key))
distkey(pod_key)
interleaved sortkey(pod_key);
grant select on table dim_ms_item to group "dba";
grant select on table dim_ms_item to group "analytics";
