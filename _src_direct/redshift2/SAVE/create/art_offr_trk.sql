drop table if exists art_offr_trk;
create table art_offr_trk(
user_id integer,
list_id integer,
offr_cd varchar(30),
rdm_dt_tm timestamp,
rdm_cd varchar(30),
rdm_amt_qy decimal(7,4),
it_cd char(5),
ord_id char(15),
prod_id integer,
evnt_id integer,
audt_cr_id char(8),
audt_cr_dt_tm timestamp,
audt_upd_id char(8),
audt_upd_dt_tm timestamp, 
primary key(user_id,list_id,offr_cd))
distkey(audt_upd_dt_tm)
interleaved sortkey(user_id,list_id,offr_cd,ord_id);
