drop table if exists dim_ms_time;
create table dim_ms_time(
time_key integer,
the_dt date,
week varchar(10),
ahdz_week varchar(10),
per_no varchar(10),
ahdz_per varchar(10),
month varchar(10),
quater varchar(10),
ahdz_qtr varchar(10),
year varchar(10),
primary key(time_key))
distkey(time_key)
interleaved sortkey(time_key);
grant select on table dim_ms_time to group "dba";
grant select on table dim_ms_time to group "analytics;
