drop table if exists art_po_fcst_hdr;
create table art_po_fcst_hdr 
  (
   po_id integer,
   vend_cd integer,
   po_dlv_dt date,
   lead_time_qy smallint,
   fcst_alg_tx varchar(50),
   load_bal_pct_qy decimal(7,2),
   stor_id char(3),
   po_cr_dt date
)
distkey(po_id)
interleaved sortkey(po_cr_dt,vend_cd,stor_id);
