drop table if exists rdt_tm_look;
create table rdt_tm_look(
day_no integer,
the_date date,
week_no integer,
week char(6),
mo_no integer,
month char(6),
week_end_dt date,
ss_per char(6),
per_no integer,
mo_4_wk char(6),
wk_num integer,
quarter char(6),
qtr_no integer,
pub_week_no integer,
pub_week char(6),
ss_per_ops char(6),
week_ops char(6),
ahdz_per char(6),
ahdz_per_no integer,
ahdz_week char(6),
ahdz_qtr char(6),
ahdz_wk_num integer,
primary key(the_date))
distkey(the_date)
interleaved sortkey(the_date);
grant select on table rdt_tm_look to group "dba";
grant select on table rdt_tm_look to group "analytics";
