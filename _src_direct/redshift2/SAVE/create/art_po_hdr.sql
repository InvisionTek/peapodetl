drop table if exists art_po_hdr;
create table art_po_hdr 
  (
   po_id integer,
   stor_id char(3),
   po_type_cd char(1),
   po_stat_cd char(1),
   vend_cd smallint,
   prim_buyr_cd smallint,
   po_desc_tx varchar(50),
   po_cr_dt date,
   ok_to_send_cd char(1),
   po_dlv_dt date,
   po_rcv_dt date,
   po_send_dt date,
   po_expt_dt date,
   po_cr_mode_cd char(1),
   po_xfer_dt date
)
distkey(po_id)
interleaved sortkey(po_xfer_dt,stor_id);
