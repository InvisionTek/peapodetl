drop table if exists dim_ms_ppass;
create table dim_ms_ppass(
ppass_key integer,
user_dlv_subs_id varchar(50),
dlv_subs_id varchar(50),
purc_dt date,
term_exp_dt date,
pr_qy decimal(7,2),
tax_pr_qy decimal(5,2),
auto_renw_cd char(1),
stat_cd char(1),
free_try_cd char(1),
audt_cr_dt date,
audt_upd_dt date,
arc_ld_dt date,
primary key(ppass_key))
distkey(ppass_key)
interleaved sortkey(ppass_key);
grant select on table dim_ms_ppass to group "dba";
grant select on table dim_ms_ppass to group "analytics;
