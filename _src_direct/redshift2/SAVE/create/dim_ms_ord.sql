drop table if exists dim_ms_ord_new;
create table dim_ms_ord_new(
ord_key integer,
ord_id varchar(10),
rdlv_ord_id varchar(10),
dlv_meth_cd char(1),
dlv_stat_cd char(2),
ord_type_cd char(1),
frst_tm_ord char(1),
ppass_used  char(1),
audt_upd_dt date,
primary key(ord_key))
distkey(ord_key)
interleaved sortkey(ord_key);
grant select on table dim_ms_ord_new to group "dba";
grant select on table dim_ms_ord_new to group "analytics;
