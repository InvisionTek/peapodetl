drop table if exists gxt_ord_hdr;
create table gxt_ord_hdr
  (
    ord_id varchar(10),
    cnsm_id varchar(10),
    stor_id varchar(7),
    ord_type_cd char(3),
    rqst_dt date,
    rqst_tm varchar(10),
    rqst_end_tm varchar(10),
    slot_id integer,
    ord_plc_cd varchar(5),
    dlv_type_cd char(1),
    seq_id integer,
    ord_tot_qy decimal(11,2),
    ord_tax_qy decimal(11,2),
    tax_grp_cd varchar(5),
    ord_btl_dep_qy decimal(11,2),
    natl_tot_qy decimal(11,2),
    natl_tax_qy decimal(11,2),
    natl_ship_fee_qy decimal(11,2),
    natl_crdt_card_qy decimal(11,2),
    pr_zone_cd smallint,
    invc_dt_tm timestamp,
    prnt_dt_tm timestamp,
    arc_ld_dt date,
    primary key(ord_id)
)
distkey(ord_id)
interleaved sortkey(rqst_dt,arc_ld_dt,cnsm_id,stor_id,slot_id);
