drop table if exists dim_ms_stor;
create table dim_ms_stor(
stor_key integer,
stor_id varchar(5),
rpt_name_cd varchar(5),
hist_strt_dt date,
hist_end_dt date,
hist_actv_cd char(1),
audt_upd_dt date,
primary key(stor_key))
distkey(stor_key)
interleaved sortkey(stor_key);
grant select on table dim_ms_item to group "dba";
grant select on table dim_ms_item to group "analytics";
