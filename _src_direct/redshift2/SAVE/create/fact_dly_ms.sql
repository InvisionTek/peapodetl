drop table if exists fact_dly_ms;
create table fact_dly_ms(
time_key integer,
ord_key integer,
stor_key integer,
pod_key  integer,
whse_id_dlv_qy integer,
whse_it_dlv_amt decimal(10,2),
arc_ld_dt date,
primary key(time_key,ord_key,stor_key,pod_key))
distkey(time_key)
interleaved sortkey(time_key,ord_key,stor_key,pod_key);
