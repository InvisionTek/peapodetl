#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

i=63;
while [ $i -le 63 -a $i -gt 0 ];
do
date;
echo "I am running $i day"
 $CDIR/DAILY_MRG_UPL.sh $i; 
 ((i-=1));
done
