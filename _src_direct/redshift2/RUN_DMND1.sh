#!/bin/ksh

CDIR=/code/pentaho/redshift2
LDIR=/logs/pentaho/redshift2

i=0;
while [ $i -le 0 -a $i -ge 0 ];
do
date;
echo "Running $i day";
 $CDIR/REDSHIFT_ALT.sh upl pit_nwtn_po_fcst_hdr $i;
 ((i-=1));
done
