#!/bin/ksh

#PDIR=/Applications/pentaho503/design-tools/data-integration
PDIR=/opt/pentaho/design-tools/data-integration

CDIR=/code/pentaho/redshift2/cloudflare
LDIR=/logs/pentaho/redshift2

FIVE_FRNT=`/bin/date --date="5 minutes ago" | awk '{split($4, a, ":"); printf "%s %s %s:%02d:00", $2, $3, a[1],int(a[2]/5)*5, $5}'`
#FIVE_FRNT=`date -v -5M | awk '{split($4, a, ":"); printf "%s %s %s:%02d:00", $2, $3, a[1],int(a[2]/5)*5, $5}'`

FIVE_RARE=`date | awk '{split($4, a, ":"); printf "%s %s %s:%02d:00", $2, $3, a[1],int(a[2]/5)*5, $5}'`

FILE=`date +'%Y%m%d_%H%M'`
FOLD=`date +'%Y%m%d`

SDATE=`date -d "${FIVE_FRNT}" "+%s"`
#SDATE=`date -j -f "%b %d %T" "${FIVE_FRNT}" "+%s"`

EDATE=`date -d "${FIVE_RARE}" "+%s"`
#EDATE=`date -j -f "%b %d %T" "${FIVE_RARE}" "+%s"`

CDIR=/code/pentaho/redshift2/cloudflare
LDIR=/logs/pentaho/redshift2

echo $SDATE $EDATE $FILE $FOLD

cd $PDIR
./kitchen.sh -file="$CDIR/jb_flare_log.kjb" -param:SDATE="$SDATE" -param:EDATE="$EDATE" -param:FILE="$FILE" -param:FOLD="$FOLD" -Level=ERROR > $LDIR/log.cloud_flare 2>&1
