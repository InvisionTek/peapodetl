#! /bin/ksh

PDIR=/opt/pentaho/design-tools/data-integration

CDIR=/code/pentaho/redshift2/cloudflare
LDIR=/logs/pentaho/redshift2

$PDIR/kitchen.sh -file="$CDIR/jb_7day_crt.kjb" -Level=ERROR > $LDIR/log.7day_crt 2>&1;

CDAY=7;
while [ $CDAY -le 7 -a $CDAY -gt 0 ];
do
 date;
 VAR_DATE=`/bin/date --date="$CDAY day ago" +%Y%m%d`;
 echo "running for $VAR_DATE";
 $PDIR/kitchen.sh -file="$CDIR/jb_7day_load.kjb" -param:FOLD="$VAR_DATE" -Level=ERROR > $LDIR/log.7day_load 2>&1;
 echo "DONE"
 date;
 echo "#################################"
 ((CDAY-=1));
done
