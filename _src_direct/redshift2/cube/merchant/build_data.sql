INSERT INTO cube_ms_measure_new
SELECT a.ord_id,b.orig_pod_id,a.dlv_dt,a.zip_cd,a.stor_id,b.orig_ship_qy,(b.orig_pr_qy*b.orig_rqst_qy*b.orig_cw_avg_qy)orig_ship_qy,0,0
FROM peapod.rdt_ord_sum a, peapod.gxt_it_act b
WHERE a.ord_id = b.ord_id
AND dlv_dt between '2016-01-03' and '2016-12-31'
and orig_ship_qy > 0
and ord_seq_num > 0
union
SELECT a.ord_id,sub_pod_id,a.dlv_dt,a.zip_cd,a.stor_id,0,0,b.sub_ship_qy,(b.sub_ship_qy*b.sub_web_pr_qy*b.sub_cw_avg_qy)
FROM peapod.rdt_ord_sum a, peapod.gxt_it_act b
WHERE a.ord_id = b.ord_id
AND dlv_dt between '2016-01-03' and '2016-12-31'
and sub_ship_qy > 0
and ord_seq_num > 0;

INSERT INTO cube_ms_dim_dept_new
SELECT pod_id, ah_dept, ah_cat, ah_sub_cat
FROM peapod.ah_cat_pod;

INSERT INTO cube_ms_dim_ord_new
select ord_id,type_cd,dlv_meth_cd, case when test > 0 then 'N' else 'Y' end from(
SELECT a.ord_id,type_cd,dlv_meth_cd, sum(case when orig_ship_qy > 0 then 1 else 0 end) test
FROM peapod.rdt_ord_sum a, peapod.gxt_it_act b
WHERE a.ord_id = b.ord_id
and dlv_dt between '2016-01-03' and '2016-12-31'
and ord_seq_num > 0
group by 1,2,3);

INSERT INTO cube_ms_dim_store_new
select distinct a.stor_id,c.sns_mkt_area_tx
from peapod.cur_dly_stor_zip a,peapod.rdt_ecom_xref c
where  a.stor_id = c.stor_id
UNION
select distinct a.stor_id,c.sns_mkt_area_tx  
from peapod.hst_stor_zip a, peapod.rdt_ecom_xref c
where a.stor_id = c.stor_id;  

INSERT INTO cube_ms_dim_time_new
select the_date,week,ss_per,quarter,substring(ss_per,1,4)years
from peapod.rdt_tm_look
where the_date >= '2014-12-28';

Analyze cube_ms_dim_time_new;
Analyze cube_ms_dim_pod_new;
Analyze cube_ms_dim_pod_new(pod_id,ah_dept,ah_cat,ah_sub_cat);
Analyze cube_ms_measure_new;
Analyze cube_ms_measure_new(ord_id,pod_id,dlv_dt,zip_cd,stor_id);
Analyze cube_ms_dim_dept_new;
Analyze cube_ms_dim_store_new;
Analyze cube_ms_dim_ord_new;
Analyze cube_ms_dim_ord_new(ord_id,ord_cnsm_type,dlv_meth_cd,is_all_sub);
