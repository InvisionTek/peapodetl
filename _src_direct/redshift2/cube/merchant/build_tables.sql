drop table if exists cube_ms_measure_new;
create table cube_ms_measure_new(
ord_id varchar(10) encode lzo,
pod_id integer encode lzo,
dlv_dt date encode delta32k,
zip_cd varchar(10) encode lzo,
stor_id varchar(10) encode lzo,
dmnd_ship_qy integer encode lzo,
dmnd_ship_amnt_qy decimal(11,4) encode bytedict,
sub_ship_qy integer encode lzo,
sub_ship_amnt_qy decimal(11,4) encode lzo,
primary key(ord_id,pod_id))
distkey(pod_id)
interleaved sortkey(ord_id,pod_id,dlv_dt,zip_cd,stor_id);

drop table if exists cube_ms_dim_dept_new;
create table cube_ms_dim_dept_new(
pod_id integer,
ah_dept varchar(50),
ah_cat varchar(50),
ah_sub_cat varchar(50))
diststyle all
interleaved sortkey(pod_id,ah_dept,ah_cat,ah_sub_cat);

drop table if exists cube_ms_dim_ord_new;
create table cube_ms_dim_ord_new(
ord_id varchar(10) encode lzo,
ord_cnsm_type char(1) encode lzo,
dlv_meth_cd char(1) encode lzo,
is_all_sub char(1) encode lzo,
primary key(ord_id))
distkey(ord_id)
interleaved sortkey(ord_id);

drop table if exists cube_ms_dim_store_new;
create table cube_ms_dim_store_new(
stor_id varchar(7),
sns_mkt_area_tx varchar(30),
primary key(stor_id))
diststyle all
interleaved sortkey(stor_id,sns_mkt_area_tx);

drop table if exists cube_ms_dim_mktg_new;
create table cube_ms_dim_mktg_new(
stor_id varchar(7),
rpt_stor_desc varchar(30),
sns_mkt_area_tx varchar(30),
primary key(stor_id))
distkey(stor_id)
interleaved sortkey(stor_id,rpt_stor_desc,sns_mkt_area_tx);

drop table if exists cube_ms_dim_pod_new;
create table cube_ms_dim_pod_new(
pod_id integer,
it_long_name_tx varchar(100),
brnd_name_tx varchar(100),
primary key(pod_id))
distkey(pod_id)
interleaved sortkey(pod_id,it_long_name_tx,brnd_name_tx);

drop table if exists cube_ms_dim_time_new;
create table cube_ms_dim_time_new(
dlv_dt date,
week varchar(10),
ss_per varchar(10),
quater varchar(10),
years varchar(10),
primary key(dlv_dt))
diststyle all
interleaved sortkey(dlv_dt,week,ss_per,quater,years);
