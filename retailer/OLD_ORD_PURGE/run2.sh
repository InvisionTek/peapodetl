#!/bin/bash
## run2.sh - Run the retailer purge job standalone

LOGFILE=/logs/pentaho/retailer/cpt_ord_hdr_purge.out

echo "--" >>$LOGFILE
echo "-- Running at: `date` "  >>$LOGFILE
echo "--"  >>$LOGFILE
/u/pentaho/bin/run_kitchen.sh /code/pentaho/retailer/OLD_ORD_PURGE/cpt_ord_hdr_purge_delete.kjb  >>$LOGFILE 2>&1

