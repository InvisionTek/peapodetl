<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">

        <xsl:for-each select="arr[@name='upc']/str">
            <xsl:variable name="numl">
                <xsl:value-of select="string-length(.)" />
            </xsl:variable>

            <xsl:if test="$numl = 12 or $numl=6">
                    <xsl:text>,</xsl:text>
                    <xsl:value-of select="." />
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
