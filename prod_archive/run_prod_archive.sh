#!/bin/bash

KDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/prod_archive
LDIR=/logs/pentaho/prod_archive

######past_purc#####
i=1;
j=0;
while [ $i -le 1 -a $i -ge 0 -a $j -le 1 -a $j -ge 0 ];
do
date;
echo "running for $i $j days"
cd $KDIR
./kitchen.sh -file="$CDIR/jb_past_purc.kjb" -param:STRT=$i -param:ENDS=$j -Level=Basic > $LDIR/log.jb_past_purc 2 >&1
((i-=1));
((j-=1));
done

######srch_term########
#k=30;
#l=0;
#date;
#cd $KDIR
#./kitchen.sh -file="$CDIR/jb_srch_term.kjb" -param:STRT=$k -param:ENDS=$l -Level=Basic > $LDIR/log.jb_srch_term 2 >&1
