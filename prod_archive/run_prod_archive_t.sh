#!/bin/bash

KDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/prod_archive
LDIR=/logs/pentaho/prod_archive

k=30;
l=0;
date;
cd $KDIR
./kitchen.sh -file="$CDIR/jb_srch_term.kjb" -param:STRT=$k -param:ENDS=$l -Level=Basic > $LDIR/log.jb_srch_term 2 >&1
