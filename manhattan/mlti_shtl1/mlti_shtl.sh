#!/bin/bash

# Time in minute to consider recent
RCNMIN=4
# Time in minute to consider recent on running file
RCNRUNMIN=30
KDIR=/opt/pentaho/design-tools/data-integration
#KDIR=/Applications/pentaho503/design-tools/data-integration
TESTDIR=/data/pentaho/requests/mlti_shtl
CDIR=/code/pentaho/manhattan/mlti_shtl1
ARCHDIR=$TESTDIR/archive
LDIR=/logs/pentaho/manhattan/mlti_shtl1
BOOLEAN=N

cd $TESTDIR
FILE=run.txt
RUNFILE=run_is_running.txt
TimeTest=`date +"%H%M%S"`

RCNTFILE=`find $FILE -mmin -$RCNMIN -print 2>/dev/null` 
RCNRUNFILE=`find $RUNFILE -mmin -$RCNRUNMIN -print 2> /dev/null`

##Specific time auto run
if [ "${TimeTest}" -gt 20000 ] && [ "${TimeTest}" -lt 20300 ]
then
  if [[ -e "$RUNFILE" && "$RUNFILE" = "$RCNRUNFILE" ]]
  then 
     if [ -e "$FILE" ]
     then
        mv $TESTDIR/$FILE $ARCHDIR/$FILE.`date +'%Y%m%d_%H%M%S'`.cancel
     fi
     date;
     echo "There is already one running";
     exit 5;
   else  
     touch $TESTDIR/$RUNFILE
     STORE="GIJ"
     EMAILS="kallon.maigore@ahold.com oliver.wallace.deborah@ahold.com, angel.reyes@ahold.com, jose.ramos1@ahold.com, michael.allison@ahold.com, louis.d'angeli@ahold.com, leonardo.toro@ahold.com, kwright@ahold.com, tchilds@ahold.com, lynn.blasio@ahold.com, susana.m.pereira@ahold.com, jason.h.cohen@ahold.com" 
     cd $KDIR 
     ./kitchen.sh -file="$CDIR/main_mlti.kjb" -param:STOR="$STORE" -param:MAIL="$EMAILS" -Level=Basic > $LDIR/log.main_mlti 2 >&1
     mv $TESTDIR/$RUNFILE $ARCHDIR/$RUNFILE.`date +'%Y%m%d_%H%M%S'`.complete
   fi
elif [[ -e "$FILE" && "$FILE" = "$RCNTFILE" ]]
then
   BOOLEAN=Y 
elif [ -e "$FILE" ]
then
   rm -r $TESTDIR/$FILE;
   exit 0;
else
   date;
   echo "No file to process";
   exit 1;
fi

if [ "$BOOLEAN" = "Y" ]
then 
  if [[ -e "$RUNFILE" && "$RUNFILE" = "$RCNRUNFILE" ]]
  then
     if [ -e "$FILE" ]
     then
        mv $TESTDIR/$FILE $ARCHDIR/$FILE.`date +'%Y%m%d_%H%M%S'`.cancel
     fi
     date;
     echo "There is already one running";
     exit 2;
  else 
    mv $TESTDIR/$FILE $TESTDIR/$RUNFILE
    STORE=`cat $TESTDIR/$RUNFILE|sed -n '1s/.*://p'`;
    EMAILS=`cat $TESTDIR/$RUNFILE|sed -n '2s/.*://p'`;

    cd $KDIR
    ./kitchen.sh -file="$CDIR/main_mlti.kjb" -param:STOR="$STORE" -param:MAIL="$EMAILS" -Level=Basic > $LDIR/log.main_mlti 2 >&1 
    mv $TESTDIR/$RUNFILE $ARCHDIR/$RUNFILE.`date +'%Y%m%d_%H%M%S'`.complete
  fi 
fi
