#!/bin/bash

ARG1=$1
ARG2=$2
ARG3=$3

KDIR=/opt/pentaho/design-tools/data-integration
#KDIR=/Applications/pentaho503/design-tools/data-integration
CDIR=/code/pentaho/manhattan/shift
LDIR=/logs/pentaho/manhattan/shift

date;
cd $KDIR
./kitchen.sh -file="$CDIR/shift_rpt_${ARG1}.kjb" -param:STOR_ID="${ARG1}" -param:SHFT="${ARG2}" -param:DY="${ARG3}" -Level=Basic > $LDIR/log.shift_rpt_${ARG1} 2>&1
