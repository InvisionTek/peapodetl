#!/bin/bash
KDIR=/opt/pentaho/design-tools/data-integration
#KDIR=/Applications/pentaho503/design-tools/data-integration

CDIR=/code/pentaho/dba/mnitor
LDIR=/logs/pentaho/dba/mnitor

#SVR="ifxp31 ifxp32 ifxdb ifxet"
SVR="ifxdb ifxp32"
date;
for svr in $SVR
do

echo "running for ${svr}...";

if [ "$svr" = "ifxdb" ]
then
  conJNDI=ifxdb1_customer_dba
elif [ "$svr" = "ifxp32" ]
then
  conJNDI=ifxp32_mcpick
fi

cd $KDIR
./kitchen.sh -file="$CDIR/infor_mon.kjb" -param:SRVR="$svr" -param:CONjndi="$conJNDI" -Level=Basic > $LDIR/log.infor_mon 2 >&1

done
date;

