set isolation dirty read;
set pdqpriority 100;

create unique index upt_user_prop_tx_pk on upt_user_prop_tx (user_id,prop_key_tx)
    in curdlyidxdbs1;
create unique index dps_user_pk on dps_user (user_id)
    in curdlyidxdbs1;
create unique index upt_comc_info_pk on upt_comc_info (user_id)
    in curdlyidxdbs1;
create unique index upt_user_pk on upt_user (user_id)
    in curdlyidxdbs1;
create unique index upt_user_dflt_adr_pk on upt_user_dflt_adr (user_id,adr_id,opco_id)
    in curdlyidxdbs1;
create unique index dps_contact_info_pk on dps_contact_info (id)
    in curdlyidxdbs1;
create        index dps_contact_info_i2 on dps_contact_info (user_id)
    in curdlyidxdbs1;
create unique index upt_cntc_info_pk on upt_cntc_info (adr_id)
    in curdlyidxdbs1;
create unique index upt_rtlr_card_pk on upt_rtlr_card (rtlr_card_id)
    in curdlyidxdbs1;
create unique index upt_rtlr_card_i2 on upt_rtlr_card (user_id,opco_id)
    in curdlyidxdbs1;

