#!/bin/bash
#KDIR=/Applications/pentaho61/design-tools/data-integration
KDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/replenish
LDIR=/logs/pentaho/replenish
DDIR=/data/pentaho/replenish

rm -rf $DDIR/actv_store.unl

date;
echo "getting active stores..."
cd $KDIR
./pan.sh -file="$CDIR/trn_get_actv_store.ktr" -Level=ERROR > $LDIR/log.trn_get_actv_store 2>&1
date;

echo "-------------------------------"

if [ -e $DDIR/actv_store.unl ]
then

for stor in `cat $DDIR/actv_store.unl`
do
  date;
  echo "running for ${stor}...";
  cd $KDIR
  ./kitchen.sh -file="$CDIR/main_prch_it.kjb" -param:STOR_ID="$stor" -Level=Basic > $LDIR/log.main_prch_it 2>&1
  date;
  echo "-------------------------------"
done

else
  echo "NOT there"
fi

