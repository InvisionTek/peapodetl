#!/bin/bash
KDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/archive/DAILY_ART3/smartmile
LDIR=/logs/pentaho/archive/DAILY_ART3/smartmile
DDIR=/data/pentaho/archive/DAILY_ART3/smartmile

i=114;

while [ $i -le 114 -a $i -ge 1 ];
do
 date;
 echo "running for $i days"
 cd $KDIR
 ./kitchen.sh -file="$CDIR/jb_ds_line_item.kjb" -param:DY="$i"  -Level=ERROR > $LDIR/log.jb_ds_line_item 2>&1
 ((i-=1));
done
