#!/bin/bash

KDIR=/opt/pentaho/design-tools/data-integration
CDIR=/code/pentaho/archive/DAILY_ART4/whse/whse_gan
LDIR=/logs/pentaho/archive/DAILY_ART4/whse/whse_gan

i=83
while [ $i -le 84 -a $i -gt 20 ];
do
cd $KDIR
echo "running for $i days"
./kitchen.sh -file="$CDIR/job_rerun_gxt_ee.kjb" -param:DAYS="$i" -Level=Basic > $LDIR/log.job_rerun_gxt_ee 2 >&1
((i-=1));
done
